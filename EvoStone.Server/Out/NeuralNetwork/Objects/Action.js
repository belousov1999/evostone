"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NNPlayer_1 = require("./NNPlayer");
class Action {
    constructor(_turn, _self, _opponent, _optionName) {
        this.Turn = _turn;
        this.Self = new NNPlayer_1.default(_self, true);
        this.Opponent = new NNPlayer_1.default(_opponent, false);
        this.OptionName = _optionName;
        this.ActionValue = 0.2; //Calculated after action apply
    }
    UpdateActionValue(newValue) {
        if (this.ActionValue == newValue)
            return;
        if (newValue < 0)
            newValue = 0;
        else if (newValue > 1.0)
            newValue = 1.0;
        this.ActionValue = newValue;
    }
    Convert() {
        var main = {
            //Turn: this.Turn,
            ["Action_" + this.OptionName]: 1.0,
            ActionValue: this.ActionValue
        };
        //Converted data to arrays
        var self = this.Self.Convert();
        var opponent = this.Opponent.Convert();
        //Add arrays data to `main`
        for (var k in self)
            main[k] = self[k];
        for (var k in opponent)
            main[k] = opponent[k];
        return main;
    }
}
exports.default = Action;
//# sourceMappingURL=Action.js.map