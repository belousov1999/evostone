"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require('fs');
class ManagerNN {
    static AddAction(action) {
        if (ManagerNN.IsActionExists(action)) {
            ManagerNN.UpdateActionValue(action);
            ManagerNN.WriteDataToFile();
            return;
        }
        if (ManagerNN.ActionsObjects.length == 0) {
            if (!ManagerNN.IsWriting && !ManagerNN.IsReading) {
                ManagerNN.IsReading = true;
                fs.readFile(this.DataFolder + "Data.json", (err, data) => {
                    if (err) {
                        console.error(err);
                        if (fs.existsSync(this.DataFolder + "Backup1.json")) {
                            fs.readFile(this.DataFolder + "Backup1.json", (err1, data1) => {
                                if (err1) {
                                    console.error(err1);
                                    ManagerNN.IsReading = false;
                                }
                                var data_json = JSON.parse(data1);
                                for (var k in data_json)
                                    ManagerNN.ActionsObjects.push(data_json[k]);
                                ManagerNN.IsReading = false;
                                ManagerNN.Preloaded = true;
                                return;
                            });
                        }
                        else
                            ManagerNN.IsReading = false;
                        return;
                    }
                    var data_json = JSON.parse(data);
                    for (var k in data_json)
                        ManagerNN.ActionsObjects.push(data_json[k]);
                    ManagerNN.IsReading = false;
                    ManagerNN.Preloaded = true;
                });
            }
        }
        ManagerNN.ActionsObjects.push(action);
        ManagerNN.WriteDataToFile();
    }
    //Find similar action. If `ActionsObjects` length is zero, return false
    static IsActionExists(action) {
        if (ManagerNN.ActionsObjects.length == 0)
            return false;
        var key = this.GetActionKey(action);
        return key != "-1";
    }
    static GetActionValue(action) {
        console.log("Length: " + ManagerNN.ActionsObjects.length);
        if (ManagerNN.ActionsObjects.length == 0)
            return -1;
        var key = this.GetActionKey(action);
        if (key == "-1")
            return -1;
        return ManagerNN.ActionsObjects[key]["ActionValue"];
    }
    static UpdateActionValue(action) {
        if (ManagerNN.ActionsObjects.length == 0)
            return;
        var key = this.GetActionKey(action);
        if (key == "-1")
            return;
        ManagerNN.ActionsObjects[key]["ActionValue"] = action["ActionValue"];
    }
    static GetActionKey(action) {
        if (ManagerNN.ActionsObjects.length == 0)
            return "-1";
        for (var k in ManagerNN.ActionsObjects) {
            var innerAction = ManagerNN.ActionsObjects[k];
            var counts = 0;
            var keys_count = 0;
            for (var kk in innerAction) {
                if (kk == "ActionValue")
                    continue;
                keys_count++;
                if (innerAction[kk] == action[kk])
                    counts++;
            }
            if (counts == keys_count) // if number of equals == elements count, return true
                return k;
        }
        return "-1";
    }
    static WriteDataToFile() {
        if (!ManagerNN.Preloaded)
            return;
        if (ManagerNN.ActionsObjects.length % 500 == 0) {
            if (!ManagerNN.IsWriting && !ManagerNN.IsReading) {
                ManagerNN.IsWriting = true;
                fs.writeFile(this.DataFolder + "Data.json", JSON.stringify(ManagerNN.ActionsObjects), 'utf8', (err) => { if (err)
                    console.log(err); ManagerNN.IsWriting = false; });
            }
            fs.writeFile(this.DataFolder + "Backup1.json", JSON.stringify(ManagerNN.ActionsObjects), 'utf8', (err) => { if (err)
                console.log(err); });
        }
    }
}
exports.default = ManagerNN;
ManagerNN.ActionsObjects = [];
ManagerNN.IsWriting = false;
ManagerNN.IsReading = false;
ManagerNN.DataFolder = "./_BotData/";
ManagerNN.Preloaded = false;
//# sourceMappingURL=ManagerNN.js.map