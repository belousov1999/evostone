var HeroClass;
(function (HeroClass) {
    HeroClass[HeroClass["None"] = 0] = "None";
    HeroClass[HeroClass["Mage"] = 1] = "Mage";
    HeroClass[HeroClass["Hunter"] = 2] = "Hunter";
    HeroClass[HeroClass["Warrior"] = 3] = "Warrior";
    HeroClass[HeroClass["Shaman"] = 4] = "Shaman";
    HeroClass[HeroClass["Druid"] = 5] = "Druid";
    HeroClass[HeroClass["Priest"] = 6] = "Priest";
    HeroClass[HeroClass["Rogue"] = 7] = "Rogue";
    HeroClass[HeroClass["Paladin"] = 8] = "Paladin";
    HeroClass[HeroClass["Warlock"] = 9] = "Warlock";
})(HeroClass || (HeroClass = {}));
module.exports = HeroClass;
//# sourceMappingURL=HeroClass.js.map