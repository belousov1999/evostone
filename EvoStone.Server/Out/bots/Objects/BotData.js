"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BotDataHelper = require("../Utils/BotDataHelper");
const Option_1 = require("./Option");
const Action_1 = require("../../NeuralNetwork/Objects/Action");
const DataManager_1 = require("../../NeuralNetwork/DataManager");
//import EvoNN from "../../NeuralNetwork/EvoNN";
class BotData {
    constructor(name) {
        this.Name = name;
        this.Actions = [];
        this.Turn = 0;
        this.isGameOver = false;
        this.WinCounts = 0;
    }
    //Calculating best option
    OnTurnStart(scene) {
        if (this.isGameOver == true)
            this.isGameOver = false;
        // console.log("------OnTurnStart---------");
        var best = this.GetBestOption(scene);
        this.LastScene = scene;
        if (best["Option"] !== undefined) {
            this.LastOption = best["Option"];
            this.LastAction = best["Action"];
        }
        else {
            this.LastOption = new Option_1.default("JobsDone", undefined);
            this.LastAction = new Action_1.default(this.Turn, this.LastScene.Self, this.LastScene.Opponent, this.LastOption.Name);
        }
        var value = DataManager_1.default.GetActionValue(this.LastAction.Convert());
        if (value != -1)
            this.LastAction.ActionValue = value;
        //console.log("Try action -> " + this.LastOption.Name);
        // console.log("--------------------------");
        return this.LastOption.Option;
    }
    //Appling option
    OnOptionApply(scene) {
        if (this.Actions === undefined)
            this.Actions = [];
        if (this.LastOption === undefined) {
            //console.log("BotData: Something went wrong. Last action is undefined");
            return;
        }
        var prevSceneValue = this.LastScene.CalculateValue();
        var currentSceneValue = scene.CalculateValue();
        //Update action value 
        var change_value = ((currentSceneValue["Self"] - prevSceneValue["Self"]) - (currentSceneValue["Opponent"] - prevSceneValue["Opponent"])) / 1000;
        if (this.LastAction.OptionName != "JobsDone")
            change_value -= 1 / 1000;
        console.log(this.LastAction.OptionName + ": Last Action Value: " + change_value);
        this.LastAction.UpdateActionValue(this.LastAction.ActionValue + change_value);
        this.Actions.push(this.LastAction);
        DataManager_1.default.AddAction(this.LastAction.Convert());
        // console.log("OnOptionApply: Option: " + this.LastOption.Name + " " + this.LastAction.ActionValue);
        this.Turn++;
    }
    //On jobs done
    OnJobsDone(scene) {
        this.OnOptionApply(scene);
    }
    //On game over
    OnGameOver(isWin) {
        if (this.isGameOver == true)
            return;
        this.isGameOver = true;
        if (isWin) {
            this.WinCounts++;
        }
        for (var k in this.Actions) {
            var action = this.Actions[k];
            action.UpdateActionValue(action.ActionValue + (isWin ? 0.3 : -0.2)); // Add value to all actions per game
            DataManager_1.default.AddAction(action.Convert());
        }
        if (this.Actions !== undefined) {
            this.Actions.length = 0;
            this.Actions = [];
        }
        this.Turn = 0;
    }
    //On bad action
    OnBadOption(scene) {
        //Last option is invalid
        if (this.LastOption === undefined)
            return;
        this.LastAction.UpdateActionValue(this.LastAction.ActionValue - 0.01);
        DataManager_1.default.UpdateActionValue(this.LastAction.Convert());
        this.LastOption = undefined;
        this.LastAction = undefined;
    }
    GetBestOption(scene) {
        var bestOption;
        var bestAction;
        var best_value = 0;
        for (var k in scene.PlayOptions) {
            var option_num = scene.PlayOptions[k];
            var optionsNames = BotDataHelper.ConvertOptionsToName(scene, option_num);
            var option = new Option_1.default(optionsNames, option_num);
            var action = new Action_1.default(this.Turn, scene.Self, scene.Opponent, option.Name);
            var action_value = DataManager_1.default.GetActionValue(action.Convert());
            //console.log(" Value: " + action_value + "   Option: " + option.Name);
            var selfHarm = BotData.isSelfHarm(optionsNames);
            if (selfHarm == true)
                continue;
            if (action_value == -1)
                return { Option: option, Action: action };
            else if (action_value > best_value) {
                best_value = action_value;
                bestOption = option;
                bestAction = action;
            }
        }
        return { Option: bestOption, Action: bestAction };
    }
    static isSelfHarm(optionName) {
        var self_1 = optionName.indexOf("Self");
        if (self_1 == -1 || self_1 == optionName.length)
            return false;
        var self_2 = optionName.indexOf("Self", self_1 + 2);
        if (self_2 == -1 || self_2 == optionName.length)
            return false;
        var shar = optionName.indexOf("��������"); //Self + Self + �������� = true
        if (shar != -1 && shar != optionName.length)
            return true;
        var fire = optionName.indexOf("Power"); //Self + Self + Power = true
        if (fire == -1 || fire == optionName.length)
            return false;
        return true;
    }
}
exports.default = BotData;
//# sourceMappingURL=BotData.js.map