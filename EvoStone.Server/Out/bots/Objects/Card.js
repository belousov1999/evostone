class Card {
    constructor(json) {
        this.RockId = json["RockId"];
        this.Name = json["Name"];
        this.CardId = json["CardId"];
        this.Damage = json["Damage"];
        this.Health = json["Health"];
        this.CardType = json["CardType"];
        this.Cost = json["Cost"];
        this.HasTaunt = json["HasTaunt"];
        this.HasCharge = json["HasCharge"];
        this.Options = [];
        var options = json["Options"];
        for (var k in options)
            if (options[k] != null)
                this.Options.push(new Card(options[k]));
    }
    isEqual(card) {
        //Options equal check
        for (var k in this.Options) {
            if (card.Options[k] !== undefined && !this.Options[k].isEqual(card.Options[k]))
                return false;
        }
        return this.RockId == card.RockId
            && this.Name == card.Name
            && this.CardId == card.CardId
            && this.Damage == card.Damage
            && this.Health == card.Health
            && this.CardType == card.CardType
            && this.Cost == card.Cost
            && this.HasTaunt == card.HasTaunt
            && this.HasCharge == card.HasCharge;
    }
}
module.exports = Card;
//# sourceMappingURL=Card.js.map