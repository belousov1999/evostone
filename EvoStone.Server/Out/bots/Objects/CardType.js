var CardType;
(function (CardType) {
    CardType[CardType["None"] = 0] = "None";
    CardType[CardType["Spell"] = 1] = "Spell";
    CardType[CardType["Enchantment"] = 2] = "Enchantment";
    CardType[CardType["Weapon"] = 3] = "Weapon";
    CardType[CardType["Minion"] = 4] = "Minion";
})(CardType || (CardType = {}));
module.exports = CardType;
//# sourceMappingURL=CardType.js.map