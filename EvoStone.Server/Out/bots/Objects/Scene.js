var _Player = require("./Player");
class Scene {
    constructor(json) {
        this.Self = new _Player(json["Self"]);
        this.Opponent = new _Player(json["Opponent"]);
        this.Turn = json["Turn"];
        this.PlayOptions = json["PlayOptions"];
        this.ActionId = json["ActionId"];
        this.SessionId = json["SessionId"];
    }
    isEqual(scene) {
        if (scene === undefined || scene.Self === undefined)
            return false;
        return this.Self.isEqual(scene.Self)
            && this.Opponent.isEqual(scene.Opponent)
            && this.Turn == scene.Turn
            && this.ActionId == scene.ActionId
            && this.SessionId == scene.SessionId;
    }
    CalculateValue() {
        var self_value = this.Self.CalculateValue();
        var opponent_value = this.Opponent.CalculateValue();
        return { Self: self_value, Opponent: opponent_value };
    }
}
module.exports = Scene;
//# sourceMappingURL=Scene.js.map