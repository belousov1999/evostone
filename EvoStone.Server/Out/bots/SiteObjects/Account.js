"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var accStatus = [
    "Startup",
    "InGame",
    "WaitingToStart",
    "Relaunch"
];
class Account {
    constructor(playerName = "", email = "", pass = "", winCount = 0, isReady = false, timeSinceReady = 0) {
        this.playerName = playerName;
        this.accountEmail = email.length == 0 ? "Empty" : email;
        this.accountPass = pass;
        this.status = accStatus[0];
        this.online = true;
        this.matchWin = winCount;
        this.readyToSearch = isReady;
        this.timeSinceReady = timeSinceReady;
        this.medalNumber = "Empty";
        this.version = 2;
    }
}
exports.default = Account;
//# sourceMappingURL=Account.js.map