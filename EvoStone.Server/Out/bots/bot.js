"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base = require('../base/bot');
const _Scene = require("../bots/Objects/Scene");
var Log = require("../bots/Utils/Log");
const BotDataManager_1 = require("./BotDataManager");
const BotUtil_1 = require("../bots/Utils/BotUtil");
class RockBot extends base.RockBotBase {
    get_mulligan_action(scene) {
        //console.log("get_mulligan_action");
        return BotUtil_1.default.CreateEmptyAction();
    }
    get_play_action(scene) {
        // Log.Print("get_play_action");
        try {
            var objScene = new _Scene(scene);
            this.actionScene = objScene;
            var action = BotDataManager_1.default.OnTurnStart(objScene);
            // console.log("Action: " + action);
            if (typeof (action) == "object")
                return BotUtil_1.default.create_action(action);
            else if (action == -10)
                return BotUtil_1.default.create_concide_action();
        }
        catch (e) {
            console.log(e);
        }
        return BotUtil_1.default.CreateEmptyAction();
    }
    on_report(scene) {
        //  Log.Print("on_report");
        var objScene = new _Scene(scene);
        if (objScene.isEqual(this.actionScene)) // IsBadAction?
         {
            BotDataManager_1.default.OnBadOption(objScene);
            return;
        }
        BotDataManager_1.default.OnOptionApply(objScene);
    }
    on_game_over(scene) {
        // Log.Print("on_game_over");
        var objScene = new _Scene(scene);
        BotDataManager_1.default.OnGameOver(objScene);
    }
    on_jobs_done(scene) {
        //Log.Print("on_jobs_done");
        var objScene = new _Scene(scene);
        BotDataManager_1.default.OnJobsDone(objScene);
    }
    on_ready_play(readyplay) {
        try {
            var canplay = BotDataManager_1.default.OnPlayButton(readyplay["nickname"], readyplay["medalLevel"], readyplay["email"]);
            return { ready: canplay };
        }
        catch (e) {
            console.log("on_ready_play:" + e);
        }
        return { ready: true };
    }
}
module.exports = RockBot;
//# sourceMappingURL=bot.js.map