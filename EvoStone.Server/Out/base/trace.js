/**
 * Tracer for RockApiService.
 */
var debug = false,
	debug_onlyInfo = true;

class RockTrace {
    static trace(data) {
    	if((debug!=true) 
    		|| (debug_onlyInfo && data['Level'] != "Info")) return;
        console.log('[' + data['Level'] + "]: " + data['Message']);
    }
}

var exports = module.exports = {
    RockTrace:RockTrace
};