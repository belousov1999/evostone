/**
 * Base class for RockBot
 */
class RockBotBase {
    get_mulligan_action(scene) {
        throw new Error("Not Implemented.")
    }

    get_play_action(scene) {
        throw new Error("Not Implemented.")
    }

    on_report(scene) {
        throw new Error("Not Implemented.")
    }

    on_game_over(scene) {
        throw new Error("Not Implemented.")
    }

    on_jobs_done(scene) {
        throw new Error("Not Implemented.")
    }

    on_ready_play(nickname) {
        throw new Error("Not Implemented.")
    }
}

var exports = module.exports = {
    RockBotBase:RockBotBase
};