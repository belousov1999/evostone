var fs = require('fs');
const brain = require('brain.js/browser');

export default class EvoNN {
    private SaveFolderName: string;
    private save_file: string; // path to save file
    private testData_file: string; // path to save file
    private net: any; // NN object
    private DataFolder: string;
    private DataFileName: string;
    private outputValues: string[];
    private AllData: object;
    public Trained: boolean;

    constructor(outValues: string[]) {
        this.SaveFolderName = "./_SaveData/";

        this.net = new brain.NeuralNetwork();
        this.save_file = this.SaveFolderName + 'NNsave.json';
        this.testData_file = this.SaveFolderName + 'TestData.json';
        this.DataFolder = "./_BotData/";
        this.DataFileName = "data.json";
        this.outputValues = outValues;
        this.Trained = false;
    }

    public TrainNN(json_data) {
        this.net.train(json_data, {
            iterations: 10000,
            log: true,
            callbackPeriod: 10,
            logPeriod: 10,
        })
    }

    public Run(data: object): object {
        const output = this.net.run(data)
        var deconvert = EvoNN.DeconvertOutputValues(this.AllData[0], output, this.outputValues);

        console.log(JSON.stringify(data) + " --> " + JSON.stringify(deconvert))
        return deconvert;
    }

    private GetFilesData() {
        var all_data = []
        fs.readdirSync(this.DataFolder).forEach(file => {
            if (file !== this.DataFileName)
                return;
            var _FileData = JSON.parse(fs.readFileSync(this.DataFolder + file, 'utf8')) // inner file strings
            all_data.push(_FileData);
        });
        return all_data;
    }

    private static CalculateMinMax(d) {
        var min = [];
        var max = [];
        for (var k in d)
            for (var kk in d[k]) {
                var v = d[k][kk];
                if (min[kk] === undefined) {
                    min[kk] = v;
                    max[kk] = v;
                }
                if (v < min[kk])
                    min[kk] = v;
                if (v > max[kk])
                    max[kk] = v;
            }
        return { "min": min, "max": max };
    }

    private static ConvertDataToNNRange(data) { //Make data between 0 and 1
        //Find minus minimum value
        var minmax = EvoNN.CalculateMinMax(data);
        //List with only positive values
        var plusData = [];
        for (var k in data)
            for (var kk in data[k]) {
                var vv = data[k][kk];
                if (plusData[k] === undefined)
                    plusData[k] = [];
                if (minmax.min[kk] < 0)
                    plusData[k][kk] = vv + Math.abs(minmax.min[kk]);
                else
                    plusData[k][kk] = vv;
            }
        //Find max value in positive list
        minmax = EvoNN.CalculateMinMax(plusData);
        //Divide positive list by max value
        var rangeData = [];
        for (var k in plusData)
            for (var kk in plusData[k]) {
                var vv = plusData[k][kk];
                if (rangeData[k] === undefined)
                    rangeData[k] = {};
                if (minmax.max[kk] <= 1.0)
                    rangeData[k][kk] = vv;
                else
                    rangeData[k][kk] = vv / minmax.max[kk];
            }
        return rangeData;
    }

    private static IsOutputKey(outValues, key): boolean {
        for (var k in outValues) {
            if (outValues[k] == key)
                return true;
        }
        return false;
    }

    private static DeconvertOutputValues(data, dataToConvert, outputValues: string[]) { // return output data to original format
        var minmax_minus = EvoNN.CalculateMinMax(data);
        //List with only positive values
        var plusData = [];
        for (var k in data)
            for (var kk in data[k]) {
                var vv = data[k][kk];
                if (plusData[k] === undefined)
                    plusData[k] = [];
                if (minmax_minus.min[kk] < 0)
                    plusData[k][kk] = vv + Math.abs(minmax_minus.min[kk]);
                else
                    plusData[k][kk] = vv;
            }
        //Find max value in positive list
        var minmax_plus = EvoNN.CalculateMinMax(plusData);

        var converted = {};
        for (var k in dataToConvert) {
            var vv = dataToConvert[k];
            if (!EvoNN.IsOutputKey(outputValues, k)) continue;
            if (minmax_plus.max[k] <= 1.0)
                converted[k] = vv;
            else
                converted[k] = vv * minmax_plus.max[k];
            if (minmax_minus.min[k] < 0)
                converted[k] -= Math.abs(minmax_minus.min[k]);
        }
        return converted;
    }

    private static ConvertToTrainData(data, outputValues: string[]) {
        var trainData = [];
        for (var k in data) {
            var inputData = {};
            var outpudData = {};
            for (var kk in data[k]) {
                if (EvoNN.IsOutputKey(outputValues, kk))
                    outpudData[kk] = data[k][kk];
                else
                    inputData[kk] = data[k][kk];
            }
            trainData.push({ input: inputData, output: outpudData });
        }
        return trainData;
    }


    public TrainFromData() {
        this.AllData = this.GetFilesData();
        var rangeData = EvoNN.ConvertDataToNNRange(this.AllData[0]);
        fs.writeFile(this.testData_file, JSON.stringify(rangeData), 'utf8', (err) => { if (err !== undefined) console.log(err); });
        var converted = EvoNN.ConvertToTrainData(rangeData, this.outputValues);
        fs.writeFile(this.SaveFolderName + "Converted.json", JSON.stringify([converted]), 'utf8', (err) => { if (err !== undefined) console.log(err); });
        this.TrainNN(converted);
        this.Trained = true;
        /*
        var out = this.Run({ "windowSizeX": 1, "windowSizeY": 1, "targetPosX": 1, "targetPosY": 0.9850746268656716, "targetPosZ": 1, "cameraPosX": 0.9962894248608535, "cameraPosY": 1, "cameraPosZ": 0.9714285714285714, "cameraMatrixX1": 0.5199966757583517, "cameraMatrixX2": 0, "cameraMatrixX3": 0.195455659, "cameraMatrixY1": 1, "cameraMatrixY2": 0.5192484160462499, "cameraMatrixY3": 0, "cameraMatrixZ1": 0.5192938481, "cameraMatrixZ2": 0.116755426, "cameraMatrixZ3": 0.92487824 });
        var deconvert = EvoNN.DeconvertOutputValues(this.AllData[0], out, this.outputValues);
        fs.writeFile(this.SaveFolderName + "Deconverted.json", JSON.stringify(deconvert), 'utf8', (err) => { if (err !== undefined) console.log(err); });
        
        return deconvert;
        */
    }

    public SaveTrainData(net_data) {
        fs.writeFile(this.save_file, JSON.stringify(net_data), 'utf8', (err) => { });
    }

    public LoadTrainData() {
        var json = fs.readFileSync(this.save_file, 'utf8')
        this.net.fromJSON(JSON.parse(json))
    }
}


