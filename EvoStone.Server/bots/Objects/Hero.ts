
class Hero {
    public RockId: number;
    public Name: string;
    public CardId: string;
    public Class: HeroClass;
    public Damage: number;
    public Health: number;
    public CanAttack: boolean;
    public IsExhausted: boolean;
    public IsQuest: boolean;
    public IsSecret: boolean;

    constructor(json: object) {
        this.RockId = json["RockId"];
        this.Name = json["Name"];
        this.CardId = json["CardId"];
        this.Class = json["Class"];
        this.Damage = json["Damage"];
        this.Health = json["Health"];
        this.CanAttack = json["CanAttack"];
        this.IsExhausted = json["IsExhausted"];
        this.IsQuest = json["IsQuest"];
        this.IsSecret = json["IsSecret"];
    }

    public isEqual(hero: Hero): boolean {
        return this.RockId == hero.RockId
            && this.Name == hero.Name
            && this.CardId == hero.CardId
            && this.Class == hero.Class
            && this.Damage == hero.Damage
            && this.Health == hero.Health
            && this.CanAttack == hero.CanAttack
            && this.IsExhausted == hero.IsExhausted
            && this.IsQuest == hero.IsQuest
            && this.IsSecret == hero.IsSecret;
    }
}

module.exports = Hero;