var _Card = require("./Card");
var _Hero = require("./Hero");
var _Minion = require("./Minion");

class Player {
    public NickName: string;
    public MedalLevel: number;
    public GameMode: number;
    public Resources: number;
    public PermanentResources: number;
    public TemporaryResources: number;
    public PowerAvailable: boolean;
    public HasWeapon: boolean;
    public Hero: Hero;
    public Power: Card;
    public Weapon: Card;
    public Minions: Minion[];
    public Cards: Card[];
    public Choices: Card[];

    constructor(json: object) {
        this.NickName = json["NickName"];
        this.MedalLevel = json["MedalLevel"];
        this.GameMode = json["GameMode"];
        this.Resources = json["Resources"];
        this.PermanentResources = json["PermanentResources"];
        this.TemporaryResources = json["TemporaryResources"];
        this.PowerAvailable = json["PowerAvailable"];
        this.Hero = new _Hero(json["Hero"]);
        if (json["Power"] != null)
            this.Power = new _Card(json["Power"]);
        if (json["Weapon"] != null)
            this.Weapon = new _Card(json["Weapon"]);
        //Minions
        this.Minions = [];
        var minions = json["Minions"];
        for (var k in minions)
            if (minions[k] != null)
                this.Minions.push(new _Minion(minions[k]));
        //Cards
        this.Cards = [];
        var cards = json["Cards"];
        for (var k in cards)
            if (cards[k] != null)
                this.Cards.push(new _Card(cards[k]));
        //Choices
        this.Choices = json["Choices"];
        this.Choices = [];
        var choices = json["Choices"];
        for (var k in choices)
            if (choices[k] != null)
                this.Choices.push(new _Card(choices[k]));
    }

    public isEqual(player: Player): boolean {
        //Minions equal check
        for (var k in this.Minions) {
            if (player.Minions[k] !== undefined && !this.Minions[k].isEqual(player.Minions[k]))
                return false;
        }
        //Cards equal check
        for (var k in this.Cards) {
            if (player.Cards[k] !== undefined && !this.Cards[k].isEqual(player.Cards[k]))
                return false;
        }
        //Choices equal check
        for (var k in this.Choices) {
            if (player.Choices[k] !== undefined && !this.Choices[k].isEqual(player.Choices[k]))
                return false;
        }

        return this.NickName == player.NickName
            && this.GameMode == player.GameMode
            && this.Resources == player.Resources
            && this.PermanentResources == player.PermanentResources
            && this.TemporaryResources == player.TemporaryResources
            && this.PowerAvailable == player.PowerAvailable
            && this.HasWeapon == player.HasWeapon
            && this.Hero.isEqual(player.Hero)
            && this.Power.isEqual(player.Power)
            && this.Weapon.isEqual(player.Weapon);
    }

    public CalculateValue(): number {
        var cardsCount = this.Cards.length;
        var weaponValue = 0;
        if (this.HasWeapon)
            weaponValue = this.Weapon.Damage + this.Weapon.Health;
        var hero = this.Hero.Damage + this.Hero.Health;
        var minions = 0;
        for (var k in this.Minions) {
            var minion = this.Minions[k];
            minions += minion.Health
                + minion.Damage
                + (minion.HasDeathrattle ? 1.0 : 0)
                + (minion.HasAura ? 1.0 : 0)
                + (minion.HasWindfury ? minion.Damage + 1.0 : 0)
                + (minion.HasTaunt ? 1.0 : 0);
        }
        var resources = this.Resources + this.TemporaryResources + this.PermanentResources;
        return cardsCount + weaponValue + hero + minions + resources;
    }
}

module.exports = Player;