class Minion {
    public RockId: number;
    public Name: string;
    public CardId: string;
    public Damage: number;
    public Health: number;
    public BaseHealth: number;
    public Race: Race;
    public IsFrozen: boolean;
    public IsExhausted: boolean;
    public IsAsleep: boolean;
    public IsStealthed: boolean;
    public CanAttack: boolean;
    public CanBeAttacked: boolean;
    public HasTaunt: boolean;
    public HasWindfury: boolean;
    public HasDivineShield: boolean;
    public HasAura: boolean;
    public IsEnraged: boolean;
    public HasTriggerVisual: boolean;
    public HasInspire: boolean;
    public HasDeathrattle: boolean;
    public HasBattlecry: boolean;
    public HasLifesteal: boolean;
    public IsPoisonous: boolean;

    constructor(json:object) {
        this.RockId = json["RockId"];
        this.Name = json["Name"];
        this.CardId = json["CardId"];
        this.Damage = json["Damage"];
        this.Health = json["Health"];
        this.BaseHealth = json["BaseHealth"];
        this.Race = json["Race"];
        this.IsFrozen = json["IsFrozen"];
        this.IsExhausted = json["IsExhausted"];
        this.IsAsleep = json["IsAsleep"];
        this.IsStealthed = json["IsStealthed"];
        this.CanAttack = json["CanAttack"];
        this.CanBeAttacked = json["CanBeAttacked"];
        this.HasTaunt = json["HasTaunt"];
        this.HasWindfury = json["HasWindfury"];
        this.HasDivineShield = json["HasDivineShield"];
        this.HasAura = json["HasAura"];
        this.IsEnraged = json["IsEnraged"];
        this.HasTriggerVisual = json["HasTriggerVisual"];
        this.HasInspire = json["HasInspire"];
        this.HasDeathrattle = json["HasDeathrattle"];
        this.HasBattlecry = json["HasBattlecry"];
        this.HasLifesteal = json["HasLifesteal"];
        this.IsPoisonous = json["IsPoisonous"];
    }

    public isEqual(minion: Minion): boolean {
        return this.RockId == minion.RockId
            && this.Name == minion.Name
            && this.CardId == minion.CardId
            && this.Damage == minion.Damage
            && this.Health == minion.Health
            && this.BaseHealth == minion.BaseHealth
            && this.Race == minion.Race
            && this.IsFrozen == minion.IsFrozen
            && this.IsExhausted == minion.IsExhausted
            && this.IsAsleep == minion.IsAsleep
            && this.IsStealthed == minion.IsStealthed
            && this.CanAttack == minion.CanAttack
            && this.CanBeAttacked == minion.CanBeAttacked
            && this.HasTaunt == minion.HasTaunt
            && this.HasWindfury == minion.HasWindfury
            && this.HasDivineShield == minion.HasDivineShield
            && this.HasAura == minion.HasAura
            && this.IsEnraged == minion.IsEnraged
            && this.HasTriggerVisual == minion.HasTriggerVisual
            && this.HasInspire == minion.HasInspire
            && this.HasDeathrattle == minion.HasDeathrattle
            && this.HasBattlecry == minion.HasBattlecry
            && this.HasLifesteal == minion.HasLifesteal
            && this.IsPoisonous == minion.IsPoisonous;
    }
}

module.exports = Minion;
