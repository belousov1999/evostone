
export default class ReadyPlay {
    public NickName: string;
    public MedalLevel: string;
    public Email: string;
    public LastTime: number;
    public StartSearchTime: number;
    public Searching: boolean;

    constructor(nickname: string, medalLevel: string, email: string, lastTime: number) {
        this.NickName = nickname;
        this.MedalLevel = medalLevel;
        this.Email = email;
        this.LastTime = lastTime;
        this.StartSearchTime = lastTime;
        this.Searching = true;
    }

    public IsEqual(nickName: string): boolean {
        return this.NickName == nickName;
    }

    public UpdateTime(lastTime: number): void {
        this.LastTime = lastTime;
        if (!this.IsTimeInRange(lastTime, 20))
            this.StartSearchTime = lastTime;
    }

    public IsTimeInRange(time: number, range: number): boolean {
        return time - this.LastTime <= range;
    }

    public IsWaitingTooLong(time: number): boolean {
        console.log("Waiting too long: " + (time - this.StartSearchTime));
        return time - this.StartSearchTime >= 600;
    }
}