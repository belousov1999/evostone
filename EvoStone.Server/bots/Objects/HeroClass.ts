enum HeroClass {
    None,
    Mage,
    Hunter,
    Warrior,
    Shaman,
    Druid,
    Priest,
    Rogue,
    Paladin,
    Warlock
}

module.exports = HeroClass;