
var accStatus = [
    "Startup",
    "InGame",
    "WaitingToStart",
    "Relaunch"
]

export default class Account {
    public playerName: string;
    public accountEmail: string;
    public accountPass: string;
    public status: string;
    public online: boolean;
    public matchWin: number;
    public readyToSearch: boolean;
    public timeSinceReady: number;
    public medalNumber: string;
    public version: number;

    constructor(playerName = "", email = "", pass = "", winCount = 0, isReady = false, timeSinceReady = 0) {
        this.playerName = playerName;
        this.accountEmail = email.length == 0 ? "Empty" : email;
        this.accountPass = pass;
        this.status = accStatus[0];
        this.online = true;
        this.matchWin = winCount;
        this.readyToSearch = isReady;
        this.timeSinceReady = timeSinceReady;
        this.medalNumber = "Empty";

        this.version = 2;
    }
}