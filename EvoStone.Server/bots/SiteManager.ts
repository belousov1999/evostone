var io = require('socket.io-client');
import Account from "./SiteObjects/Account";

export default class SiteManager {
    //Public
    public connected: boolean;

    //Private
    private socket;

    //Public Static
    public static Instance: SiteManager;

    constructor() {
        SiteManager.Instance = this;
        this.socket = io.connect('http://192.168.0.100:80', { reconnect: true });
        // Add a connect listener
        this.socket.on('connect', function (socket) {
            console.log('Connected to Site!');
            SiteManager.Instance.connected = true;
        });
        this.socket.on('disconnect', function (socket) {
            console.log('Disconnected from Site!');
            SiteManager.Instance.connected = false;
        });
    }

    public SendDataToSite(accs: Account[]): void {
        if (SiteManager.Instance.connected === undefined
            || SiteManager.Instance.connected == false) return;
        this.socket.emit('DataUpdate', accs);
    }
}



