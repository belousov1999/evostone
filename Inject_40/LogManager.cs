﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Inject_40
{
    public static class LogManager
    {
        public static string CurrentDir { get; private set; } = "";
        public const string LogFile = "RockUnityLOG.txt";
        public const string SaveInfoFile = "SaveInfo.txt";
        public const string ErrorFile = "Error.txt";
        public const string ReadFile = "Read.txt";

        public static void WriteToFile(string s)
        {
            if (CurrentDir == "")
                CurrentDir = Directory.GetCurrentDirectory() + @"\";
            using (StreamWriter w = File.AppendText(CurrentDir + LogFile))
                w.Write(s + "\n");
        }

        public static void WriteToErrorFile(string s)
        {
            if (CurrentDir == "")
                CurrentDir = Directory.GetCurrentDirectory() + @"\";
            using (StreamWriter w = File.AppendText(CurrentDir + ErrorFile))
                w.Write(s + "\n");
        }

        public static void WriteSaveInfo(string s)
        {
            if (CurrentDir == "")
                CurrentDir = Directory.GetCurrentDirectory() + @"\";
            using (StreamWriter w = File.AppendText(CurrentDir + SaveInfoFile))
                w.Write(s + "\n");
        }

        public static string ReadFromFile(string filename)
        {
            if (CurrentDir == "")
                CurrentDir = Directory.GetCurrentDirectory() + @"\";
            return File.ReadAllText(CurrentDir + filename);
        }
    }
}
