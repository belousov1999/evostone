const fs = require('fs')
import * as XlsxPopulate from "xlsx-populate"
/*
var fileName = "testFile.XLSX";
const workbook = new ExcelJS.Workbook();
var loaded = false, writing = false, hasNewData = false;
async function GetOrCreateSheet(workbook: any, fileName: string, sheetName: string): Promise<any> {
    if (loaded) {
        Promise.resolve(workbook.getWorksheet(sheetName));
        return;
    }

    try {
        await workbook.xlsx.readFile(fileName);
        loaded = true;
    } catch (e) {}


    var worksheet = workbook.getWorksheet(sheetName);
    if (worksheet === undefined) {
        worksheet = workbook.addWorksheet(sheetName, { properties: { tabColor: { argb: 'FFC0000' } } });
        loaded = true;
    }

    worksheet.columns = [
        { header: 'Login', key: 'login', width: 32, horizontal: "left" },
        { header: 'Pass', key: 'pass', width: 32, horizontal: "left" },
        { header: 'Sand', key: 'sandnumber', width: 10, horizontal: "left" },
        { header: 'Coloda', key: 'coloda', width: 70, horizontal: "left" },
        { header: 'Rank', key: 'rank', width: 17, horizontal: "left" },
        { header: 'Wins', key: 'wins', width: 10, horizontal: "left" },
        { header: 'Levels', key: 'levels', width: 10, horizontal: "left" },
        { header: 'Packs', key: 'packs', width: 10, horizontal: "left" },
        { header: 'Gold', key: 'golds', width: 10, horizontal: "left" },
        { header: 'Status', key: 'status', width: 20, horizontal: "left" },
    ];

    for (var i = 1; i <= 10; i++) {
        var col = worksheet.getColumn(i);
        col.border = {
            top: { style: 'thin', color: { argb: '000000' } },
            left: { style: 'thin', color: { argb: '000000' } },
            bottom: { style: 'thin', color: { argb: '000000' } },
            right: { style: 'thin', color: { argb: '000000' } },
        };
    }
    worksheet.getColumn("login").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '92D050' }
    };
    worksheet.getColumn("pass").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '92D050' }
    };
    worksheet.getColumn("sandnumber").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '00B050' }
    };
    worksheet.getColumn("coloda").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '00B050' }
    };
    worksheet.getColumn("rank").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFD966' }
    };
    worksheet.getColumn("wins").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '0070C0' }
    };
    worksheet.getColumn("levels").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '0070C0' }
    };
    worksheet.getColumn("packs").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '0070C0' }
    };
    worksheet.getColumn("golds").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '0070C0' }
    };
    worksheet.getColumn("status").fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '0070C0' }
    };

    return Promise.resolve(worksheet);
}

async function WriteData(){
    if (!writing) {
        hasNewData = false;
        writing = true;
        await workbook.xlsx.writeFile(fileName);
        writing = false;
        console.log("Excel updated");
        if (hasNewData)
            WriteData();
    } else {
        hasNewData = true;
    }
}

async function ExcelTest(login: string, pass: string, sandNumber: number, coloda: string, rank: number,
    wins: number, levels: number, packs: number, gold: number, status: string) {


    var worksheet = await GetOrCreateSheet(workbook, fileName, "HearthStone")

    const loginCol = await worksheet.getColumn("login");

    var targetRowNumber = -1;
    for (var i = 1; i < 999; i++) {
        var loginField_value = loginCol.values[i];
        console.log(i + " - " + loginField_value);
        if (loginField_value === undefined || loginField_value == login) {
            targetRowNumber = i;
            break;
        }
    }
    if (targetRowNumber == -1) {
        console.log("Not found row for login: " + login);
        return;
    }
    var targetRow = worksheet.getRow(targetRowNumber);
    targetRow.getCell(1).value = login;
    targetRow.getCell(2).value = pass;
    targetRow.getCell(3).value = sandNumber;
    targetRow.getCell(4).value = coloda;
    targetRow.getCell(5).value = rank;
    targetRow.getCell(6).value = wins;
    targetRow.getCell(7).value = levels;
    targetRow.getCell(8).value = packs;
    targetRow.getCell(9).value = gold;
    targetRow.getCell(10).value = status;

    for (var i = 2; i < 999; i++) {
        var loginField_value = loginCol.values[i];
        if (loginField_value === undefined)
            break;
        var row = worksheet.getRow(i);
        var statusValue = row.getCell("status").value;
        console.log("Row: " + i + " Status: " + statusValue);
        if (statusValue != "Playing" && statusValue != "Selling") {
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '595959' }
            };
        }
    }
    WriteData();
}
*/
var colors = [
    '92D050',
    '92D050',
    '00B050',
    '00B050',
    'FFD966',
    '0070C0',
    '0070C0',
    '0070C0',
    '0070C0',
    'FFD966'
]
var widths = [
    32, 32, 10, 70, 17, 10, 10, 10, 10, 20
]

async function GetOrCreateWorkbook(file: string): Promise<XlsxPopulate.Workbook> {
    var workbook;
    try {
        if (fs.existsSync(file)) {
            workbook = await XlsxPopulate.fromFileAsync(file);
        }
    } catch (err) {
    }
    if (workbook === undefined)
        workbook = await XlsxPopulate.fromBlankAsync();
    return Promise.resolve(workbook);
}

var fileName = "./HearthStone.xlsx";
var WritingToFile = false, HasNewData = false;
var workbook: XlsxPopulate.Workbook;
async function WriteData(file: string): Promise<void> {
    try {
        if (!WritingToFile) {
            HasNewData = false;
            WritingToFile = true;
            await workbook.toFileAsync(file);
            WritingToFile = false;
            console.log("Excel updated");
            if (HasNewData)
                WriteData(file);
        } else {
            HasNewData = true;
        }
    } catch (e) {
        HasNewData = false;
        WritingToFile = false;
    }
}

async function ExcelTest(login: string, pass: string, sandNumber: string, coloda: string, rank: string,
    wins: number, levels: number, packs: number, gold: number, status: string) {

    workbook = await GetOrCreateWorkbook(fileName);
    var sheet = workbook.sheet("HearthStone");

    if (sheet === undefined) {
        sheet = workbook.addSheet('HearthStone');
        for (var i = 1; i < colors.length + 1; i++) {
            sheet.column(i).width(widths[i]).style("fill", {
                type: "pattern",
                pattern: "solid",
                foreground: {
                    rgb: colors[i - 1]
                }
            }).style("border", true);
        }
        sheet.column(1).cell(1).value("EMAIL").style("bold");
        sheet.column(2).cell(1).value("PASS").style("bold");
        sheet.column(3).cell(1).value("SAND").style("bold");
        sheet.column(4).cell(1).value("COLODA").style("bold");
        sheet.column(5).cell(1).value("RANK").style("bold");
        sheet.column(6).cell(1).value("WINS").style("bold");
        sheet.column(7).cell(1).value("LEVELS").style("bold");
        sheet.column(8).cell(1).value("PACKS").style("bold");
        sheet.column(9).cell(1).value("GOLDS").style("bold");
        sheet.column(10).cell(1).value("STATUS").style("bold");
    }

    var loginCol = sheet.column(1);
    var targetRowNumber = -1;
    for (var i = 2; i < 999; i++) {
        var loginField_value = loginCol.cell(i).value();
        console.log(i + " - " + loginField_value);
        if (loginField_value === undefined || loginField_value + "" == login) {
            targetRowNumber = i;
            break;
        }
    }
    if (targetRowNumber == -1) {
        console.log("Not found row for login: " + login);
        return;
    }

    var targetRow = sheet.row(targetRowNumber);

    var grayed = true;
    if (status == "Play" || status == "Selling")
        grayed = false;

    SetCellValue(targetRow, 1, login, grayed);
    SetCellValue(targetRow, 2, pass, grayed);
    SetCellValue(targetRow, 3, sandNumber, grayed);
    SetCellValue(targetRow, 4, coloda, grayed);
    SetCellValue(targetRow, 5, rank, grayed);
    SetCellValue(targetRow, 6, wins + "", grayed);
    SetCellValue(targetRow, 7, levels + "", grayed);
    SetCellValue(targetRow, 8, packs + "", grayed);
    SetCellValue(targetRow, 9, gold + "", grayed);
    if (!(targetRow.cell(10).value() + "").includes("/"))
        SetCellValue(targetRow, 10, status, grayed);

    await WriteData(fileName);
    console.log("Excel update");
}

function SetCellValue(row: XlsxPopulate.Row, colNumber: number, value: string, grayed: boolean) {
    var cell = row.cell(colNumber).value(value);
    var color;
    if (grayed)
        color = '595959';
    else
        color = colors[colNumber - 1];
    cell.style("fill", {
        type: "pattern",
        pattern: "solid",
        foreground: {
            rgb: color
        }
    });
}

try {
    ExcelTest("testLogin", "testPass", "", "-", "RANK 0", 0, 100, 1, 100, "Play");
    // ExcelTest("MyBestLogin", "testPass", 1, "-", -1, 100, 1, 1, 1, "Bought");
    // ExcelTest("Asbs@mail.ru", "testPass", 1, "-", -1, 100, 1, 1, 1, "Playing");
    // TestDataManager.FindSimilars();

} catch (e) {
    console.log("Error: " + e);
}
console.log('End...');

require('readline')
    .createInterface(process.stdin, process.stdout)
    .question("Press [Enter] to exit...", function () {
        process.exit();
    });