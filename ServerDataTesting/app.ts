console.log('Start...');
import DataManager from "./src/DataManager"
import TestDataManager from "./src/TestDataManager"




async function init() {
    DataManager.AddAction({
        "Action_Self_Cards_���� ������������": 1,
        "ActionValue": 0.15100000000000002,
        "Self_PowerAvailable": 1,
        "Self_HeroClass_1": 1,
        "Self_Resources": 3,
        "Self_PermanentResources": 0,
        "Self_TemporaryResources": 0,
        "Self_Cards_���� �������� ����": 1,
        "Self_Cards_��������� �������": 1,
        "Self_Cards_����������� ������": 1,
        "Self_Cards_������-��������": 1,
        "Self_Cards_�������": 1,
        "Self_Cards_��� �������� ������": 1,
        "Self_Cards_�������� ����������": 1,
        "Opponent_PowerAvailable": 1,
        "Opponent_HeroClass_4": 1,
        "Opponent_Minions_�������� ���������_Damage": 3,
        "Opponent_Minions_�������� ���������_Health": 2,
        "Opponent_Minions_�������� ���������_Taunt": 0
    });
    await sleep(500);
    DataManager.AddAction({
        "Action_Self_Cards_�������� ����": 1,
        "ActionValue": 0.14700000000000002,
        "Self_PowerAvailable": 1,
        "Self_HeroClass_1": 1,
        "Self_Resources": 1,
        "Self_PermanentResources": 0,
        "Self_TemporaryResources": 0,
        "Self_Cards_��������� �������": 1,
        "Self_Cards_�������� ���������": 1,
        "Self_Cards_������ ����� �������": 1,
        "Self_Cards_�������� ����": 1,
        "Opponent_PowerAvailable": 1,
        "Opponent_HeroClass_5": 1
    });
    await sleep(100);
    console.log(DataManager.GetActionValue({
        "Action_Self_Cards_�������� ����": 1,
        "Self_PowerAvailable": 1,
        "Self_HeroClass_1": 1,
        "Self_Resources": 1,
        "Self_PermanentResources": 0,
        "Self_TemporaryResources": 0,
        "Opponent_PowerAvailable": 1,
        "Opponent_HeroClass_5": 1
    }));
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
} 

try {
    init();


   // TestDataManager.FindSimilars();

} catch (e) {
    console.log("Error: "+e);
}
console.log('End...');


require('readline')
    .createInterface(process.stdin, process.stdout)
    .question("Press [Enter] to exit...", function () {
        process.exit();
    });