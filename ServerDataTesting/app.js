"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
console.log('Start...');
const DataManager_1 = require("./src/DataManager");
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        DataManager_1.default.AddAction({
            "Action_Self_Cards_���� ������������": 1,
            "ActionValue": 0.15100000000000002,
            "Self_PowerAvailable": 1,
            "Self_HeroClass_1": 1,
            "Self_Resources": 3,
            "Self_PermanentResources": 0,
            "Self_TemporaryResources": 0,
            "Self_Cards_���� �������� ����": 1,
            "Self_Cards_��������� �������": 1,
            "Self_Cards_����������� ������": 1,
            "Self_Cards_������-��������": 1,
            "Self_Cards_�������": 1,
            "Self_Cards_��� �������� ������": 1,
            "Self_Cards_�������� ����������": 1,
            "Opponent_PowerAvailable": 1,
            "Opponent_HeroClass_4": 1,
            "Opponent_Minions_�������� ���������_Damage": 3,
            "Opponent_Minions_�������� ���������_Health": 2,
            "Opponent_Minions_�������� ���������_Taunt": 0
        });
        yield sleep(500);
        DataManager_1.default.AddAction({
            "Action_Self_Cards_�������� ����": 1,
            "ActionValue": 0.14700000000000002,
            "Self_PowerAvailable": 1,
            "Self_HeroClass_1": 1,
            "Self_Resources": 1,
            "Self_PermanentResources": 0,
            "Self_TemporaryResources": 0,
            "Self_Cards_��������� �������": 1,
            "Self_Cards_�������� ���������": 1,
            "Self_Cards_������ ����� �������": 1,
            "Self_Cards_�������� ����": 1,
            "Opponent_PowerAvailable": 1,
            "Opponent_HeroClass_5": 1
        });
        yield sleep(100);
        console.log(DataManager_1.default.GetActionValue({
            "Action_Self_Cards_�������� ����": 1,
            "Self_PowerAvailable": 1,
            "Self_HeroClass_1": 1,
            "Self_Resources": 1,
            "Self_PermanentResources": 0,
            "Self_TemporaryResources": 0,
            "Opponent_PowerAvailable": 1,
            "Opponent_HeroClass_5": 1
        }));
    });
}
function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}
try {
    init();
    // TestDataManager.FindSimilars();
}
catch (e) {
    console.log("Error: " + e);
}
console.log('End...');
require('readline')
    .createInterface(process.stdin, process.stdout)
    .question("Press [Enter] to exit...", function () {
    process.exit();
});
//# sourceMappingURL=app.js.map