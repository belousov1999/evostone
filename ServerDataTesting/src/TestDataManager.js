"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
class TestDataManager {
    static FindSimilars() {
        var file_path = this.DataFolder + "Data.json";
        var data = fs.readFileSync(file_path, "utf8");
        var data_json = JSON.parse(data);
        console.log("Finding...");
        for (var k in data_json) {
            for (var kk in data_json) {
                if (k != kk && TestDataManager.IsSimilar(data_json[k], data_json[kk])) {
                    console.log(JSON.stringify(data_json[k]));
                    console.log("----------------------------");
                    console.log(JSON.stringify(data_json[kk]));
                    return;
                }
            }
        }
    }
    static IsSimilar(action_0, action_1) {
        var n = 0;
        var l, l2 = 0;
        for (var k in action_0) {
            if (k == "ActionValue")
                continue;
            for (var kk in action_1) {
                if (k == kk) {
                    if (action_0[k] == action_1[kk])
                        n++;
                }
                l2++;
            }
            l++;
        }
        return n == l && n == l2;
    }
}
exports.default = TestDataManager;
TestDataManager.DataFolder = "./_BotData/";
//# sourceMappingURL=TestDataManager.js.map