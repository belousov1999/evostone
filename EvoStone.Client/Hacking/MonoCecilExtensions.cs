﻿
namespace Hearthrock.Client.Hacking
{
    using System;
    using System.IO;
    using System.Reflection;
    using Hearthrock.Bot.Exceptions;
    using Mono.Cecil;
    using Mono.Cecil.Cil;

    /// <summary>
    /// Extensions for MonoCecil
    /// </summary>
    public static class MonoCecilExtensions
    {
        /// <summary>
        /// Get a MethodDefinition with type name and method name.
        /// </summary>
        /// <param name="assemblyDefinition">The AssemblyDefinition</param>
        /// <param name="typeName">The type name.</param>
        /// <param name="methodName">The method name.</param>
        /// <returns>The MethodDefinition.</returns>
        public static MethodDefinition GetMethod(this AssemblyDefinition assemblyDefinition, string typeName, string methodName)
        {
            // find hook method
            try
            {
                TypeDefinition td = null;
                foreach (TypeDefinition t in assemblyDefinition.MainModule.Types)
                {
                    if (t.Name.ToLower() == typeName.ToLower())
                    {
                        td = t;
                        break;
                    }
                }

                MethodDefinition md = null;
                foreach (MethodDefinition t in td.Methods)
                {
                    if (methodName == "GetUserAgent")
                    {
                        Console.WriteLine("Method: " + t.Name);
                    }
                    if (t.Name == methodName)
                    {
                        md = t;
                        break;
                    }
                }
                if (md == null)
                {
                    throw new PegasusException($"{assemblyDefinition.FullName} {typeName}.{methodName} method not found!");
                }
                return md;
            }
            catch (Exception e)
            {
                throw new PegasusException($"{assemblyDefinition.FullName} {typeName}.{methodName} method not found!", e);
            }
        }

        public static MethodDefinition GetNestedMethod(this AssemblyDefinition assemblyDefinition, string typeName, string nestedType, string methodName, out ModuleDefinition module)
        {
            // find hook method
            try
            {
                TypeDefinition td = null;
                foreach (TypeDefinition t in assemblyDefinition.MainModule.Types)
                {
                    if (t.Name.ToLower() == typeName.ToLower())
                    {
                        td = t;
                        break;
                    }
                }

                foreach (var t in td.NestedTypes)
                {
                    if (t.Name == nestedType)
                    {
                        td = t;
                        break;
                    }
                }
                module = td.Module;

                MethodDefinition md = null;
                foreach (MethodDefinition t in td.Methods)
                {

                    if (t.Name == methodName)
                    {
                        md = t;
                        break;
                    }
                }

                return md;
            }
            catch (Exception e)
            {
                throw new PegasusException($"{assemblyDefinition.FullName} {typeName}.{methodName} method not found!", e);
            }
        }

        private const string PATH = "/RockUnityLOG.txt";

        public static void WriteToFile(string s)
        {
            using (StreamWriter w = File.AppendText(Directory.GetCurrentDirectory() + PATH))
                w.Write(s + "\n");
        }


        public static AssemblyDefinition InjectClearing(this AssemblyDefinition assemblyDefinition, MethodDefinition methodDefinition)
        {
            try
            {
                ILProcessor ilp = methodDefinition.Body.GetILProcessor();
                //  Instruction ins_first = ilp.Body.Instructions[1];


                // Instruction ins = ilp.Create(OpCodes.Ret);
                ilp.Body.Instructions.Clear();
                // ilp.InsertBefore(ins_first, ins);
                return assemblyDefinition;
            }
            catch (Exception e)
            {
                throw new PegasusException("Failed to inject method.", e);
            }
        }


        /// <summary>
        /// Inject a method to an existing method.
        /// </summary>
        /// <param name="assemblyDefinition">The AssemblyDefinition</param>
        /// <param name="newMethodDefinition">The MethodDefinition.</param>
        /// <param name="baseMethodDefinition">The MethodDefinition To Inject</param>
        /// <returns>The updated AssemblyDefinition.</returns>
        public static AssemblyDefinition InjectMethod(this AssemblyDefinition assemblyDefinition, MethodDefinition newMethodDefinition, MethodDefinition baseMethodDefinition)
        {
            try
            {
                ILProcessor ilp = newMethodDefinition.Body.GetILProcessor();
                Instruction ins_first = ilp.Body.Instructions[0];

                Instruction ins = ilp.Create(OpCodes.Call, assemblyDefinition.MainModule.Import(baseMethodDefinition.Resolve()));

                ilp.InsertBefore(ins_first, ins);
                return assemblyDefinition;
            }
            catch (Exception e)
            {
                throw new PegasusException("Failed to inject method.", e);
            }
        }


        public static AssemblyDefinition InjectMethodToEnd(this AssemblyDefinition assemblyDefinition, MethodDefinition newMethodDefinition, MethodDefinition baseMethodDefinition)
        {
            try
            {
                ILProcessor ilp = newMethodDefinition.Body.GetILProcessor();
                Console.WriteLine("Method: " + newMethodDefinition.Name + " Count: " + ilp.Body.Instructions.Count);
                Instruction ins_last = ilp.Body.Instructions[ilp.Body.Instructions.Count - 2];

                Instruction ins = ilp.Create(OpCodes.Call, assemblyDefinition.MainModule.Import(baseMethodDefinition.Resolve()));

                ilp.InsertAfter(ins_last, ins);
                return assemblyDefinition;
            }
            catch (Exception e)
            {
                throw new PegasusException("Failed to inject method.", e);
            }
        }

        /// <summary>
        /// Hijack a method's return value.
        /// </summary>
        /// <param name="assemblyDefinition">The AssemblyDefinition</param>
        /// <param name="methodDefinition">The MethodDefinition to filter the return value.</param>
        /// <param name="baseMethodDefinition">The MethodDefinition To Hijack</param>
        /// <returns>The updated AssemblyDefinition.</returns>
        public static AssemblyDefinition HijackMethod(this AssemblyDefinition assemblyDefinition, MethodDefinition methodDefinition, MethodDefinition baseMethodDefinition)
        {
            try
            {
                ILProcessor ilp = methodDefinition.Body.GetILProcessor();

                // the index of temporary variable
                int index = ilp.Body.Variables.Count;
                ilp.Body.Variables.Add(new VariableDefinition(assemblyDefinition.MainModule.Import(baseMethodDefinition.ReturnType)));

                var instructions = ilp.Body.Instructions.ToArray();

                foreach (Instruction instruction in instructions)
                {
                    // for all RET operation, call the filter function to filter the value.
                    if (instruction.OpCode == OpCodes.Ret)
                    {
                        ilp.InsertBefore(instruction, ilp.Create(OpCodes.Call, assemblyDefinition.MainModule.Import(baseMethodDefinition.Resolve())));
                        ilp.InsertBefore(instruction, ilp.Create(OpCodes.Stloc, index));
                        ilp.InsertBefore(instruction, ilp.Create(OpCodes.Ldloc, index));
                    }
                }

                return assemblyDefinition;
            }
            catch (Exception e)
            {
                throw new PegasusException("Failed to hiject method.", e);
            }
        }

        public static AssemblyDefinition HijackNestedMethod(this AssemblyDefinition assemblyDefinition, ModuleDefinition module, MethodDefinition methodDefinition, MethodDefinition baseMethodDefinition)
        {
            try
            {
                ILProcessor ilp = methodDefinition.Body.GetILProcessor();

                // the index of temporary variable
                int index = ilp.Body.Variables.Count;
                ilp.Body.Variables.Add(new VariableDefinition(module.Import(baseMethodDefinition.ReturnType)));

                var instructions = ilp.Body.Instructions.ToArray();

                foreach (Instruction instruction in instructions)
                {
                    // for all RET operation, call the filter function to filter the value.
                    if (instruction.OpCode == OpCodes.Ret)
                    {
                        ilp.InsertBefore(instruction, ilp.Create(OpCodes.Call, assemblyDefinition.MainModule.Import(baseMethodDefinition.Resolve())));
                        ilp.InsertBefore(instruction, ilp.Create(OpCodes.Stloc, index));
                        ilp.InsertBefore(instruction, ilp.Create(OpCodes.Ldloc, index));
                    }
                }

                return assemblyDefinition;
            }
            catch (Exception e)
            {
                throw new PegasusException("Failed to hiject method.", e);
            }
        }
    }
}
