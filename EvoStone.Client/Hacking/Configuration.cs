﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoStone.Client.Hacking
{
    public enum GameMode
    {
        None = 0,
        NormalPractice = 1,
        ExpertPractice = 2,
        Casual = 3,
        Ranked = 4,
        WildCasual = 5,
        WildRanked = 6
    }

    public struct Configuration
    {
        public int TraceLevel;
        public string TraceEndpoint;
        public string BotEndpoint;
        public GameMode GameMode;
        public int DeckIndex;
        public int OpponentIndex;
    }
}
