﻿
namespace EvoStone.Client
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using EvoStone.Client.Hacking;
    using Hearthrock.Bot.Exceptions;
    using Hearthrock.Client.Hacking;

    public static class Constants
    {
        /// <summary>
        /// Default Endpoint
        /// </summary>
        public const string DefaultEndpoint = "http://127.0.0.1:7625";

        /// <summary>
        /// Default url relative path to send trace.
        /// </summary>
        public const string DefaultTracePath = "/trace";

        /// <summary>
        /// Default url relative path of bot.
        /// </summary>
        public const string DefaultBotPath = "/";
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The RockConfiguration.
        /// </summary>
        private Configuration configuration = new Configuration();

        /// <summary>
        /// The Patcher instance.
        /// </summary>
        private Patcher patcher;

        

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.patcher = new Patcher(Environment.CurrentDirectory);
            this.InitializeConfigurationAsync();
        }

        /// <summary>
        /// The method to response Click event for PatchButton.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private async void PatchButton_Click(object sender, RoutedEventArgs e)
        {
            PatchButton.IsEnabled = false;

            try
            {
                await this.patcher.RecoverHearthstoneAsync();
            }
            catch (PegasusException pegasusException)
            {
                MessageBox.Show(pegasusException.Message);
                PatchButton.IsEnabled = true;
                return;
            }

            await this.patcher.InjectAsync();
            PatchButton.IsEnabled = true;

            if (!await this.patcher.TestRockConfigurationAsync())
            {
                await this.patcher.WriteRockConfigurationAsync(this.configuration);
            }
        }

        /// <summary>
        /// The method to response Click event for RecoverButton.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private async void RecoverButton_Click(object sender, RoutedEventArgs e)
        {
            RecoverButton.IsEnabled = false;

            try
            {
                await this.patcher.RecoverHearthstoneAsync();
            }
            catch (PegasusException pegasusException)
            {
                MessageBox.Show(pegasusException.Message);
                RecoverButton.IsEnabled = true;
                return;
            }

            RecoverButton.IsEnabled = true;
        }

        /// <summary>
        /// The method to response Click event for SaveButton.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveButton.IsEnabled = false;

            switch (TraceComboBox.SelectedIndex)
            {
                default:
                case 0:
                    this.configuration.TraceEndpoint = string.Empty;
                    break;
                case 1:
                    this.configuration.TraceEndpoint = Constants.DefaultEndpoint + Constants.DefaultTracePath;
                    break;
                case 2:
                    this.configuration.TraceEndpoint = TraceTextBox.Text;
                    break;
            }

            switch (this.BotComboBox.SelectedIndex)
            {
                default:
                case 0:
                    this.configuration.BotEndpoint = string.Empty;
                    break;
                case 1:
                    this.configuration.BotEndpoint = Constants.DefaultEndpoint + Constants.DefaultBotPath;
                    break;
                case 2:
                    this.configuration.BotEndpoint = BotTextBox.Text;
                    break;
            }

            if (this.configuration.BotEndpoint != string.Empty && !this.configuration.BotEndpoint.EndsWith("/"))
            {
                MessageBox.Show("Bot endpoints usually end with \"/\".");
            }

            try
            {
                await this.patcher.WriteRockConfigurationAsync(this.configuration);
            }
            catch (PegasusException pegasusException)
            {
                MessageBox.Show(pegasusException.Message);
                SaveButton.IsEnabled = true;
                return;
            }

            await this.InitializeConfigurationAsync(this.patcher.RootPath);

            SaveButton.IsEnabled = true;
        }

        /// <summary>
        /// The method to response SelectionChanged event for TraceComboBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private void TraceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (TraceComboBox.SelectedIndex)
            {
                default:
                case 0:
                    TraceTextBox.Text = "N/A";
                    TraceTextBox.IsEnabled = false;
                    break;
                case 1:
                    TraceTextBox.Text = Constants.DefaultEndpoint + Constants.DefaultTracePath;
                    TraceTextBox.IsEnabled = false;
                    break;
                case 2:
                    TraceTextBox.IsEnabled = true;
                    TraceTextBox.Text = this.configuration.TraceEndpoint;
                    break;
            }
        }
        
        /// <summary>
        /// The method to response SelectionChanged event for BotComboBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private void BotComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (BotComboBox.SelectedIndex)
            {
                default:
                case 0:
                    BotTextBox.Text = "N/A";
                    BotTextBox.IsEnabled = false;
                    break;
                case 1:
                    BotTextBox.Text = Constants.DefaultEndpoint + Constants.DefaultBotPath;
                    BotTextBox.IsEnabled = false;
                    break;
                case 2:
                    BotTextBox.Text = this.configuration.BotEndpoint;
                    BotTextBox.IsEnabled = true;
                    break;
            }
        }

        /// <summary>
        /// The method to response SelectionChanged event for GameModeComboBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private void GameModeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.configuration.GameMode = (GameMode)(GameModeComboBox.SelectedIndex + 1);
        }
        
        /// <summary>
        /// The method to response SelectionChanged event for DeckComboBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private void DeckComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.configuration.DeckIndex = DeckComboBox.SelectedIndex;
        }

        /// <summary>
        /// The method to response SelectionChanged event for OpponentComboBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private void OpponentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.configuration.OpponentIndex = OpponentComboBox.SelectedIndex;
        }

        /// <summary>
        /// The method to response SelectionChanged event for LoggingComboBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private void LoggingComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.configuration.TraceLevel = LoggingComboBox.SelectedIndex;
        }

        /// <summary>
        /// The method to response Click event for PathButton.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        private async void PathButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                var result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
                {
                    try
                    {
                        this.patcher.RootPath = dialog.SelectedPath;
                        await this.InitializeConfigurationAsync(this.patcher.RootPath);
                    }
                    catch (PegasusException pegasusException)
                    {
                        MessageBox.Show(pegasusException.Message);
                        return;
                    }
                }
            }
        }

        private async void Test1_Click(object sender, RoutedEventArgs e)
        {
            var path = "D:\\Moxem\\_BattleNet\\HS\\T1\\Hearthstone";
            try
            {
                this.patcher.RootPath = path;
                await this.InitializeConfigurationAsync(this.patcher.RootPath);
            }
            catch (PegasusException pegasusException)
            {
                MessageBox.Show(pegasusException.Message);
                return;
            }
        }

        /// <summary>
        /// Initialize Configuration and UI.
        /// </summary>
        private async void InitializeConfigurationAsync()
        {
            string path = string.Empty;
            try
            {
                path = await this.patcher.SearchHearthstoneDirectoryAsync();
                this.patcher.RootPath = path;
            }
            catch
            {
            }

            await this.InitializeConfigurationAsync(path);
        }

        /// <summary>
        /// Initialize Configuration and UI.
        /// </summary>
        /// <param name="path">the rootPath</param>
        /// <returns>The task</returns>
        private async Task InitializeConfigurationAsync(string path)
        {
            await Task.Delay(0);

            PathTextBox.Text = path;

            if (string.IsNullOrWhiteSpace(this.configuration.TraceEndpoint))
            {
                TraceComboBox.SelectedIndex = 0;
            }

            try
            {
                var defaultTraceEndpoint = new Uri(Constants.DefaultEndpoint + Constants.DefaultTracePath);
                var traceEndpoint = new Uri(this.configuration.TraceEndpoint);
                if (traceEndpoint.AbsoluteUri == defaultTraceEndpoint.AbsoluteUri)
                {
                    TraceComboBox.SelectedIndex = 1;
                }
                else
                {
                    TraceComboBox.SelectedIndex = 2;
                }
            }
            catch
            {
            }

            if (string.IsNullOrWhiteSpace(this.configuration.BotEndpoint))
            {
                BotComboBox.SelectedIndex = 0;
            }

            try
            {
                var defaultBotEndpoint = new Uri(Constants.DefaultEndpoint + Constants.DefaultBotPath);
                var botEndpoint = new Uri(this.configuration.BotEndpoint);
                if (botEndpoint.AbsoluteUri == defaultBotEndpoint.AbsoluteUri)
                {
                    BotComboBox.SelectedIndex = 1;
                }
                else
                {
                    BotComboBox.SelectedIndex = 2;
                }
            }
            catch
            {
            }

            DeckComboBox.SelectedIndex = 0;
            DeckComboBox.IsEnabled = false;

            OpponentComboBox.SelectedIndex = 0;
            OpponentComboBox.IsEnabled = false;

            if (this.configuration.GameMode == GameMode.None)
            {
                this.configuration.GameMode = GameMode.NormalPractice;
            }

            GameModeComboBox.SelectedIndex = (int)this.configuration.GameMode - 1;

            if (this.configuration.TraceLevel < 0 && this.configuration.TraceLevel > 4)
            {
                this.configuration.TraceLevel = 0;
            }

            LoggingComboBox.SelectedIndex = this.configuration.TraceLevel;
        }
    }
}
