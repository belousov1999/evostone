"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DataManager {
    static AddAction(action) {
        DataManager.NewActions.push(action);
        if (DataManager.IsActionExists(action)) {
            DataManager.UpdateActionValue(action);
            DataManager.WriteDataToFile();
            return;
        }
        //if (!DataManager.Preloaded)
        //    return;
        DataManager.ActionsObjects.push(action);
        //DataManager.WriteDataToFile();
    }
    //Find similar action. If `ActionsObjects` length is zero, return false
    static IsActionExists(action) {
        if (DataManager.ActionsObjects.length == 0)
            return false;
        var key = this.GetActionKey(action);
        return key != "-1";
    }
    static GetActionValue(action) {
        if (DataManager.ActionsObjects.length == 0)
            return -1;
        var key = this.GetActionKey(action);
        if (key == "-1") {
            var sim_action = DataManager.FindMostSimilarAction(action);
            if (sim_action !== undefined) {
                //console.log("Length: " + DataManager.ActionsObjects.length + DataManager.GetActionOption(sim_action) + " " + sim_action["ActionValue"]);
                return sim_action["ActionValue"];
            }
            return -1;
        }
        //console.log("Length: " + DataManager.ActionsObjects.length + DataManager.GetActionOption(DataManager.ActionsObjects[key]) + " " + DataManager.ActionsObjects[key]["ActionValue"]);
        return DataManager.ActionsObjects[key]["ActionValue"];
    }
    static GetActionOption(action) {
        if (action != undefined)
            for (var k in action) {
                if (k.indexOf("Action") == 0) {
                    return k;
                }
            }
        return undefined;
    }
    static UpdateActionValue(action) {
        if (DataManager.ActionsObjects.length == 0)
            return;
        var key = this.GetActionKey(action);
        if (key == "-1")
            return;
        DataManager.ActionsObjects[key]["ActionValue"] = action["ActionValue"];
    }
    static GetActionKey(action) {
        if (DataManager.ActionsObjects.length == 0)
            return "-1";
        for (var k in DataManager.ActionsObjects) {
            var innerAction = DataManager.ActionsObjects[k];
            var counts = 0;
            var keys_count = 0;
            for (var kk in innerAction) {
                if (kk == "ActionValue")
                    continue;
                keys_count++;
                if (innerAction[kk] == action[kk])
                    counts++;
            }
            if (counts == keys_count) // if number of equals == elements count, return true
                return k;
        }
        return "-1";
    }
    static GetKeyValue(action, key) {
        if (action === undefined || action[key] === undefined)
            return -1;
        return action[key];
    }
    static FindMostSimilarAction(action) {
        var n = 0; //similar keys-values
        var similar;
        for (var k in DataManager.ActionsObjects) {
            var sim_n = 0;
            var sim_action = DataManager.ActionsObjects[k];
            for (var kk in action)
                if (DataManager.GetKeyValue(action, kk) == DataManager.GetKeyValue(sim_action, kk)) {
                    sim_n++;
                }
            if (sim_n > n) {
                similar = sim_action;
                n = sim_n;
            }
        }
        return similar;
    }
    static ReadDataFromFiles(files_data) {
        new Promise(resolve => {
            var c = 0;
            for (var i = 0; i < files_data.length; i++) {
                setTimeout(() => {
                    if (c % 100 == 0)
                        console.log(`${c}/${files_data.length}`);
                    if (!DataManager.IsActionExists(files_data[c]))
                        DataManager.ActionsObjects.push(files_data[c]);
                    else
                        DataManager.UpdateActionValue(files_data[c]);
                    c++;
                    if (c >= files_data.length) {
                        DataManager.Preloaded = true;
                        console.log("[Worker]: PRELOADED");
                        resolve();
                    }
                }, i * 30);
            }
        });
        /*
        if (!fs.existsSync(this.DataFolder))
            fs.mkdirSync(this.DataFolder);
        var files = fs.readdirSync(this.DataFolder);
        if (!DataManager.IsWriting && !DataManager.IsReading && files.length > 0) {
            DataManager.IsReading = true;
            var count = files.length;
            for (var n in files) {
                var file_path = this.DataFolder + files[n];
                fs.readFile(file_path, "utf8", (err, data) => {
                    count--;
                    console.log("Readed: " + (files.length - count) + "/" + files.length);
                    if (count <= 0)
                        DataManager.IsReading = false;
                    if (err) {
                        console.error("Read file: " + files[n] + " uncompleted.\n" + err);
                        fs.unlink(file_path, (delete_err) => {
                            if (delete_err)
                                console.log(delete_err);
                        });
                        return;
                    }
                    var data_json = JSON.parse(data);
                    for (var k in data_json) {
                        if (!DataManager.IsActionExists(data_json[k]))
                            DataManager.ActionsObjects.push(data_json[k]);
                        else
                            DataManager.UpdateActionValue(data_json[k]);
                    }
                });
            }
        }
        if (files.length == 0)
            DataManager.IsReading = false;
        DataManager.Preloaded = true;
        */
    }
    static WriteDataToFile() {
        /*
        if (!DataManager.Preloaded) return;
        if (DataManager.NewActions.length > DataManager.ActionsPerSave) {
            if (!DataManager.IsWriting && !DataManager.IsReading) {
                DataManager.IsWriting = true;
                var today = new Date();
                var fileName = "Data_" + today.getFullYear() + '_' + (today.getMonth() + 1) + '_' + today.getDate() + "_" + today.getHours() + "_" + today.getMinutes() + ".json";
                fs.writeFile(this.DataFolder + fileName, JSON.stringify(DataManager.NewActions), 'utf8', (err) => {
                    if (err) {
                        console.log(err);
                    } else
                        DataManager.NewActions = []
                    DataManager.IsWriting = false;
                });
            }
        }
        */
    }
}
exports.default = DataManager;
DataManager.ActionsObjects = [];
DataManager.NewActions = [];
DataManager.ActionsPerSave = 20;
DataManager.IsWriting = false;
DataManager.IsReading = false;
DataManager.DataFolder = "./_BotData/";
DataManager.Preloaded = false;
//# sourceMappingURL=DataManager.js.map