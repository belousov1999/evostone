import NNPlayer from "./NNPlayer";

export default class Action {
    public Turn: number;
    public Self: NNPlayer;
    public Opponent: NNPlayer;
    public OptionName: string;
    public ActionValue: number;

    constructor(_turn: number, _self: Player, _opponent: Player, _optionName: string) {
        this.Turn = _turn;
        this.Self = new NNPlayer(_self, true);
        this.Opponent = new NNPlayer(_opponent, false);
        this.OptionName = _optionName;
        this.ActionValue = 0.2; //Calculated after action apply
    }

    public UpdateActionValue(newValue: number) {
        if (this.ActionValue == newValue)
            return;
        if (newValue < 0)
            newValue = 0;
        else if (newValue > 1.0)
            newValue = 1.0;
        this.ActionValue = newValue;
    }

    public Convert(): object {
        var main = {
            //Turn: this.Turn,
            ["Action_"+this.OptionName]: 1.0,
            ActionValue: this.ActionValue
        };

        //Converted data to arrays
        var self = this.Self.Convert();
        var opponent = this.Opponent.Convert();

        //Add arrays data to `main`
        for (var k in self)
            main[k] = self[k];
        for (var k in opponent)
            main[k] = opponent[k];

        return main;
    }
}
