"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NNPlayer {
    constructor(player, _isSelf) {
        //Data vars
        this.Resources = player.Resources;
        this.PermanentResources = player.PermanentResources;
        this.TemporaryResources = player.TemporaryResources;
        this.PowerAvailable = player.PowerAvailable;
        this.HeroClass = player.Hero.Class;
        this.PowerName = player.Power.Name;
        this.WeaponName = player.Weapon.Name;
        this.Minions = player.Minions;
        this.Cards = player.Cards;
        //Help vars
        this.prefix = _isSelf ? "Self" : "Opponent";
        this.isSelf = _isSelf;
    }
    static ConvertData(data, prefix) {
        return {
            [prefix + data["Name"] + "_Damage"]: data["Damage"],
            [prefix + data["Name"] + "_Health"]: data["Health"],
            [prefix + data["Name"] + "_Taunt"]: data["HasTaunt"] ? 1.0 : 0
        };
    }
    Convert() {
        var main = {
            [this.prefix + "_PowerAvailable"]: this.PowerAvailable ? 1.0 : 0,
            [this.prefix + "_HeroClass_" + this.HeroClass]: 1.0
        };
        if (this.isSelf) {
            main[this.prefix + "_Resources"] = this.Resources;
            main[this.prefix + "_PermanentResources"] = this.PermanentResources;
            main[this.prefix + "_TemporaryResources"] = this.TemporaryResources;
        }
        //Convert arrays data to NN and adds to `main`
        var f = function (d, prefix) {
            for (var k in d) {
                var inner = d[k];
                var inner_data = NNPlayer.ConvertData(inner, prefix);
                for (var kk in inner_data)
                    main[kk] = inner_data[kk];
            }
        };
        //Add arrays data to `main`
        f(this.Minions, this.prefix + "_Minions_");
        if (this.isSelf)
            for (var k in this.Cards) {
                var card = this.Cards[k];
                var full = this.prefix + "_Cards_" + card.Name;
                main[full] = 1.0;
            }
        return main;
    }
}
exports.default = NNPlayer;
//# sourceMappingURL=NNPlayer.js.map