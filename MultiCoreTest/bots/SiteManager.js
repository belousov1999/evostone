"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var io = require('socket.io-client');
class SiteManager {
    constructor() {
        SiteManager.Instance = this;
        this.socket = io.connect('http://192.168.0.100:80', { reconnect: true });
        // Add a connect listener
        this.socket.on('connect', function (socket) {
            console.log('Connected to Site!');
            SiteManager.Instance.connected = true;
        });
        this.socket.on('disconnect', function (socket) {
            console.log('Disconnected from Site!');
            SiteManager.Instance.connected = false;
        });
    }
    SendDataToSite(accs) {
        if (SiteManager.Instance.connected === undefined
            || SiteManager.Instance.connected == false)
            return;
        this.socket.emit('DataUpdate', accs);
    }
}
exports.default = SiteManager;
//# sourceMappingURL=SiteManager.js.map