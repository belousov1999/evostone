import BotData from "./Objects/BotData";
import ReadyPlay from "./Objects/ReadyPlay";
import Account from "./SiteObjects/Account";
import SiteManager from "./SiteManager";
import BotExcelSaver from "./Utils/BotExcelSaver";

export default class BotDataManager {

    public static BotDataArray: BotData[];
    public static BotReadyArray: ReadyPlay[];
    public static AccountDataArray: Account[];

    private static AutoStart = false;
    private static MinStartCount = 3;
    private static HeartbeatMax = 180;
    private static HeartbeatToDelete = 240;
    private static RankMaxRange = 3;

    private static LastUpdateSiteTime = 0;

    public static OnTurnStart(scene: Scene): number[] | number {
        var bot = BotDataManager.CreateOrGet(this.GetBotName(scene));
        this.UpdateAccountInfo(bot);
        if (this.IsEnemyBot(scene) && scene.Turn >= 4) {
            var waiter = BotDataManager.TryGetReadyPlay(this.GetBotName(scene));
            var waiterEnemy = BotDataManager.TryGetReadyPlay(this.GetEnemyName(scene));
            console.log("IsEnemyBot: " + this.GetBotName(scene) + ": true : " + waiter + " : " + waiterEnemy);
            if (!waiterEnemy || !waiter || waiterEnemy.MedalLevel >= waiter.MedalLevel) {
                return -10; //Concide
            }
        } else if (scene.Turn >= 4) {
            var waiter = BotDataManager.TryGetReadyPlay(this.GetBotName(scene));
            if (waiter && waiter.MedalLevel.toLowerCase() != "none") {
                var all_medals_count = BotDataManager.GetMedalWaitersCountInRange(waiter.MedalLevel);
                if (all_medals_count > 1 && scene.Self.GameMode != 1)
                    return -10;
            }
        }
        if (scene.Self.GameMode == 1)
            return bot.OnTurnStart(scene);
        else
            return -100; //Random action
    }

    public static OnOptionApply(scene: Scene): void {
        var bot = BotDataManager.CreateOrGet(this.GetBotName(scene));
        bot.OnOptionApply(scene);
        this.UpdateAccountInfo(bot);
    }

    public static OnJobsDone(scene: Scene): void {
        var bot = BotDataManager.CreateOrGet(this.GetBotName(scene));
        bot.OnJobsDone(scene);
        this.UpdateAccountInfo(bot);
    }

    public static OnGameOver(scene: Scene): void {
        var bot = BotDataManager.CreateOrGet(this.GetBotName(scene));
        bot.OnGameOver(scene.Opponent.Hero.Health <= 0 && scene.Self.Hero.Health > 0);
        this.UpdateAccountInfo(bot);
    }

    public static OnBadOption(scene: Scene): void {
        var bot = BotDataManager.CreateOrGet(this.GetBotName(scene));
        bot.OnBadOption(scene);
    }

    public static OnAccountData(MedalLevel: string, Email: string, Pass: string, SandNumber: string, GoldCount: number, TotalGamesCount: number, TotalLevelsCount: number, BoostersCount: number) {
        try {
            var medalReady = MedalLevel.indexOf("X") > 0 || MedalLevel.indexOf("V") > 0 || MedalLevel.indexOf("I") > 0;
            var status = "Play";
            if (medalReady && ((GoldCount / 100) + BoostersCount) >= 65)
                status = "Selling";
            BotExcelSaver.UpdateData(Email, Pass, SandNumber, "-", MedalLevel, TotalGamesCount, TotalLevelsCount, BoostersCount, GoldCount, status);
        } catch (e) {
            console.log("Cannot save to excel: " + e);
        }
    }

    public static OnPlayButton(NickName: string, MedalLevel: string, Email: string): boolean {
        //console.log("[OnPlayButton]: " + NickName + " " + MedalLevel);


        var bot = BotDataManager.CreateOrGet(NickName);

        var time_seconds = Math.round(new Date().getTime() / 1000);
        var readyPlay = BotDataManager.TryGetReadyPlay(NickName);

        if (readyPlay === undefined) {
            readyPlay = new ReadyPlay(NickName, MedalLevel, Email, time_seconds);
            if (BotDataManager.BotReadyArray === undefined)
                BotDataManager.BotReadyArray = []
            BotDataManager.BotReadyArray[NickName] = readyPlay;
        } else {
            readyPlay.MedalLevel = MedalLevel;
            if (readyPlay.Searching == false) {
                readyPlay.Searching = true;
                readyPlay.StartSearchTime = time_seconds;
            }
        }

        this.UpdateAccountInfo(bot, readyPlay);
        if (BotDataManager.AutoStart) {
            return true;
        }
        var waiters_n = BotDataManager.GetPlayButtonWaitCount(MedalLevel);
        var all_medals_count = BotDataManager.GetMedalWaitersCountInRange(MedalLevel);
        console.log("[OnPlayButton]: " + NickName + " -- Waiters: " + waiters_n + " Rank: " + MedalLevel + " / " + all_medals_count);
        if ((waiters_n >= BotDataManager.MinStartCount
            || (BotDataManager.MinStartCount > all_medals_count && waiters_n >= all_medals_count))
            || BotDataManager.WaitTooLong(NickName)) {
            return true;
        }
        return false;
    }

    private static TryGetReadyPlay(NickName: string): ReadyPlay {
        if (BotDataManager.BotReadyArray !== undefined)
            return BotDataManager.BotReadyArray[NickName];
    }

    private static IsOnline(readyPlay: ReadyPlay): boolean {
        var time_seconds = Math.round(new Date().getTime() / 1000);
        return readyPlay.IsTimeInRange(time_seconds, BotDataManager.HeartbeatMax);
    }

    private static IsOflineToLong(readyPlay: ReadyPlay): boolean {
        var time_seconds = Math.round(new Date().getTime() / 1000);
        return readyPlay.IsTimeInRange(time_seconds, BotDataManager.HeartbeatToDelete);
    }

    private static GetMedalWaitersCountInRange(medalLevel: string): number {
        var medalInt = parseInt(medalLevel);
        var count = 0;
        for (var w in BotDataManager.BotReadyArray) {
            var waiter = BotDataManager.BotReadyArray[w];
            if (this.IsOnline(waiter)) {
                var waiterInt = parseInt(waiter.MedalLevel);
                if (!isNaN(waiterInt) && !isNaN(medalInt) && Math.abs(waiterInt - medalInt) <= BotDataManager.RankMaxRange)
                    count++;
                else if (waiter.MedalLevel.length > 4
                    && medalLevel.indexOf(waiter.MedalLevel.substring(0, waiter.MedalLevel.length - 2)) >= 0)
                    count++;
            }

        }
        return count;
    }

    private static GetPlayButtonWaitCount(medalLevel: string): number {
        var medalInt = parseInt(medalLevel);
        var n = 0;
        for (var w in BotDataManager.BotReadyArray) {
            var waiter = BotDataManager.BotReadyArray[w];
            if (this.IsOnline(waiter) && waiter.Searching == true) {
                var waiterInt = parseInt(waiter.MedalLevel);
                if (!isNaN(waiterInt) && !isNaN(medalInt) && Math.abs(waiterInt - medalInt) <= BotDataManager.RankMaxRange)
                    n++;
                else if (waiter.MedalLevel.length > 4
                    && medalLevel.indexOf(waiter.MedalLevel.substring(0, waiter.MedalLevel.length - 2)) >= 0)
                    n++;
            }
        }
        return n;
    }

    private static WaitTooLong(nickName: string): boolean {
        var time_seconds = Math.round(new Date().getTime() / 1000);
        return BotDataManager.BotReadyArray[nickName].IsWaitingTooLong(time_seconds);
    }

    private static GetBotName(scene: Scene): string {
        return scene.Self.NickName;
    }

    private static GetEnemyName(scene: Scene): string {
        return scene.Opponent.NickName;
    }

    private static IsEnemyBot(scene: Scene): boolean {
        if (!BotDataManager.BotDataArray)
            return false;
        var enemyName = this.GetEnemyName(scene);
        if (!enemyName)
            return false;
        var bot = BotDataManager.TryGetBot(enemyName);
        return bot["Exist"] == true;
    }

    //[TODO]: optimize code section
    private static TryGetBot(Name: string): object {
        if (BotDataManager.BotDataArray === undefined)
            BotDataManager.BotDataArray = [];
        for (var k in BotDataManager.BotDataArray) {
            var bot = BotDataManager.BotDataArray[k];
            if (bot.Name == Name)
                return { Exist: true, BotData: bot };
        }
        return { Exist: false };
    }

    private static TryGetAccount(botData: BotData): object {
        if (BotDataManager.AccountDataArray === undefined)
            BotDataManager.AccountDataArray = [];
        for (var k in BotDataManager.AccountDataArray) {
            var acc = BotDataManager.AccountDataArray[k];
            if (acc.playerName == botData.Name)
                return { Exist: true, Acc: acc };
        }
        return { Exist: false };
    }

    private static CreateOrGet(Name: string): BotData {
        var tryBot = BotDataManager.TryGetBot(Name);
        var bot;
        if (tryBot["Exist"] == false) {
            bot = new BotData(Name)
            BotDataManager.BotDataArray.push(bot);
        } else
            bot = tryBot["BotData"];
        tryBot = null;
        return bot;
    }

    private static CreateOrGetAccount(botData: BotData): Account {
        var tryAcc = BotDataManager.TryGetAccount(botData);
        var acc;
        if (tryAcc["Exist"] == false) {
            acc = new Account(botData.Name, "", "")
            BotDataManager.AccountDataArray.push(acc);
        } else
            acc = tryAcc["Acc"];
        return acc;
    }

    private static UpdateAllAccountsInfo() {
        var toDelete = []
        var time_seconds = Math.round(new Date().getTime() / 1000);
        for (var k in BotDataManager.BotDataArray) {
            var bot = BotDataManager.BotDataArray[k];
            if (!bot)
                continue;
            if (!BotDataManager.BotReadyArray)
                BotDataManager.BotReadyArray = []
            var waiter: ReadyPlay = BotDataManager.BotReadyArray[bot.Name];
            if (waiter) {
                var online = BotDataManager.IsOnline(waiter);
                if (BotDataManager.IsOflineToLong(waiter)) {
                    toDelete.push(k);
                    continue;
                }
                if (online == false)
                    waiter.Searching = false;
                if (waiter.Searching == false)
                    waiter.StartSearchTime = time_seconds;
                var acc = BotDataManager.CreateOrGetAccount(bot);
                acc.online = online;
                acc.readyToSearch = waiter.Searching;
                acc.timeSinceReady = time_seconds - waiter.StartSearchTime;
                acc.accountEmail = waiter.Email;
            }
        }
        /*
        for (var i = 0; i < BotDataManager.BotDataArray.length; i++) {
            var contains = false;
            for (var k in toDelete)
                if (toDelete[k] == BotDataManager.BotDataArray[k]) {
                    contains = true;
                    break;
                }
            if (contains) { BotDataManager.BotDataArray.splice(i, 1); }
        }

        for (var i = 0; i < BotDataManager.BotReadyArray.length; i++) {
            var contains = false;
            for (var k in toDelete)
                if (toDelete[k] == BotDataManager.BotReadyArray[k]) {
                    contains = true;
                    break;
                }
            if (contains) { BotDataManager.BotReadyArray.splice(i, 1); }
        }

        for (var i = 0; i < BotDataManager.AccountDataArray.length; i++) {
            var contains = false;
            for (var k in toDelete)
                if (toDelete[k] == BotDataManager.AccountDataArray[k]) {
                    contains = true;
                    break;
                }
            if (contains) { BotDataManager.AccountDataArray.splice(i, 1); }
        }
        */
    }

    private static UpdateAccountInfo(botData: BotData, readyPlay: ReadyPlay = undefined) {
        var time_seconds = Math.round(new Date().getTime() / 1000);

        var acc = BotDataManager.CreateOrGetAccount(botData);
        acc.matchWin = botData.WinCounts;
        if (SiteManager.Instance === undefined)
            new SiteManager();
        if (readyPlay !== undefined) {
            acc.online = BotDataManager.IsOnline(readyPlay);
            acc.readyToSearch = readyPlay.Searching;
            acc.medalNumber = readyPlay.MedalLevel + "";
            acc.accountEmail = readyPlay.Email;
            acc.timeSinceReady = time_seconds - readyPlay.StartSearchTime;

            readyPlay.LastTime = time_seconds;
        } else {
            acc.readyToSearch = false;
            var waiter = BotDataManager.TryGetReadyPlay(botData.Name);
            if (waiter) {
                waiter.LastTime = time_seconds;
                waiter.Searching = false;
            }
        }

        if (time_seconds - BotDataManager.LastUpdateSiteTime > 5) {
            BotDataManager.UpdateAllAccountsInfo();
            BotDataManager.LastUpdateSiteTime = time_seconds;
            SiteManager.Instance.SendDataToSite(BotDataManager.AccountDataArray);
        }
    }
}
