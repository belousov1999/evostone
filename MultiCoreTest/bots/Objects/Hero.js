class Hero {
    constructor(json) {
        this.RockId = json["RockId"];
        this.Name = json["Name"];
        this.CardId = json["CardId"];
        this.Class = json["Class"];
        this.Damage = json["Damage"];
        this.Health = json["Health"];
        this.CanAttack = json["CanAttack"];
        this.IsExhausted = json["IsExhausted"];
        this.IsQuest = json["IsQuest"];
        this.IsSecret = json["IsSecret"];
    }
    isEqual(hero) {
        return this.RockId == hero.RockId
            && this.Name == hero.Name
            && this.CardId == hero.CardId
            && this.Class == hero.Class
            && this.Damage == hero.Damage
            && this.Health == hero.Health
            && this.CanAttack == hero.CanAttack
            && this.IsExhausted == hero.IsExhausted
            && this.IsQuest == hero.IsQuest
            && this.IsSecret == hero.IsSecret;
    }
}
module.exports = Hero;
//# sourceMappingURL=Hero.js.map