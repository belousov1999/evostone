"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReadyPlay {
    constructor(nickname, medalLevel, email, lastTime) {
        this.NickName = nickname;
        this.MedalLevel = medalLevel;
        this.Email = email;
        this.LastTime = lastTime;
        this.StartSearchTime = lastTime;
        this.Searching = true;
    }
    IsEqual(nickName) {
        return this.NickName == nickName;
    }
    UpdateTime(lastTime) {
        this.LastTime = lastTime;
        if (!this.IsTimeInRange(lastTime, 20))
            this.StartSearchTime = lastTime;
    }
    IsTimeInRange(time, range) {
        return time - this.LastTime <= range;
    }
    IsWaitingTooLong(time) {
        console.log("Waiting too long: " + (time - this.StartSearchTime));
        return time - this.StartSearchTime >= 600;
    }
}
exports.default = ReadyPlay;
//# sourceMappingURL=ReadyPlay.js.map