enum CardType {
    None,
    Spell,
    Enchantment,
    Weapon,
    Minion
}

module.exports = CardType;