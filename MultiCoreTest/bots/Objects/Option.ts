export default class Option {
    public Name: string;
    public Option: number[];
    constructor(name: string, option: number[]) {
        this.Name = name;
        this.Option = option;
    }
}

//module.exports = Option;