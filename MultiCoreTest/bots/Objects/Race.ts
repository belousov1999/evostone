enum Race {
    None = 0,
    Bloodelf,
    Draenei,
    Dwarf,
    Gnome,
    Goblin,
    Human,
    Nightelf,
    Orc,
    Tauren,
    Troll,
    Undead,
    Worgen,
    Goblin2,
    Murloc,
    Demon,
    Scourge,
    Mechanical,
    Elemental,
    Ogre,
    Pet,
    Totem,
    Nerubian,
    Pirate,
    Dragon
}

module.exports = Race;
