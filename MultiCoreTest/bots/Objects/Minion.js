class Minion {
    constructor(json) {
        this.RockId = json["RockId"];
        this.Name = json["Name"];
        this.CardId = json["CardId"];
        this.Damage = json["Damage"];
        this.Health = json["Health"];
        this.BaseHealth = json["BaseHealth"];
        this.Race = json["Race"];
        this.IsFrozen = json["IsFrozen"];
        this.IsExhausted = json["IsExhausted"];
        this.IsAsleep = json["IsAsleep"];
        this.IsStealthed = json["IsStealthed"];
        this.CanAttack = json["CanAttack"];
        this.CanBeAttacked = json["CanBeAttacked"];
        this.HasTaunt = json["HasTaunt"];
        this.HasWindfury = json["HasWindfury"];
        this.HasDivineShield = json["HasDivineShield"];
        this.HasAura = json["HasAura"];
        this.IsEnraged = json["IsEnraged"];
        this.HasTriggerVisual = json["HasTriggerVisual"];
        this.HasInspire = json["HasInspire"];
        this.HasDeathrattle = json["HasDeathrattle"];
        this.HasBattlecry = json["HasBattlecry"];
        this.HasLifesteal = json["HasLifesteal"];
        this.IsPoisonous = json["IsPoisonous"];
    }
    isEqual(minion) {
        return this.RockId == minion.RockId
            && this.Name == minion.Name
            && this.CardId == minion.CardId
            && this.Damage == minion.Damage
            && this.Health == minion.Health
            && this.BaseHealth == minion.BaseHealth
            && this.Race == minion.Race
            && this.IsFrozen == minion.IsFrozen
            && this.IsExhausted == minion.IsExhausted
            && this.IsAsleep == minion.IsAsleep
            && this.IsStealthed == minion.IsStealthed
            && this.CanAttack == minion.CanAttack
            && this.CanBeAttacked == minion.CanBeAttacked
            && this.HasTaunt == minion.HasTaunt
            && this.HasWindfury == minion.HasWindfury
            && this.HasDivineShield == minion.HasDivineShield
            && this.HasAura == minion.HasAura
            && this.IsEnraged == minion.IsEnraged
            && this.HasTriggerVisual == minion.HasTriggerVisual
            && this.HasInspire == minion.HasInspire
            && this.HasDeathrattle == minion.HasDeathrattle
            && this.HasBattlecry == minion.HasBattlecry
            && this.HasLifesteal == minion.HasLifesteal
            && this.IsPoisonous == minion.IsPoisonous;
    }
}
module.exports = Minion;
//# sourceMappingURL=Minion.js.map