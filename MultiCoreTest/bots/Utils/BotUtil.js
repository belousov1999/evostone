"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BotUtil {
    static CreateEmptyAction() {
        return this.create_action([]);
    }
    static CreateRandomAction(scene) {
        return this.create_action(scene['PlayOptions'][Math.floor(Math.random() * scene['PlayOptions'].length)]);
    }
    static create_action(objects) {
        let action = { Version: 1, Objects: objects, Slot: -1 };
        return action;
    }
    static create_concide_action() {
        let action = { Version: 1, Objects: [], Slot: -10 };
        return action;
    }
}
exports.default = BotUtil;
//# sourceMappingURL=BotUtil.js.map