export default class BotUtil {

    public static CreateEmptyAction(): object {
        return this.create_action([]);
    }

    public static CreateRandomAction(scene: object):object {
        return this.create_action(scene['PlayOptions'][Math.floor(Math.random() * scene['PlayOptions'].length)]);
    }

    public static create_action(objects: object): object {
        let action = { Version: 1, Objects: objects, Slot: -1 };
        return action;
    }

    public static create_concide_action(): object {
        let action = { Version: 1, Objects: [], Slot: -10 };
        return action;
    }
}
