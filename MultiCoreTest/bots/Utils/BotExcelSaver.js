"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ExcelJS = require('exceljs');
var colors = [
    '92D050',
    '92D050',
    '00B050',
    '00B050',
    'FFD966',
    '0070C0',
    '0070C0',
    '0070C0',
    '0070C0',
    '0070C0'
];
class BotExcelSaver {
    static GetOrCreateSheet(workbook, fileName, sheetName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (BotExcelSaver.LoadedSheet) {
                Promise.resolve(workbook.getWorksheet(sheetName));
                return;
            }
            try {
                yield workbook.xlsx.readFile(fileName);
                BotExcelSaver.LoadedSheet = true;
            }
            catch (e) { }
            console.log("INIT EXCEL WORKSHEET");
            var worksheet = workbook.getWorksheet(sheetName);
            if (worksheet === undefined) {
                worksheet = workbook.addWorksheet(sheetName, { properties: { tabColor: { argb: 'FFC0000' } } });
                BotExcelSaver.LoadedSheet = true;
            }
            worksheet.columns = [
                { header: 'Login', key: 'login', width: 32, horizontal: "left" },
                { header: 'Pass', key: 'pass', width: 32, horizontal: "left" },
                { header: 'Sand', key: 'sandnumber', width: 10, horizontal: "left" },
                { header: 'Coloda', key: 'coloda', width: 70, horizontal: "left" },
                { header: 'Rank', key: 'rank', width: 17, horizontal: "left" },
                { header: 'Wins', key: 'wins', width: 10, horizontal: "left" },
                { header: 'Levels', key: 'levels', width: 10, horizontal: "left" },
                { header: 'Packs', key: 'packs', width: 10, horizontal: "left" },
                { header: 'Gold', key: 'golds', width: 10, horizontal: "left" },
                { header: 'Status', key: 'status', width: 20, horizontal: "left" },
            ];
            for (var i = 1; i <= 10; i++) {
                var col = worksheet.getColumn(i);
                col.border = {
                    top: { style: 'thin', color: { argb: '000000' } },
                    left: { style: 'thin', color: { argb: '000000' } },
                    bottom: { style: 'thin', color: { argb: '000000' } },
                    right: { style: 'thin', color: { argb: '000000' } },
                };
                worksheet.getColumn(i).fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: colors[i - 1] }
                };
            }
            return Promise.resolve(worksheet);
        });
    }
    static WriteData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!BotExcelSaver.WritingToFile) {
                    BotExcelSaver.HasNewData = false;
                    BotExcelSaver.WritingToFile = true;
                    yield BotExcelSaver.Workbook.xlsx.writeFile(BotExcelSaver.FileName);
                    BotExcelSaver.WritingToFile = false;
                    console.log("Excel updated");
                    if (BotExcelSaver.HasNewData)
                        BotExcelSaver.WriteData();
                }
                else {
                    BotExcelSaver.HasNewData = true;
                }
            }
            catch (e) {
                BotExcelSaver.HasNewData = false;
                BotExcelSaver.WritingToFile = false;
            }
        });
    }
    static UpdateData(login, pass, sandNumber, coloda, rank, wins, levels, packs, gold, status) {
        return __awaiter(this, void 0, void 0, function* () {
            if (BotExcelSaver.Worksheet === undefined)
                BotExcelSaver.Worksheet = yield BotExcelSaver.GetOrCreateSheet(BotExcelSaver.Workbook, BotExcelSaver.FileName, "HearthStone");
            if (BotExcelSaver.Worksheet === undefined)
                return;
            var loginCol = yield BotExcelSaver.Worksheet.getColumn(1);
            var targetRowNumber = -1;
            for (var i = 1; i < 999; i++) {
                var loginField_value = loginCol.values[i];
                if (typeof (loginField_value) === "object")
                    loginField_value = loginField_value.text;
                console.log(i + " - " + JSON.stringify(loginField_value));
                if (loginField_value === undefined || (loginField_value + "").includes(login)) {
                    targetRowNumber = i;
                    break;
                }
            }
            if (targetRowNumber == -1) {
                console.log("Not found row for login: " + login);
                return;
            }
            var targetRow = BotExcelSaver.Worksheet.getRow(targetRowNumber);
            var grayed = true;
            if (status == "Play" || status == "Selling")
                grayed = false;
            BotExcelSaver.SetCellValue(targetRow, 1, login, grayed);
            BotExcelSaver.SetCellValue(targetRow, 2, pass, grayed);
            if (sandNumber.length > 0)
                BotExcelSaver.SetCellValue(targetRow, 3, sandNumber, grayed);
            BotExcelSaver.SetCellValue(targetRow, 4, coloda, grayed);
            BotExcelSaver.SetCellValue(targetRow, 5, rank, grayed);
            BotExcelSaver.SetCellValue(targetRow, 6, wins + "", grayed);
            BotExcelSaver.SetCellValue(targetRow, 7, levels + "", grayed);
            BotExcelSaver.SetCellValue(targetRow, 8, packs + "", grayed);
            BotExcelSaver.SetCellValue(targetRow, 9, gold + "", grayed);
            if (!(targetRow.getCell(10).value + "").includes("/"))
                BotExcelSaver.SetCellValue(targetRow, 10, status, grayed);
            /*
        for (var i = 2; i < 999; i++) {
            var loginField_value = loginCol.values[i];
            if (loginField_value === undefined)
                break;
            var row = BotExcelSaver.Worksheet.getRow(i);
            var statusValue = row.getCell("status").value;
            console.log("Row: " + i + " Status: " + statusValue);
            if (statusValue != "Play" && statusValue != "Selling") {
                row.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    bgColor: { argb: '595959' }
                };
            }
        }
        */
            BotExcelSaver.WriteData();
        });
    }
    static SetCellValue(targetRow, colNumber, value, grayed) {
        var cell = targetRow.getCell(colNumber);
        var color;
        if (grayed)
            color = '595959';
        else
            color = colors[colNumber - 1];
        cell.value = value;
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: color }
        };
    }
}
exports.default = BotExcelSaver;
BotExcelSaver.FileName = "HearthstoneAccounts.xlsx";
BotExcelSaver.LoadedSheet = false;
BotExcelSaver.WritingToFile = false;
BotExcelSaver.HasNewData = false;
BotExcelSaver.Workbook = new ExcelJS.Workbook();
//# sourceMappingURL=BotExcelSaver.js.map