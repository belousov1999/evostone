const ExcelJS = require('exceljs');

var colors = [
    '92D050',
    '92D050',
    '00B050',
    '00B050',
    'FFD966',
    '0070C0',
    '0070C0',
    '0070C0',
    '0070C0',
    '0070C0'
]
export default class BotExcelSaver {
    private static FileName: string = "HearthstoneAccounts.xlsx";
    private static LoadedSheet: boolean = false;
    private static WritingToFile: boolean = false;
    private static HasNewData: boolean = false;
    private static Workbook = new ExcelJS.Workbook();
    private static Worksheet;

    private static async GetOrCreateSheet(workbook: any, fileName: string, sheetName: string): Promise<any> {
        if (BotExcelSaver.LoadedSheet) {
            Promise.resolve(workbook.getWorksheet(sheetName));
            return;
        }

        try {
            await workbook.xlsx.readFile(fileName);
            BotExcelSaver.LoadedSheet = true;
        } catch (e) { }

        console.log("INIT EXCEL WORKSHEET");
        var worksheet = workbook.getWorksheet(sheetName);
        if (worksheet === undefined) {
            worksheet = workbook.addWorksheet(sheetName, { properties: { tabColor: { argb: 'FFC0000' } } });
            BotExcelSaver.LoadedSheet = true;
        }

        worksheet.columns = [
            { header: 'Login', key: 'login', width: 32, horizontal: "left" },
            { header: 'Pass', key: 'pass', width: 32, horizontal: "left" },
            { header: 'Sand', key: 'sandnumber', width: 10, horizontal: "left" },
            { header: 'Coloda', key: 'coloda', width: 70, horizontal: "left" },
            { header: 'Rank', key: 'rank', width: 17, horizontal: "left" },
            { header: 'Wins', key: 'wins', width: 10, horizontal: "left" },
            { header: 'Levels', key: 'levels', width: 10, horizontal: "left" },
            { header: 'Packs', key: 'packs', width: 10, horizontal: "left" },
            { header: 'Gold', key: 'golds', width: 10, horizontal: "left" },
            { header: 'Status', key: 'status', width: 20, horizontal: "left" },
        ];

        for (var i = 1; i <= 10; i++) {
            var col = worksheet.getColumn(i);
            col.border = {
                top: { style: 'thin', color: { argb: '000000' } },
                left: { style: 'thin', color: { argb: '000000' } },
                bottom: { style: 'thin', color: { argb: '000000' } },
                right: { style: 'thin', color: { argb: '000000' } },
            };
            worksheet.getColumn(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: colors[i - 1] }
            }
        }
        return Promise.resolve(worksheet);
    }

    private static async WriteData() {
        try {
            if (!BotExcelSaver.WritingToFile) {
                BotExcelSaver.HasNewData = false;
                BotExcelSaver.WritingToFile = true;
                await BotExcelSaver.Workbook.xlsx.writeFile(BotExcelSaver.FileName);
                BotExcelSaver.WritingToFile = false;
                console.log("Excel updated");
                if (BotExcelSaver.HasNewData)
                    BotExcelSaver.WriteData();
            } else {
                BotExcelSaver.HasNewData = true;
            }
        } catch (e) {
            BotExcelSaver.HasNewData = false;
            BotExcelSaver.WritingToFile = false;
        }
    }

    public static async UpdateData(login: string, pass: string, sandNumber: string, coloda: string, rank: string,
        wins: number, levels: number, packs: number, gold: number, status: string) {

        if (BotExcelSaver.Worksheet === undefined)
            BotExcelSaver.Worksheet = await BotExcelSaver.GetOrCreateSheet(BotExcelSaver.Workbook, BotExcelSaver.FileName, "HearthStone")
        if (BotExcelSaver.Worksheet === undefined)
            return;
        var loginCol = await BotExcelSaver.Worksheet.getColumn(1);

        var targetRowNumber = -1;
        for (var i = 1; i < 999; i++) {
            var loginField_value = loginCol.values[i];

            if (typeof (loginField_value) === "object")
                loginField_value = loginField_value.text;
            console.log(i + " - " + JSON.stringify(loginField_value));
            if (loginField_value === undefined || (loginField_value + "").includes(login)) {
                targetRowNumber = i;
                break;
            }
        }
        if (targetRowNumber == -1) {
            console.log("Not found row for login: " + login);
            return;
        }
        var targetRow = BotExcelSaver.Worksheet.getRow(targetRowNumber);

        var grayed = true;
        if (status == "Play" || status == "Selling")
            grayed = false;

        BotExcelSaver.SetCellValue(targetRow, 1, login, grayed);
        BotExcelSaver.SetCellValue(targetRow, 2, pass, grayed);
        if (sandNumber.length > 0)
            BotExcelSaver.SetCellValue(targetRow, 3, sandNumber, grayed);
        BotExcelSaver.SetCellValue(targetRow, 4, coloda, grayed);
        BotExcelSaver.SetCellValue(targetRow, 5, rank, grayed);
        BotExcelSaver.SetCellValue(targetRow, 6, wins + "", grayed);
        BotExcelSaver.SetCellValue(targetRow, 7, levels + "", grayed);
        BotExcelSaver.SetCellValue(targetRow, 8, packs + "", grayed);
        BotExcelSaver.SetCellValue(targetRow, 9, gold + "", grayed);
        if (!(targetRow.getCell(10).value + "").includes("/"))
            BotExcelSaver.SetCellValue(targetRow, 10, status, grayed);
        /*
    for (var i = 2; i < 999; i++) {
        var loginField_value = loginCol.values[i];
        if (loginField_value === undefined)
            break;
        var row = BotExcelSaver.Worksheet.getRow(i);
        var statusValue = row.getCell("status").value;
        console.log("Row: " + i + " Status: " + statusValue);
        if (statusValue != "Play" && statusValue != "Selling") {
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                bgColor: { argb: '595959' }
            };
        }
    }
    */
        BotExcelSaver.WriteData();
    }

    private static SetCellValue(targetRow: any, colNumber: number, value: string, grayed: boolean) {
        var cell = targetRow.getCell(colNumber);
        var color;
        if (grayed)
            color = '595959';
        else
            color = colors[colNumber - 1];
        cell.value = value;
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: color }
        }
    }
}