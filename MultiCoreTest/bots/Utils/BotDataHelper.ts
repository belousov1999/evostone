class DataHelper {
    private static playerObjects = ["Hero", "Power", "Weapon", "Minions", "Cards", "Choices"];

    private static GetOptionName(scene: Scene, rockID: number) {
        var players = { Self: scene.Self, Opponent: scene.Opponent };
        for (var k in DataHelper.playerObjects) {
            var objType = DataHelper.playerObjects[k];
            for (var playerType in players) {
                var obj = players[playerType][objType]; // players[Self][Hero]
                if (obj === undefined)
                    continue;
                if (objType == "Minions" || objType == "Cards" || objType == "Choices") { //Array of objects
                    for (var objKey in obj) {
                        var subObj = obj[objKey]; // players[Self][Minions][n]
                        if (subObj.RockId == rockID)
                            return playerType + "_" + objType + "_" + subObj.Name; //Self_Minion_Monster
                    }
                } else if (obj.RockId == rockID) //Single object
                    return playerType + "_" + objType + "_" + obj.Name;
            }
        }
        return undefined;
    }

    public static ConvertOptionsToName(scene: Scene, option: number[]): string {
        var t = []
        for (var k in option) {
            var option_rockId = option[k];
            var name = DataHelper.GetOptionName(scene, option_rockId);
            if (name !== undefined)
                t.push(name)
        }
        if (t.length == 0)
            return "JobsDone";
        var s = "";
        for (var k in t) {
            if (t.length - 1 == parseInt(k))
                s += t[k];
            else
                s += t[k] + ",";
        }
        return s;
    }
}

module.exports = DataHelper;