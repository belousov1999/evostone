const _Scene = require("../bots/Objects/Scene");
var Log = require("../bots/Utils/Log");

import BotDataManager from "./BotDataManager";
import BotUtil from "../bots/Utils/BotUtil";

class RockBot  {
    private actionScene: Scene;

    public get_mulligan_action(scene: object): object {
        //console.log("get_mulligan_action");
        return BotUtil.CreateEmptyAction();
    }

    public get_play_action(scene: object): object {
        // Log.Print("get_play_action");
        try {
            var objScene = new _Scene(scene);
            this.actionScene = objScene;
            var action = BotDataManager.OnTurnStart(objScene);
           // console.log("Action: " + action);
            if (typeof (action) == "object")
                return BotUtil.create_action(action);
            else if (action == -10)
                return BotUtil.create_concide_action();
            else if (action == -100)
                return BotUtil.CreateRandomAction(scene);
        } catch (e) { console.log(e) }
        return BotUtil.CreateEmptyAction();
    }

    public on_report(scene: object): void {
        //  Log.Print("on_report");
        var objScene = new _Scene(scene);
        if (objScene.isEqual(this.actionScene)) // IsBadAction?
        {
            BotDataManager.OnBadOption(objScene);
            return;
        }
        BotDataManager.OnOptionApply(objScene);
    }

    public on_game_over(scene: object): void {
        // Log.Print("on_game_over");
        var objScene = new _Scene(scene);
        BotDataManager.OnGameOver(objScene);
    }

    public on_jobs_done(scene: object): void {
        //Log.Print("on_jobs_done");
        var objScene = new _Scene(scene);
        BotDataManager.OnJobsDone(objScene);
    }

    public on_account_data(accountdata: object): void {
        BotDataManager.OnAccountData(accountdata["medalLevel"], accountdata["email"], accountdata["pass"], accountdata["sandNumber"],
            accountdata["GoldCount"], accountdata["TotalGamesCount"], accountdata["TotalLevelsCount"], accountdata["BoostersCount"]);
    }

    public on_ready_play(readyplay: object): object {
        try {
            var canplay = BotDataManager.OnPlayButton(readyplay["nickname"], readyplay["medalLevel"], readyplay["email"]);
            return { ready: canplay };
        } catch (e) {
            console.log("on_ready_play:" + e);
        }
        return { ready: true };
    }
}

module.exports = RockBot;