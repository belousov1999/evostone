"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var BOT = require("./../bots/bot");
const DataManager_1 = require("./../NeuralNetwork/DataManager");
var bot = new BOT();
function secure(data) {
    data.path = (data.path || "").replace(/\\/g, "/").replace(/(?:\/)+/g, "/").replace(/(?:\.)+/g, ".");
    return data;
}
function HandleAPI(req, res) {
    let body = "";
    req.on('data', function (chunk) {
        body += chunk;
    });
    req.on('end', function () {
        try {
            if (!req.url || req.url === "") {
                res.sendStatus(400);
                return;
            }
            //console.log("[Worker]: " + req.url);
            var ret;
            let json = JSON.parse(body);
            if (req.url == "/mulligan") {
                ret = bot.get_mulligan_action(json);
            }
            else if (req.url == "/play") {
                ret = bot.get_play_action(json);
            }
            else if (req.url == "/report") {
                ret = bot.on_report(json);
            }
            else if (req.url == "/gameover") {
                ret = bot.on_game_over(json);
            }
            else if (req.url == "/jobsdone") {
                ret = bot.on_jobs_done(json);
            }
            else if (req.url == "/readyplay") {
                ret = bot.on_ready_play(json);
            }
            else if (req.url == "/accountdata") {
                ret = bot.on_account_data(json);
            }
            res.end(JSON.stringify(ret));
        }
        catch (e) {
            console.log(`api.js error: ` + e);
            res.end("{}");
        }
    });
}
process.on('message', function (msg) {
    try {
        if (msg.type == "readFiles") {
            console.log(`[Worker]: Reading ${msg.data.length} files`);
            var files_data = [];
            var c = 0;
            for (var k in msg.data) {
                var file_path = msg.data[k];
                fs.readFile(file_path, "utf8", (err, data) => {
                    if (err) {
                        console.error("Read file: " + file_path + " uncompleted.\n" + err);
                        fs.unlink(file_path, (delete_err) => {
                            if (delete_err)
                                console.log(delete_err);
                        });
                        return;
                    }
                    var data_json;
                    try {
                        data_json = JSON.parse(data);
                    }
                    catch (e) {
                        console.error("Parse file: " + file_path + " uncompleted.\n" + err);
                        fs.unlink(file_path, (delete_err) => {
                            if (delete_err)
                                console.log(delete_err);
                        });
                        return;
                    }
                    for (var k in data_json)
                        files_data.push(data_json[k]);
                    c++;
                    if (c % 10 == 0)
                        process.send({ type: "FolderMultiReadingProgress", data: { folder: './_BotData/', progress: 10 } });
                    else if (c == msg.data.length)
                        process.send({ type: "FolderMultiReadingProgress", data: { folder: './_BotData/', progress: c % 10, files_data: files_data } });
                });
            }
        }
        else if (msg.type == "init") {
            //process.send({ type: "DoFolderMultiReading", data: { folder: './_BotData/' } });
        }
        else if (msg.type == "dataReadComplete") {
            DataManager_1.default.ReadDataFromFiles(msg.data);
        }
    }
    catch (e) {
        console.log("[Worker]: Error message: " + e);
    }
});
module.exports = () => {
    //scriptCache = new ScriptCache()
    // dataCache = new DataCache()
    return HandleAPI;
};
//# sourceMappingURL=api.js.map