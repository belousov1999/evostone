"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cluster = require("cluster");
const master_1 = require("./master");
if (cluster.isMaster) {
    new master_1.default(cluster);
}
else {
    const Worker = require("./worker");
    new Worker();
}
//# sourceMappingURL=index.js.map