import * as cluster from "cluster";
import Master from "./master"

if (cluster.isMaster) {
	new Master(cluster)
} else {
	const Worker = require("./worker")
	new Worker()
}