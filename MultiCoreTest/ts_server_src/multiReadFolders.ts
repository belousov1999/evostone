
export default class MultiReadFolders {
    //Public
    public chunked;
    public data = [];

    //Private
    private folderPath: string;
    private progress: number;
    private filesCount: number;
    

    constructor(folderPath, workersLength, fs, utils) {
        this.folderPath = folderPath;
        this.progress = 0;
        if (!fs.existsSync(folderPath))
            fs.mkdirSync(folderPath);
        var files = fs.readdirSync(folderPath);
        console.log("Files count: " + files.length);
        this.filesCount = files.length;
        var tempChunk = utils.chunkArray(files, files.length / workersLength);
        for (var k in tempChunk) {
            var chunk = tempChunk[k];
            for (var kk in chunk)
                chunk[kk] = folderPath + "/" + chunk[kk];
        }
        this.chunked = tempChunk;
    }

    public isReading(folderPath): boolean {
        return this.folderPath == folderPath;
    }

    public isCompleteReading(): boolean {
        return this.progress >= this.filesCount;
    }

    public updateProgress(count): void {
        this.progress += count;
        console.log(`Progress: ${this.progress}/${this.filesCount}`);
    }

    public updateData(data: object): void {
        for (var k in data) 
            this.data.push(data[k]);
    }
}