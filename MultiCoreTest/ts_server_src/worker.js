const express = require('express'), bodyParser = require('body-parser'), app = express();
module.exports = class Worker {
    constructor() {
        app.use(bodyParser.urlencoded({ extended: true }));
        var api = require("./api")();
        app.post("/play", api);
        app.post("/readyplay", api);
        app.post("/jobsdone", api);
        app.post("/mulligan", api);
        app.post("/gameover", api);
        app.post("/report", api);
        app.post("/accountdata", api);
        app.listen(7625);
    }
};
//# sourceMappingURL=worker.js.map