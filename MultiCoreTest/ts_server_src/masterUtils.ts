class Utils {
    static chunkArray(myArray, chunk_size) {
        var index = 0;
        var arrayLength = myArray.length;
        var tempArray = [];
        for (index = 0; index < arrayLength; index += chunk_size) {
            tempArray.push(myArray.slice(index, index + chunk_size));
        }
        return tempArray;
    }
    static removeElement(arr, element) {
        var output = [];
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] !== element) {
                output.push(arr[i])
            }
        }
        return output;
    }
}

module.exports = Utils;