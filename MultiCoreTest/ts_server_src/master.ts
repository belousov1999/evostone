import MultiReadFolders from "./multiReadFolders";

export default class Master {
    //Public
    public workers;

    //Private
    private os;
    private fs;
    private utils;
    private reading: MultiReadFolders[];


    constructor(cluster) {
        this.os = require("os");
        this.fs = require("fs");
         
        this.utils = require("./masterUtils");
        this.reading = [];
        this.workers = [];

        var self = this;
       // for (let i = 0; i < this.os.cpus().length; i++)
        for (let i = 0; i < 2; i++)
            this.ForkWorker(cluster)

        //cluster.on('disconnect', function (worker) {
        //    cluster.fork();
        //});

        cluster.on("exit", function (worker, code) {
            console.log(`[master] Worker ${worker.process.pid} exited with code ${code}`)
            //self.utils.removeElement(this.workers, worker);
            //self.ForkWorker()
        });
        setTimeout((w) => {
            for (var k in w)
                w[k].send({ type: "init" });
        }, 100, this.workers);
    }

    private ForkWorker(cluster): void {
        var new_worker_env = {};
        var forkedWorker = cluster.fork(new_worker_env);
        this.workers.push(forkedWorker);
        console.log(`[master] Spawned worker ${forkedWorker.process.pid}`);
        var self = this;
        forkedWorker.on('message', function (msg) {
            try {
                if (msg.type) {
                    self.ProcessWorkerMessage(forkedWorker, msg.type, msg.data);
                }
            } catch (e) {
                console.log("Worker message error: " + e);
            }
        });
    }

    private ProcessWorkerMessage(worker, msgType, data): void {
        switch (msgType) {
            case "DoFolderMultiReading":
                if (!data || !data.folder) {
                    console.log("[Master]: DoFolderMultiReading error");
                    return;
                }
                var folder = data.folder;
                for (var k in this.reading)
                    if (this.reading[k].isReading(folder)) {
                        console.log("[Master]: Already reading");
                        return;
                    }
                try {
                    console.log("[Master]: DoFolderMultiReading from " + folder);
                    var read = new MultiReadFolders(folder, this.workers.length, this.fs, this.utils);
                    this.reading.push(read);
                    for (var i in read.chunked)
                        if (this.workers[i] !== undefined)
                            this.workers[i].send({ type: "readFiles", data: read.chunked[i] });
                } catch (e) {
                    console.log("[Master]: DoFolderMultiReading error: " + e);
                    var obj;
                    for (var k in this.reading) {
                        if (this.reading[k].isReading(folder)) {
                            obj = this.reading[k];
                            break;
                        }
                    }
                    if (obj)
                        this.utils.removeElement(this.reading, obj);
                }
                break;
            case "FolderMultiReadingProgress":
                if (!data || !data.folder || !data.progress) {
                    console.log("[Master]: FolderMultiReadingProgress error");
                    return;
                }
                var read_obj: MultiReadFolders;
                var kk;
                for (var k in this.reading) {
                    if (this.reading[k].isReading(data.folder)) {
                        read_obj = this.reading[k];
                        kk = k;
                        break;
                    }
                }
                if (read_obj) {
                    read_obj.updateProgress(parseInt(data.progress));
                    if (data.files_data) 
                        read_obj.updateData(data.files_data);
                    if (read_obj.isCompleteReading()) {
                        console.log("[Master]: Reading is complete");
                        for (var k in this.workers)
                            this.workers[k].send({ type: "dataReadComplete", data: read_obj.data });
                        this.reading[kk] = undefined;
                        read_obj.data = undefined;
                        read_obj.chunked = undefined;
                    }
                }
                break;
        }
    }
}