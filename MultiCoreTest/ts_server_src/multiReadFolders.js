"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MultiReadFolders {
    constructor(folderPath, workersLength, fs, utils) {
        this.data = [];
        this.folderPath = folderPath;
        this.progress = 0;
        if (!fs.existsSync(folderPath))
            fs.mkdirSync(folderPath);
        var files = fs.readdirSync(folderPath);
        console.log("Files count: " + files.length);
        this.filesCount = files.length;
        var tempChunk = utils.chunkArray(files, files.length / workersLength);
        for (var k in tempChunk) {
            var chunk = tempChunk[k];
            for (var kk in chunk)
                chunk[kk] = folderPath + "/" + chunk[kk];
        }
        this.chunked = tempChunk;
    }
    isReading(folderPath) {
        return this.folderPath == folderPath;
    }
    isCompleteReading() {
        return this.progress >= this.filesCount;
    }
    updateProgress(count) {
        this.progress += count;
        console.log(`Progress: ${this.progress}/${this.filesCount}`);
    }
    updateData(data) {
        for (var k in data)
            this.data.push(data[k]);
    }
}
exports.default = MultiReadFolders;
//# sourceMappingURL=multiReadFolders.js.map