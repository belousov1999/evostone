﻿using InjectTest.Contracts;
using InjectTest.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;

namespace InjectTest.Communication
{
    public class EvoloApiClient
    {
        /// <summary>
        /// ContentType of Json
        /// </summary>
        private const string JsonContentType = "application/json";

        /// <summary>
        /// Initializes a new instance of the <see cref="RockApiClient" /> class.
        /// </summary>
        public EvoloApiClient()
        {
        }

        /// <summary>
        /// Post a object to remote and get the result.
        /// </summary>
        /// <typeparam name="T">Return Type.</typeparam>
        /// <param name="endpoint">The api endpoint.</param>
        /// <param name="obj">The object to be posted.</param>
        /// <returns>The return object.</returns>
        public T Post<T>(string endpoint, EvoloScene obj)
        {
            var ret = this.Post(endpoint, EvoloJsonSerializer.Serialize(obj));
            return EvoloJsonSerializer.Deserialize<T>(ret);
        }

        /// <summary>
        /// Post a object to remote and without getting the result.
        /// </summary>
        /// <param name="endpoint">The api endpoint.</param>
        /// <param name="obj">The object to be posted.</param>
        public void Post(string endpoint, EvoloScene obj) => this.Post(endpoint, EvoloJsonSerializer.Serialize(obj));

        public T PostValue<T>(string endpoint, object obj)
        {
            var ret = this.Post(endpoint, EvoloJsonSerializer.Serialize(obj));
            return EvoloJsonSerializer.Deserialize<T>(ret);
        }

        /// <summary>
        /// Post a json to remote and get the result string.
        /// </summary>
        /// <param name="endpoint">The api endpoint.</param>
        /// <param name="json">The json to be posted.</param>
        /// <returns>The return string.</returns>
        public string Post(string endpoint, string json)
        {
            LogManager.WriteToFile("EndPoint: "+endpoint+" Json: "+json);
            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers[HttpRequestHeader.ContentType] = JsonContentType;
                    return webClient.UploadString(endpoint, json);
                }
            }
            catch (System.Net.Sockets.SocketException) { LogManager.WriteToErrorFile("EvoloApiClient: Server dont answer"); }
            return "";
        }

        /// <summary>
        /// Post a object to remote.
        /// </summary>
        /// <param name="endpoint">The api endpoint.</param>
        /// <param name="obj">The object to be posted.</param>
        public void PostAsync(string endpoint, object obj)
        {
            new Thread(delegate ()
            {
                try
                {
                    this.Post(endpoint, EvoloJsonSerializer.Serialize(obj));
                }
                catch (Exception e)
                {
                    // for any exception
                    Console.WriteLine(e);
                }
            }).Start();
        }
    }
}
