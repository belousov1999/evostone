﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InjectTest.Contracts
{
    [Serializable]
    public struct EvoloScene
    {
        /// <summary>
        /// Gets or sets the friendly player.
        /// </summary>
        public EvoloPlayer Self;

        /// <summary>
        /// Gets or sets the enemy player.
        /// </summary>
        public EvoloPlayer Opponent;

        /// <summary>
        /// Gets or sets the turn number of the game.
        /// </summary>
        public int Turn;

        /// <summary>
        /// Gets or sets the play options.
        /// </summary>
        public List<List<int>> PlayOptions;

        /// <summary>
        /// Gets or sets the action sequence of the game.
        /// The sequence increases during a session, and remain the same when reporting action results.
        /// </summary>
        public int ActionId;

        /// <summary>
        /// Gets or sets the unique client session GUID.
        /// SessionId changes when start a new game or re-start the bot.
        /// </summary>
        public string SessionId;

        public EvoloScene(EvoloPlayer self, EvoloPlayer opponent, int turn, List<List<int>> playOptions, int actionId, string sessionId)
        {
            Self = self;
            Opponent = opponent;
            Turn = turn;
            PlayOptions = playOptions;
            ActionId = actionId;
            SessionId = sessionId;
        }
    }
}
