﻿namespace InjectTest.Contracts
{

    public interface IEvoloObject
    {
        /// <summary>
        /// Gets the Id of the object.
        /// </summary>
        int RockId { get; }

        /// <summary>
        /// Gets the name of the object.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the CardId of the object.
        /// All Card, Hero, Weapon, Minion have an CardId.
        /// Bot authors can use CardId to get some information of a card, hero, weapon or minion.
        /// </summary>
        string CardId { get; }
    }
}
