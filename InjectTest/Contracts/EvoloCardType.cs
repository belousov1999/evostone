﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InjectTest.Contracts
{
    [Serializable]
    public enum EvoloCardType
    {
        /// <summary>
        /// The None.
        /// </summary>
        None,

        /// <summary>
        /// Is spell card.
        /// </summary>
        Spell,

        /// <summary>
        /// Is Enchantment card.
        /// </summary>
        Enchantment,

        /// <summary>
        /// Is weapon card.
        /// </summary>
        Weapon,

        /// <summary>
        /// Is minion card.
        /// </summary>
        Minion
    }
}
