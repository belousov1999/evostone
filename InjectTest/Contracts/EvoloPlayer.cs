﻿using System;
using System.Collections.Generic;

namespace InjectTest.Contracts
{
    [Serializable]
    public struct EvoloPlayer
    {
        public string NickName;

        public int MedalLevel;

        public int GameMode;
        /// <summary>
        /// Gets or sets the available resources of player.
        /// </summary>
        public int Resources;

        /// <summary>
        /// Gets or sets the permanent resources of player.
        /// </summary>
        public int PermanentResources;

        /// <summary>
        /// Gets or sets the temporary resources of player.
        /// </summary>
        public int TemporaryResources;

        /// <summary>
        /// Gets or sets a value indicating whether the power of player is available.
        /// </summary>
        public bool PowerAvailable;

        /// <summary>
        ///  Gets or sets a value indicating whether the hero has weapon.
        /// </summary>
        public bool HasWeapon;

        /// <summary>
        /// Gets or sets the hero of player.
        /// </summary>
        public EvoloHero Hero;

        /// <summary>
        /// Gets or sets the power of player.
        /// </summary>
        public EvoloCard Power;

        /// <summary>
        /// Gets or sets the power of player.
        /// </summary>
        public EvoloCard Weapon;

        /// <summary>
        /// Gets or sets the minions of player.
        /// </summary>
        public List<EvoloMinion> Minions;

        /// <summary>
        /// Gets or sets the cards of player.
        /// </summary>
        public List<EvoloCard> Cards;

        /// <summary>
        /// Gets or sets the (card) choices of player.
        /// </summary>
        public List<EvoloCard> Choices;

        public EvoloPlayer(string nickName, int medalLevel, int gameMode, int resources, int permanentResources, int temporaryResources, bool powerAvailable, bool hasWeapon, EvoloHero hero, EvoloCard power, EvoloCard weapon, List<EvoloMinion> minions, List<EvoloCard> cards, List<EvoloCard> choices)
        {
            NickName = nickName;
            MedalLevel = medalLevel;
            GameMode = gameMode;
            Resources = resources;
            PermanentResources = permanentResources;
            TemporaryResources = temporaryResources;
            PowerAvailable = powerAvailable;
            HasWeapon = hasWeapon;
            Hero = hero;
            Power = power;
            Weapon = weapon;
            Minions = minions;
            Cards = cards;
            Choices = choices;
        }
    }
}
