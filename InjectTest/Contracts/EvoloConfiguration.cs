﻿using System;

namespace InjectTest.Contracts
{
    [Serializable]
    public struct EvoloConfiguration
    {
        /// <summary>
        /// Gets or sets the TraceLevel.
        /// Off = 0, Error = 1, Warning = 2, Info = 3, Verbose = 4.
        /// </summary>
        public int TraceLevel;

        /// <summary>
        /// Gets or sets the endpoint of Hearthrock trace.
        /// Empty of null means use standard output to output trace.
        /// </summary>
        public string TraceEndpoint;

        /// <summary>
        /// Gets or sets the endpoint of Hearthrock bot.
        /// Empty of null means use local bot.
        /// </summary>
        public string BotEndpoint;

        /// <summary>
        /// Gets or sets the preferred game mode.
        /// </summary>
        public EvoloGameMode GameMode;

        /// <summary>
        /// Gets or sets the preferred deck index.
        /// Current selected deck will be used if the deck index is not available.
        /// </summary>
        public int DeckIndex;

        /// <summary>
        /// Gets or sets the preferred opponent index.
        /// Index starts from one, and zero means random.
        /// </summary>
        public int OpponentIndex;
    }
}
