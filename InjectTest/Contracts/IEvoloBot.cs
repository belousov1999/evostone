﻿namespace InjectTest.Contracts
{
    public interface IEvoloBot
    {

        /// <summary>
        /// Generate a mulligan action for current scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns>The cards to be mulligan-ed.</returns>
        EvoloAction GetMulliganAction(EvoloScene scene);

        /// <summary>
        /// Generate a play action for current scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns>The cards to be played.</returns>
        EvoloAction GetPlayAction(EvoloScene scene);

        /// <summary>
        /// Report the result of an action by providing the current scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        void ReportActionResult(EvoloScene scene);

        void SendPostGameOver(EvoloScene scene);

        void SendJobsDone(EvoloScene scene);

        bool SendReadyPlay(string name, string medalLevel);

        void OnAccountData();
    }
}
