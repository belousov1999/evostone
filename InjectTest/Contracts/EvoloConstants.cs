﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InjectTest.Contracts
{
    public static class EvoloConstants
    {
        /// <summary>
        /// Default Endpoint
        /// </summary>
        public const string DefaultEndpoint = "http://127.0.0.1:7625";

        /// <summary>
        /// Default url relative path to send trace.
        /// </summary>
        public const string DefaultTracePath = "/trace";

        /// <summary>
        /// Default url relative path of bot.
        /// </summary>
        public const string DefaultBotPath = "/";

        /// <summary>
        /// Default url relative path of bot play action.
        /// </summary>
        public const string DefaultBotPlayRelativePath = "play";

        /// <summary>
        /// Default url relative path of bot mulligan action.
        /// </summary>
        public const string DefaultBotMulliganRelativePath = "mulligan";

        /// <summary>
        /// Default url relative path of bot report action.
        /// </summary>
        public const string DefaultBotReportRelativePath = "report";

        public const string DefaultBotGameOverRelativePath = "gameover";

        public const string DefaultBotJobsDoneRelativePath = "jobsdone";

        public const string DefaultBotReadyPlayRelativePath = "readyplay";

        public const string DefaultAccountDataRelativePath = "accountdata";
    }
}
