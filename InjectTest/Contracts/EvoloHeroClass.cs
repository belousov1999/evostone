﻿using System;

namespace InjectTest.Contracts
{
    [Serializable]
    public enum EvoloHeroClass
    {
        /// <summary>
        /// The None.
        /// </summary>
        None,

        /// <summary>
        /// The Mage.
        /// </summary>
        Mage,

        /// <summary>
        /// The Hunter.
        /// </summary>
        Hunter,

        /// <summary>
        /// The Warrior.
        /// </summary>
        Warrior,

        /// <summary>
        /// The Shaman.
        /// </summary>
        Shaman,

        /// <summary>
        /// The Druid.
        /// </summary>
        Druid,

        /// <summary>
        /// The Priest.
        /// </summary>
        Priest,

        /// <summary>
        /// The Rogue.
        /// </summary>
        Rogue,

        /// <summary>
        /// The Paladin.
        /// </summary>
        Paladin,

        /// <summary>
        /// The Warlock.
        /// </summary>
        Warlock
    }
}
