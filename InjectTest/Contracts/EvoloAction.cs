﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InjectTest.Contracts
{
    [Serializable]
    public struct EvoloAction
    {
        /// <summary>
        /// Gets or sets the version of the action
        /// </summary>
        public int Version;

        /// <summary>
        /// Gets or sets the sequence of the objects.
        /// </summary>
        public List<int> Objects;

        /// <summary>
        /// Gets or sets the slot, slot is used when to place a minion.
        /// Index starts from zero.
        /// </summary>
        public int Slot;

        /// <summary>
        /// The factory method of EvoloAction
        /// </summary>
        /// <returns>The EvoloAction.</returns>
        public static EvoloAction Create()
        {
            return Create(new List<int>(), -1);
        }

        public static EvoloAction CreateErrorAction()
        {
            return Create(new List<int>(), -404);
        }

        /// <summary>
        /// The factory method of EvoloAction
        /// </summary>
        /// <param name="objects">The EvoloObject IDs of the action.</param>
        /// <returns>The EvoloAction.</returns>
        public static EvoloAction Create(List<int> objects)
        {
            return Create(objects, -1);
        }

        /// <summary>
        /// The factory method of EvoloAction
        /// </summary>
        /// <param name="objects">The EvoloObject IDs of the action.</param>
        /// <param name="slot">The slot when apply the action.</param>
        /// <returns>The EvoloAction.</returns>
        public static EvoloAction Create(List<int> objects, int slot)
        {
            var result = new EvoloAction();
            result.Version = 1;
            result.Objects = objects;
            result.Slot = slot;

            return result;
        }
    }
}
