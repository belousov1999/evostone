﻿using System;

namespace InjectTest.Contracts
{
    /// <summary>
    /// The GameModes supported.
    /// </summary>
    [Serializable]
    public enum EvoloGameMode
    {
        /// <summary>
        /// The None.
        /// </summary>
        None,

        /// <summary>
        /// The Normal Practice.
        /// </summary>
        NormalPractice,

        /// <summary>
        /// The Expert Practice.
        /// </summary>
        ExpertPractice,

        /// <summary>
        /// The Standard Casual.
        /// </summary>
        Casual,

        /// <summary>
        /// The Standard Ranked.
        /// </summary>
        Ranked,

        /// <summary>
        /// The Wild Casual.
        /// </summary>
        WildCasual,

        /// <summary>
        /// The Wild Ranked.
        /// </summary>
        WildRanked
    }
}
