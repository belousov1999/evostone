﻿using InjectTest.Bot;
using InjectTest.Communication;
using InjectTest.Contracts;
using InjectTest.Diagnostics;
using InjectTest.Hooks;
using InjectTest.Pegasus;
using InjectTest.Utils;
using PegasusShared;
using System;
using System.IO;
using UnityEngine;

namespace InjectTest.Engine
{
    public class EvoloEngine
    {
        /// <summary>
        /// The EvoloConfiguration.
        /// </summary>
        public EvoloConfiguration configuration;

        /// <summary>
        /// The IEvoloBot
        /// </summary>
        public IEvoloBot bot;

        /// <summary>
        /// The EvoloEngineTracer
        /// </summary>
        private EvoloTracer tracer;

        /// <summary>
        /// The IEvoloPegasus
        /// </summary>
        private IEvoloPegasus pegasus;

        /// <summary>
        /// Context for action.
        /// </summary>
        private EvoloEngineAction currentAction;

        /// <summary>
        /// The action sequence of the game.
        /// The sequence increases during a session, and remain the same when reporting action results.
        /// </summary>
        private int actionId;

        /// <summary>
        /// The unique client session GUID.
        /// SessionId changes when start a new game or re-start the bot.
        /// </summary>
        private string sessionId;

        private static EvoloEngine _instance;
        /// <summary>
        /// Initializes a new instance of the <see cref="EvoloEngine" /> class.
        /// </summary>
        public EvoloEngine()
        {
            this.Reload();
            _instance = this;
        }

        public static EvoloEngine Get()
        {
            return _instance;
        }

        /// <summary>
        /// Gets current GameMode
        /// </summary>
        public EvoloGameMode GameMode
        {
            get
            {
                return this.configuration.GameMode;
            }
        }

        /// <summary>
        /// Gets a value indicating whether use local as trace output.
        /// </summary>
        public bool UseBuiltinTrace
        {
            get
            {
                return string.IsNullOrEmpty(this.configuration.TraceEndpoint);
            }
        }

        /// <summary>
        /// Gets a value indicating whether use built-in bot.
        /// </summary>
        public bool UseBuiltinBot
        {
            get
            {
                return string.IsNullOrEmpty(this.configuration.BotEndpoint);
            }
        }

        /// <summary>
        /// Reload the configuration of EvoloEngine.
        /// Warning: not thread safe.
        /// </summary>
        public void Reload()
        {
            var configurationString = File.ReadAllText(EvoloEngineConstants.ConfigurationFilePath);
            this.configuration = EvoloJsonSerializer.Deserialize<EvoloConfiguration>(configurationString);
            this.tracer = new EvoloTracer(this.configuration);
            this.bot = new EvoloEngineBot(this.configuration, this.tracer);
            this.pegasus = EvoloPegasusFactory.CreatePegasus(this.tracer);
            this.actionId = 0;
            this.sessionId = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// called every frame
        /// </summary>
        public void Tick()
        {
            this.pegasus.TriggerUserActive();

            //// try
            //// {
            ////     var pegasusState = this.pegasus.GetSceneMode();
            ////     this.tracer.Verbose(pegasusState.ToString());
            ////     EvoloPegasusGameState state = this.pegasus.GetPegasusGameState();
            ////     this.tracer.Verbose(state.ToString());
            //// }
            //// catch
            //// {
            //// }
        }

        /// <summary>
        /// Display trace info to screen.
        /// </summary>
        /// <param name="message">The trace info.</param>
        public void ShowInfo(string message)
        {
            try
            {
                UIStatus.Get().enabled = true;
                UIStatus.Get().AddInfo(message);
            }
            catch (Exception e)
            {
                LogManager.WriteToFile("ShowInfo: Error:" + e);
            }
        }

        private static int BlockingSceneInRow = 0, BlockToQuit = 130;
        private static int GamePlaySceneInRow = 0, GamePlayToQuit = 480;
        /// <summary>
        /// The update method of engine.
        /// </summary>
        /// <returns>Seconds to be delayed before next call.</returns>
        public double Update()
        {
            try
            {
                var pegasusState = this.pegasus.GetPegasusSceneState();
                EvoloGameHooks.EnableLockMousePosition = pegasusState == EvoloPegasusSceneState.GamePlay;

                this.ShowInfo("State: " + pegasusState);
                if (pegasusState != EvoloPegasusSceneState.BlockingScene)
                    BlockingSceneInRow = 0;
                if (pegasusState != EvoloPegasusSceneState.GamePlay)
                    GamePlaySceneInRow = 0;
                switch (pegasusState)
                {
                    case EvoloPegasusSceneState.BlockingScene:
                        BlockingSceneInRow++;
                        if (BlockingSceneInRow >= BlockToQuit)
                            Application.Quit();
                        return 8;
                    case EvoloPegasusSceneState.QuestsDialog:
                        this.pegasus.DoCloseQuestsDialog();
                        return 4;
                    case EvoloPegasusSceneState.RandomPopup:
                        this.pegasus.DoCloseRandomPopup();
                        return 4;
                    case EvoloPegasusSceneState.GeneralDialog:
                        this.pegasus.DoCloseRandomPopup();
                        this.pegasus.DoCloseGeneralDialog();
                        return 4;
                    case EvoloPegasusSceneState.CancelableScene:
                        this.pegasus.NavigateToHubScene();
                        break;
                    case EvoloPegasusSceneState.HubScene:
                        switch (this.GameMode)
                        {
                            case EvoloGameMode.NormalPractice:
                            case EvoloGameMode.ExpertPractice:
                                this.pegasus.NavigateToAdventureScene();
                                break;
                            case EvoloGameMode.Casual:
                            case EvoloGameMode.Ranked:
                            case EvoloGameMode.WildCasual:
                            case EvoloGameMode.WildRanked:
                                this.pegasus.NavigateToTournamentScene();
                                //// Tournament.Get().NotifyOfBoxTransitionStart();
                                break;
                            default:
                                break;
                        }

                        break;
                    case EvoloPegasusSceneState.AdventureScene:
                        switch (this.GameMode)
                        {
                            case EvoloGameMode.NormalPractice:
                                return this.OnEvoloPracticeMode(false);
                            case EvoloGameMode.ExpertPractice:
                                return this.OnEvoloPracticeMode(true);
                            case EvoloGameMode.Casual:
                            case EvoloGameMode.Ranked:
                            case EvoloGameMode.WildCasual:
                            case EvoloGameMode.WildRanked:
                                this.pegasus.NavigateToHubScene();
                                break;
                            default:
                                this.pegasus.NavigateToHubScene();
                                break;
                        }

                        break;
                    case EvoloPegasusSceneState.TournamentScene:
                        switch (this.GameMode)
                        {
                            case EvoloGameMode.NormalPractice:
                            case EvoloGameMode.ExpertPractice:
                                this.pegasus.NavigateToHubScene();
                                break;
                            case EvoloGameMode.Casual:
                                return this.OnEvoloTournamentMode(false, false);
                            case EvoloGameMode.WildCasual:
                                return this.OnEvoloTournamentMode(false, true);
                            case EvoloGameMode.Ranked:
                                return this.OnEvoloTournamentMode(true, false);
                            case EvoloGameMode.WildRanked:
                                return this.OnEvoloTournamentMode(true, true);
                            default:
                                this.pegasus.NavigateToHubScene();
                                break;
                        }

                        break;
                    case EvoloPegasusSceneState.GamePlay:
                        GamePlaySceneInRow++;
                        if (GamePlaySceneInRow >= GamePlayToQuit)
                            Application.Quit();
                        return this.OnEvoloGamePlay();
                    case EvoloPegasusSceneState.TutorialScene:

                        return this.OnTutorial();
                    case EvoloPegasusSceneState.InvalidScene:
                        Application.Quit();
                        break;
                    case EvoloPegasusSceneState.None:
                    default:
                        break;
                }

                return 4;
            }
            catch (Exception e)
            {
                this.tracer.Error(e.ToString());

                return 3;
            }
        }

        /// <summary>
        /// On GamePlay state
        /// </summary>
        /// <returns>Seconds to be delayed before next call.</returns>
        private double OnEvoloGamePlay()
        {
            this.pegasus.DoCloseRandomPopup();
            var pegasusGameState = this.pegasus.GetPegasusGameState();
            switch (pegasusGameState)
            {
                case EvoloPegasusGameState.Blocking:
                    return 4;
                case EvoloPegasusGameState.GameOver:
                    this.ShowInfo("Game Over");
                    // IsGameEnded = true;
                    this.pegasus.DoEndFinishedGame();
                    return this.OnGameOver();
                case EvoloPegasusGameState.WaitForPlay:
                    return this.OnEvoloAction();
                case EvoloPegasusGameState.WaitForMulligan:
                    this.ShowInfo("WaitForMulligan");
                    return this.OnEvoloMulligan();
                case EvoloPegasusGameState.None:
                default:
                    return 2;
            }
        }

        public static int GetGameMode()
        {
            try
            {
                var state = Get().pegasus.GetPegasusSceneState();
                if (state == EvoloPegasusSceneState.TutorialScene)
                    return 0;
                else if (Get().GameMode == EvoloGameMode.NormalPractice)
                    return 1;
                else if (Get().GameMode == EvoloGameMode.Ranked)
                    return 2;
            }
            catch (Exception) { }
            return -1;
        }


        //private static bool IsGameEnded = true;


        private double OnTutorial()
        {
            var coin = EvoloGUI.FindObjectOfType<HeroCoin>();
            var name = GameState.Get()?.GetFriendlySidePlayer()?.GetName();
            if (coin != null || (NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>().CampaignProgress
                == TutorialProgress.NOTHING_COMPLETE && (name == null || name.Length == 0)))
            {
                Options.Get().SetEnum(Option.LOCAL_TUTORIAL_PROGRESS, TutorialProgress.ILLIDAN_COMPLETE);
                NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
                netObject.CampaignProgress = TutorialProgress.ILLIDAN_COMPLETE;
                NetCache.Get().NetCacheChanged<NetCache.NetCacheProfileProgress>();
                InjectMain.IsBotEnabled = true;
                Application.Quit();
                return 20;
            }
            return 4;
        }

        private double OnGameOver()
        {
            var scene = this.pegasus.SnapshotScene(this.sessionId, this.actionId);
            this.bot.SendPostGameOver(scene);

            for (var i = 0; i < 30; i++)
                for (var j = 0;  j < 30; j++)
                    Hearthstone.Progression.RewardTrackManager.Get().ClaimRewardTrackReward(i, j, false);

            if (EvoloGUI.IsMedalReceived)
                DeviceDataSaver.Get().CompleteCurrentDevice();
            else
                DeviceDataSaver.ClearConfig();
            EvoloGameHooks.EnableLockMousePosition = false;
            return 6;
        }
        /// <summary>
        /// On WaitForAction state
        /// </summary>
        /// <returns>Seconds to be delayed before next call.</returns>
        private double OnEvoloAction()
        {
            if (this.currentAction?.IsValid() == false)
            {
                this.currentAction = null;
            }

            if (this.currentAction?.IsDone() == true)
            {
                var scene = this.pegasus.SnapshotScene(this.sessionId, this.actionId);
                this.bot.ReportActionResult(scene);
            }
            if (this.currentAction == null || this.currentAction.IsDone())
            {
                this.actionId += 1;
                var scene = this.pegasus.SnapshotScene(this.sessionId, this.actionId);
                var playAction = this.bot.GetPlayAction(scene);
                try
                {
                    if (playAction.Objects != null && playAction.Objects.Count != 0)
                    {
                        var EvoloActionContext = new EvoloEngineAction(this.pegasus, playAction.Objects, playAction.Slot);
                        if (EvoloActionContext.IsValid())
                        {
                            this.currentAction = EvoloActionContext;
                            this.ShowInfo("EvoloEngine: " + this.currentAction.Interpretation);
                        }
                        else
                        {
                            this.ShowInfo("Invalid EvoloAction");
                            LogManager.WriteToFile("Invalid EvoloAction");
                            this.currentAction = null;
                            return 4;
                        }
                    }
                    else if (playAction.Slot >= -1)
                    {
                        this.ShowInfo("Job's Done");
                        this.bot.SendJobsDone(scene);
                        this.pegasus.DoEndTurn();
                        this.currentAction = null;
                        return 2;
                    }
                    else if (playAction.Slot == -10)
                    {
                        this.ShowInfo("Concede");
                        this.currentAction = null;
                        if (GameState.Get() != null)
                            GameState.Get().Concede();
                        return 2;
                    }
                }
                catch (Exception e) { LogManager.WriteToErrorFile("EvoloEngine.OnEvoloAction" + e); }
            }

            if (this.currentAction != null && this.currentAction.IsValid())
            {
                this.currentAction.Apply();
            }
            return 3;
        }

        /// <summary>
        /// On WaitForMulligan state
        /// </summary>
        /// <returns>Seconds to be delayed before next call.</returns>
        private double OnEvoloMulligan()
        {
            if (this.currentAction == null)
            {
                this.ShowInfo("Mulligan");
                this.actionId += 1;
                // Force update sessionId when starting a new game.
                this.sessionId = Guid.NewGuid().ToString();
                var scene = this.pegasus.SnapshotScene(this.sessionId, this.actionId);
                var mulliganedAction = this.bot.GetMulliganAction(scene);

                this.currentAction = new EvoloEngineAction(this.pegasus, mulliganedAction.Objects, mulliganedAction.Slot);
            }

            if (this.currentAction.IsDone() || this.currentAction.IsEmptyAction())
            {
                this.ShowInfo("Mulligan is DONE");
                try
                {
                    MulliganManager.Get().AutomaticContinueMulligan();
                    var scene = this.pegasus.SnapshotScene(this.sessionId, this.actionId);
                    this.bot.ReportActionResult(scene);
                }
                catch (Exception e)
                {
                    LogManager.WriteToFile("Mulligan is Error: " + e);
                }
                return 4;
            }

            this.currentAction.ApplyAll();
            return 4;
        }

        private bool OnGameStartButton()
        {
            try
            {
                var medal = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(FormatType.FT_CLASSIC).GetMedalText();
                if (!int.TryParse(medal, out _))
                    medal = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(FormatType.FT_CLASSIC).GetRankName();

                // LogManager.WriteToFile("Medal: " +  + " / " + RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(false).GetMedalText() + " / " + RankMgr.Get().GetLocalPlayerLeagueConfig(false).LeagueLevel);
                if (EvoloEngineBot.BotNickname == "")
                    return true;
                return this.bot.SendReadyPlay(EvoloEngineBot.BotNickname, medal);
            }
            catch (Exception e) { LogManager.WriteToErrorFile("EvoloEngine.OnGameStartButton" + e); }
            return false;
        }

        /// <summary>
        /// On Tournament state
        /// </summary>
        /// <param name="ranked">if play in rank mode.</param>
        /// <param name="wild">if play in wild format.</param>
        /// <returns>Seconds to be delayed before next call.</returns>
        private double OnEvoloTournamentMode(bool ranked, bool wild)
        {
            EvoloPegasusSubsceneState subscene = this.pegasus.GetPegasusSubsceneState(EvoloPegasusSceneState.TournamentScene);

            switch (subscene)
            {
                case EvoloPegasusSubsceneState.Ready:
                    this.pegasus.ConfigTournamentMode(ranked, wild);
                    if (this.OnGameStartButton())
                    {
                        this.pegasus.PlayTournamentGame();
                    }
                    return 2;
                default:
                case EvoloPegasusSubsceneState.WaitForChooseMode:
                case EvoloPegasusSubsceneState.WaitForChooseDeck:
                case EvoloPegasusSubsceneState.WaitForChooseOpponent:
                case EvoloPegasusSubsceneState.None:
                    return 2;
            }
        }

        /// <summary>
        /// On Practice state
        /// </summary>
        /// <param name="expert">if play in expert mode.</param>
        /// <returns>Seconds to be delayed before next call.</returns>
        private double OnEvoloPracticeMode(bool expert)
        {
            EvoloPegasusSubsceneState subscene = this.pegasus.GetPegasusSubsceneState(EvoloPegasusSceneState.AdventureScene);
            LogManager.WriteToErrorFile("AdventureSubScene State: " + subscene);
            switch (subscene)
            {
                case EvoloPegasusSubsceneState.WaitForChooseMode:
                    this.pegasus.ConfigPracticeMode(expert);
                    return 1;

                case EvoloPegasusSubsceneState.WaitForChooseOpponent:
                    this.pegasus.ConfigPracticeOpponent(this.configuration.OpponentIndex);
                    return 1;
                case EvoloPegasusSubsceneState.WaitForChooseDeck:
                    if (this.pegasus.ConfigDeck())
                    {
                        var button = Hearthstone.UI.Clickable.FindObjectOfType<PlayButton>();
                        button.TriggerRelease();
                    }
                    return 1;
                case EvoloPegasusSubsceneState.Ready:
                    this.pegasus.PlayPracticeGame();
                    return 1;
                default:
                case EvoloPegasusSubsceneState.None:
                    return 1;
            }
        }
    }
}
