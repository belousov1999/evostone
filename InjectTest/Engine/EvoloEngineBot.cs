﻿using InjectTest.Communication;
using InjectTest.Contracts;
using InjectTest.Diagnostics;
using InjectTest.Pegasus.Request;
using InjectTest.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using static NetCache;

namespace InjectTest.Engine
{
    public class EvoloEngineBot : IEvoloBot
    {
        /// <summary>
        /// The EvoloConfiguration.
        /// </summary>
        private EvoloConfiguration configuration;

        /// <summary>
        /// The EvoloTracer.
        /// </summary>
        private EvoloTracer tracer;

        public static string BotNickname { get; set; } = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="EvoloEngineBot" /> class.
        /// </summary>
        /// <param name="configuration">The EvoloConfiguration.</param>
        /// <param name="tracer">The EvoloTracer.</param>
        public EvoloEngineBot(EvoloConfiguration configuration, EvoloTracer tracer)
        {
            this.configuration = configuration;
            this.tracer = tracer;
        }

        /// <summary>
        /// Generate a mulligan action for current scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns>The cards to be mulligan-ed.</returns>
        public EvoloAction GetMulliganAction(EvoloScene scene)
        {
            this.tracer.Verbose(EvoloJsonSerializer.Serialize(scene));

            try
            {
                var apiClient = new EvoloApiClient();
                var mulligan = apiClient.Post<EvoloAction>($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultBotMulliganRelativePath}", scene);
                this.tracer.Verbose(EvoloJsonSerializer.Serialize(mulligan));
                return mulligan;
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"GetMulliganAction: Unexpected Exception from Bot: {e}");
                return EvoloAction.Create();
            }
        }

        /// <summary>
        /// Generate a play action for current scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns>The cards to be played.</returns>
        public EvoloAction GetPlayAction(EvoloScene scene)
        {
            this.tracer.Verbose(EvoloJsonSerializer.Serialize(scene));
            try
            {
                if (string.IsNullOrEmpty(this.configuration.BotEndpoint))
                    return EvoloAction.CreateErrorAction();
                else
                {
                    var apiClient = new EvoloApiClient();
                    var action = apiClient.Post<EvoloAction>($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultBotPlayRelativePath}", scene);
                    // this.tracer.Verbose(EvoloJsonSerializer.Serialize(action));
                    return action;
                }
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"GetPlayAction: Unexpected Exception from Bot: {e}");
                return EvoloAction.CreateErrorAction();
            }
        }


        public void SendPostGameOver(EvoloScene scene)
        {
            this.tracer.Verbose(EvoloJsonSerializer.Serialize(scene));
            try
            {
                OnAccountData();
                var apiClient = new EvoloApiClient();
                apiClient.Post($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultBotGameOverRelativePath}", scene);
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"SendPostGameOver: Unexpected Exception from Bot: {e}");
            }
        }

        public void SendJobsDone(EvoloScene scene)
        {
            //this.tracer.Verbose(EvoloJsonSerializer.Serialize(scene));
            try
            {
                OnAccountData();
                var apiClient = new EvoloApiClient();
                apiClient.Post($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultBotJobsDoneRelativePath}", scene);
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"SendJobsDone: Unexpected Exception from Bot: {e}");
            }
        }

        /// <summary>
        /// Report the result of an action by providing the current scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        public void ReportActionResult(EvoloScene scene)
        {
            this.tracer.Verbose(EvoloJsonSerializer.Serialize(scene));

            try
            {
                if (string.IsNullOrEmpty(this.configuration.BotEndpoint))
                    return;
                else
                {
                    var apiClient = new EvoloApiClient();
                    apiClient.Post($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultBotReportRelativePath}", scene);
                    return;
                }
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"ReportActionResult: Unexpected Exception from Bot: {e}");
                return;
            }
        }



        public bool SendReadyPlay(string name, string medalLevel)
        {
            try
            {
                OnAccountData();
                string[] commandLineArgs = Environment.GetCommandLineArgs();
                string email = "";
                for (var i = 0; i < commandLineArgs.Length; i++)
                {
                    var arg = commandLineArgs[i];
                    if (arg.Contains("-email") && i + 1 < commandLineArgs.Length)
                        email = commandLineArgs[i + 1];
                }

                var json = EvoloJsonSerializer.Serialize(new ReadyPlay(name, medalLevel, email));
                var apiClient = new EvoloApiClient();
                var ret = apiClient.Post($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultBotReadyPlayRelativePath}", json);
                return ret.Contains("true");
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"SendReadyPlay: Unexpected Exception from Bot: {e}");
                return false;
            }
        }

        public void OnAccountData()
        {
            try
            {
                var medal = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(PegasusShared.FormatType.FT_CLASSIC).GetMedalText();
                if (!int.TryParse(medal, out _))
                    medal = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(PegasusShared.FormatType.FT_CLASSIC).GetRankName();

                int gold = 0, games = 0, sumLevels = -1, packs = 0;
                var netCache = NetCache.Get();

                netCache.RefreshNetObject<NetCacheGamesPlayed>();
                netCache.RefreshNetObject<NetCacheHeroLevels>();
                netCache.RefreshNetObject<NetCacheBoosters>();
                //Gold
                gold = (int)netCache.GetGoldBalance();
                //Games
                var gamesData = netCache.GetNetObject<NetCacheGamesPlayed>();
                if (gamesData != null)
                    games = gamesData.GamesWon;
                //Levels
                var levelsData = netCache.GetNetObject<NetCacheHeroLevels>();
                if (levelsData != null)
                    foreach (var heroLevel in levelsData.Levels)
                        sumLevels += heroLevel.CurrentLevel.Level;
                //Packs
                var boostersData = netCache.GetNetObject<NetCacheBoosters>();
                if (boostersData != null)
                    packs = boostersData.GetTotalNumBoosters();
                string[] commandLineArgs = Environment.GetCommandLineArgs();
                string email = "", pass = "", sandNumber = "";
                for (var i = 0; i < commandLineArgs.Length; i++)
                {
                    var arg = commandLineArgs[i];
                    if (arg.Contains("-email") && i + 1 < commandLineArgs.Length)
                        email = commandLineArgs[i + 1];
                    if (arg.Contains("-pass") && i + 1 < commandLineArgs.Length)
                        pass = commandLineArgs[i + 1];
                    if (arg.Contains("-sand") && i + 1 < commandLineArgs.Length)
                        sandNumber = commandLineArgs[i + 1];
                }
                pass = pass.Replace("-token", "");
                var json = EvoloJsonSerializer.Serialize(new AccountData(medal, email, pass, sandNumber, gold, games, sumLevels, packs));
                var apiClient = new EvoloApiClient();
                var ret = apiClient.Post($"{this.configuration.BotEndpoint}{EvoloConstants.DefaultAccountDataRelativePath}", json);
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile($"OnAccountData: Unexpected Exception from Bot: {e}");
            }
        }
    }
}
