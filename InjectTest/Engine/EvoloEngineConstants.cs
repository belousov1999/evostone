﻿namespace InjectTest.Engine
{
    public static class EvoloEngineConstants
    {
        /// <summary>
        /// Path of configuration file.
        /// </summary>
        public const string ConfigurationFilePath = @"Hearthstone_Data\Managed\hearthrock.json";
    }
}
