﻿using InjectTest.Hooks;
using InjectTest.Utils;
using System;
using UnityEngine;

public class InjectMain
{
    public static bool IsBotEnabled { get; set; } = false;
    public static bool IsDevBot { get; } = true;
    public static string LastLaunchToken { get; set; } = "";

    private static bool injected = false;
    public static void TestInject()
    {
        LogManager.WriteToFile("TestInject start");
        if (injected) return;
        LogManager.WriteToFile("Hooked");
        GameObject sceneObject = SceneMgr.Get().SceneObject;
        sceneObject.AddComponent<EvoloGUI>();
        injected = true;
    }
}

