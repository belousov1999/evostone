﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace InjectTest.Utils
{
    public class EvoloConfigFile
    {
        //Public
        [SerializeField]
        public TAG_CLASS heroClass
        {
            get => heroClass1; set
            {
                heroClass1 = value;
                SaveConfig();
            }
        }

        //Public Static
        public static EvoloConfigFile Instance { get; private set; }

        //Private
        private const string CONFIG_PATH = "config.json";
        private TAG_CLASS heroClass1;

        private EvoloConfigFile()
        {
            heroClass = TAG_CLASS.MAGE;
        }

        public static void SaveConfig()
        {
            if (Instance == null) return;
            var path = Directory.GetCurrentDirectory() + @"\" + CONFIG_PATH;
            File.WriteAllText(path, JsonConvert.SerializeObject(Instance));
        }

        public static void LoadConfig()
        {
            var path = Directory.GetCurrentDirectory() + @"\" + CONFIG_PATH;
            if (File.Exists(path))
            {
                Instance = JsonConvert.DeserializeObject<EvoloConfigFile>(File.ReadAllText(path));
                if (Instance == null)
                    Instance = new EvoloConfigFile();
            }
            else
                Instance = new EvoloConfigFile();
        }
    }
}
