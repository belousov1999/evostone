﻿using System;
using System.IO;

namespace InjectTest.Utils
{
    public static class LogManager
    {
        public static string CurrentDir { get; private set; } = "";
        public const string LogFile = "RockUnityLOG.txt";
        public const string SaveInfoFile = "SaveInfo.txt";
        public const string ErrorFile = "Error.txt";
        public const string ReadFile = "Read.txt";
        public static bool Logging = true;

        //Private Static
        private static bool Inited = false;

        static LogManager()
        {
            if (Inited) return;
            try
            {
                if (File.Exists(CurrentDir + LogFile))
                    File.Delete(CurrentDir + LogFile);
                if (File.Exists(CurrentDir + ErrorFile))
                    File.Delete(CurrentDir + ErrorFile);
                if (File.Exists(CurrentDir + SaveInfoFile))
                    File.Delete(CurrentDir + SaveInfoFile);
            }
            catch (Exception e)
            {
                WriteToErrorFile(e.ToString());
            }
            Inited = true;
        }

        public static void WriteToFile(string s)
        {
            if (!Logging) return;
            try
            {
                if (CurrentDir == "")
                    CurrentDir = Directory.GetCurrentDirectory() + @"\";
                using (StreamWriter w = File.AppendText(CurrentDir + LogFile))
                    w.Write(s + "\n");
            }
            catch (Exception e)
            {
            }
        }

        public static void WriteToErrorFile(string s)
        {
            if (!Logging) return;
            try
            {
                if (CurrentDir == "")
                    CurrentDir = Directory.GetCurrentDirectory() + @"\";
                using (StreamWriter w = File.AppendText(CurrentDir + ErrorFile))
                    w.Write(s + "\n");
            }
            catch (Exception e)
            {
            }
        }

        public static void WriteSaveInfo(string s)
        {
            try
            {
                if (CurrentDir == "")
                    CurrentDir = Directory.GetCurrentDirectory() + @"\";
                using (StreamWriter w = File.AppendText(CurrentDir + SaveInfoFile))
                    w.Write(s + "\n");
            }
            catch (Exception e)
            {
            }
        }

        public static string ReadFromFile(string filename)
        {
            try
            {
                if (CurrentDir == "")
                    CurrentDir = Directory.GetCurrentDirectory() + @"\";
                return File.ReadAllText(CurrentDir + filename);
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}
