﻿using Hearthstone.Core;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InjectTest.Utils
{
    public class EvoloNetCache : NetCache
    {
        private class NetCacheBatchRequest
        {
            public Map<Type, Request> m_requests = new Map<Type, Request>();

            public NetCacheCallback m_callback;

            public ErrorCallback m_errorCallback;

            public bool m_canTimeout = true;

            public DateTime m_timeAdded = DateTime.Now;

            public RequestFunc m_requestFunc;

            public string m_requestStackTrace;

            public NetCacheBatchRequest(NetCacheCallback reply, ErrorCallback errorCallback, RequestFunc requestFunc)
            {
                m_callback = reply;
                m_errorCallback = errorCallback;
                m_requestFunc = requestFunc;
                m_requestStackTrace = Environment.StackTrace;
            }

            public void AddRequests(List<Request> requests)
            {
                foreach (Request request in requests)
                {
                    AddRequest(request);
                }
            }

            public void AddRequest(Request r)
            {
                if (!m_requests.ContainsKey(r.m_type))
                {
                    m_requests.Add(r.m_type, r);
                }
            }
        }

        public NetCacheCallback Callback_c;
        public ErrorCallback ErrorCallbacl_c;
        private static readonly List<Type> m_ServerInitiatedAccountInfoTypes = new List<Type>
    {
        typeof(NetCacheCollection),
        typeof(NetCacheClientOptions),
        typeof(NetCacheArcaneDustBalance),
        typeof(NetCacheGoldBalance),
        typeof(NetCacheProfileNotices),
        typeof(NetCacheBoosters),
        typeof(NetCacheDecks)
    };

        private Map<Type, object> m_netCache = new Map<Type, object>();
        private List<Type> m_inTransitRequests = new List<Type>();
        private static readonly Map<Type, int> m_genericRequestTypeMap = new Map<Type, int>
    {
        {
            typeof(ClientStaticAssetsResponse),
            340
        }
    };
        private static readonly Map<Type, GetAccountInfo.Request> m_getAccountInfoTypeMap = new Map<Type, GetAccountInfo.Request>
    {
        {
            typeof(NetCacheDecks),
            GetAccountInfo.Request.DECK_LIST
        },
        {
            typeof(NetCacheMedalInfo),
            GetAccountInfo.Request.MEDAL_INFO
        },
        {
            typeof(NetCacheCardBacks),
            GetAccountInfo.Request.CARD_BACKS
        },
        {
            typeof(NetCachePlayerRecords),
            GetAccountInfo.Request.PLAYER_RECORD
        },
        {
            typeof(NetCacheGamesPlayed),
            GetAccountInfo.Request.GAMES_PLAYED
        },
        {
            typeof(NetCacheProfileProgress),
            GetAccountInfo.Request.CAMPAIGN_INFO
        },
        {
            typeof(NetCacheCardValues),
            GetAccountInfo.Request.CARD_VALUES
        },
        {
            typeof(NetCacheFeatures),
            GetAccountInfo.Request.FEATURES
        },
        {
            typeof(NetCacheRewardProgress),
            GetAccountInfo.Request.REWARD_PROGRESS
        },
        {
            typeof(NetCacheHeroLevels),
            GetAccountInfo.Request.HERO_XP
        },
        {
            typeof(NetCacheFavoriteHeroes),
            GetAccountInfo.Request.FAVORITE_HEROES
        },
        {
            typeof(NetCacheAccountLicenses),
            GetAccountInfo.Request.ACCOUNT_LICENSES
        }
    };

        private List<NetCacheBatchRequest> m_cacheRequests = new List<NetCacheBatchRequest>();

        public void RegisterScreenEndOfGame(NetCacheCallback callback, ErrorCallback errorCallback, PegasusShared.GameType gameType)
        {
            GameMgr service;
            if (HearthstoneServices.TryGet(out service) && service.IsSpectator())
            {
                Processor.ScheduleCallback(0f, false, delegate
                {
                    callback();
                });
                return;
            }
            NetCacheBatchRequest netCacheBatchRequest = new NetCacheBatchRequest(callback, errorCallback, RegisterScreenEndOfGame);
            netCacheBatchRequest.AddRequests(new List<Request>
        {
            new Request(typeof(NetCacheProfileProgress), true),
            new Request(typeof(NetCacheRewardProgress)),
            new Request(typeof(NetCacheMedalInfo), true),
            new Request(typeof(NetCacheGamesPlayed), true),
            new Request(typeof(NetCachePlayerRecords), true),
            new Request(typeof(NetCacheHeroLevels), true)
        });
            NetCacheMakeBatchRequest(netCacheBatchRequest);
            bool flag = GameUtils.IsTavernBrawlGameType(gameType);
            if (gameType == PegasusShared.GameType.GT_VS_FRIEND && FriendChallengeMgr.Get().IsChallengeTavernBrawl())
            {
                NetCacheFeatures netObject = Get().GetNetObject<NetCacheFeatures>();
                if (netObject != null && netObject.FriendWeekAllowsTavernBrawlRecordUpdate && SpecialEventManager.Get().IsEventActive(SpecialEventType.FRIEND_WEEK, false))
                {
                    flag = true;
                }
            }
            if (flag)
            {
                TavernBrawlManager.Get().RefreshPlayerRecord();
            }
            if (GameUtils.IsFiresideGatheringGameType(gameType))
            {
                Network.Get().RequestFSGPatronListUpdate();
            }
        }


        private void NetCacheMakeBatchRequest(NetCacheBatchRequest batchRequest)
        {
            List<GetAccountInfo.Request> list = new List<GetAccountInfo.Request>();
            List<GenericRequest> list2 = null;
            foreach (KeyValuePair<Type, Request> request in batchRequest.m_requests)
            {
                Request value = request.Value;
                if (value == null)
                {
                    LogManager.WriteToErrorFile(string.Format("NetUseBatchRequest Null request for {0}...SKIP", value.m_type.Name));
                }
                else if (m_ServerInitiatedAccountInfoTypes.Contains(value.m_type))
                {
                    if (value.m_reload)
                    {
                        Log.All.PrintWarning("Attempting to reload server-initiated NetCache request {0}. This is not valid - the server sends this data when it changes!", value.m_type.FullName);
                    }
                }
                else
                {
                    if (value.m_reload)
                    {
                        m_netCache[value.m_type] = null;
                    }
                    if ((!m_netCache.ContainsKey(value.m_type) || m_netCache[value.m_type] == null) && !m_inTransitRequests.Contains(value.m_type))
                    {
                        value.m_result = RequestResult.PENDING;
                        m_inTransitRequests.Add(value.m_type);
                        GetAccountInfo.Request value2;
                        int value3;
                        if (m_getAccountInfoTypeMap.TryGetValue(value.m_type, out value2))
                        {
                            list.Add(value2);
                        }
                        else if (m_genericRequestTypeMap.TryGetValue(value.m_type, out value3))
                        {
                            if (list2 == null)
                            {
                                list2 = new List<GenericRequest>();
                            }
                            GenericRequest genericRequest = new GenericRequest();
                            genericRequest.RequestId = value3;
                            list2.Add(genericRequest);
                        }
                        else
                        {
                            LogManager.WriteToErrorFile("NetCache: Unable to make request for type=" + value.m_type.FullName);
                        }
                    }
                }
            }
            if (list.Count > 0 || list2 != null)
            {
                Network.Get().RequestNetCacheObjectList(list, list2);
            }
            if (m_cacheRequests.FindIndex((NetCacheBatchRequest o) => o.m_callback == batchRequest.m_callback) >= 0)
            {
                LogManager.WriteToErrorFile("NetCache: detected multiple registrations for same callback!" + batchRequest.m_callback.Target.GetType().Name + "    " + batchRequest.m_callback.Method.Name);
            }
            m_cacheRequests.Add(batchRequest);
            NetCacheCheckRequest(batchRequest);
        }

        private void NetCacheCheckRequest(NetCacheBatchRequest request)
        {
            foreach (KeyValuePair<Type, Request> request2 in request.m_requests)
            {
                if (!m_netCache.ContainsKey(request2.Key) || m_netCache[request2.Key] == null)
                {
                    return;
                }
            }
            request.m_canTimeout = false;
            request.m_callback?.Invoke();
        }
    }
}
