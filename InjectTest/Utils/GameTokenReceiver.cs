﻿using InjectTest.Communication;
using InjectTest.Contracts;
using InjectTest.Engine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace InjectTest.Utils
{
    public static class GameTokenReceiver
    {
        private static string ADRESS = @""; //http://localhost:4297
        private static string PORT = "4297";
        private const string PATH = "_TestFolder";

        static GameTokenReceiver()
        {
            var configurationString = File.ReadAllText(EvoloEngineConstants.ConfigurationFilePath);
            var configuration = EvoloJsonSerializer.Deserialize<EvoloConfiguration>(configurationString);
            var ip = configuration.BotEndpoint.Substring(0, configuration.BotEndpoint.IndexOf(":", 10) + 1);
            ADRESS = ip + PORT;
            LogManager.WriteToErrorFile("Adress: " + ADRESS);
        }

        private static JObject ServerRequest(string name, string data)
        {
            try
            {
                using (var wb = new WebClient())
                {
                    var j = new NameValueCollection();
                    j["name"] = name;
                    j["path"] = PATH;
                    j["msg"] = data;

                    var response = wb.UploadValues(ADRESS, "POST", j);
                    var responseInString = Encoding.UTF8.GetString(response);
                    return JObject.Parse(responseInString);
                }
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile("[GameTokenReceiver.ServerRequest]: Error: " + e.ToString());
                return JObject.Parse(@"{ }");
            }
        }

        public static string GetGameToken()
        {
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            string mail = "", pass = "", server = "";
            for (var i = 0; i < commandLineArgs.Length; i++)
            {
                var arg = commandLineArgs[i];
                if (arg.Contains("-email") && i + 1 < commandLineArgs.Length)
                    mail = commandLineArgs[i + 1];
                if (arg.Contains("-pass") && i + 1 < commandLineArgs.Length)
                    pass = commandLineArgs[i + 1];
                if (arg.Contains("-server") && i + 1 < commandLineArgs.Length)
                    server = commandLineArgs[i + 1];
            }
            if (server != "")
                server = server.Substring(0, 2);
            else
                server = "eu";

            var accData = new Dictionary<string, string>
            {
                { "email", mail },
                { "pass", pass },
                { "region", server}
            };

            var h = ServerRequest("GetToken", JsonConvert.SerializeObject(accData));
            if (h.TryGetValue("Error", out JToken value))
                return "";
            try
            {
                var success = h.Value<bool>("Success");
                if (success)
                {
                    if (InjectMain.IsDevBot)
                        return h.Value<string>("token");
                    else
                        return "";
                }
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile("[GameTokenReceiver.GetGameToken]: " + e);
            }
            return "";
        }
    }
}
