﻿
using Hearthstone;
using InjectTest.Pegasus.Internal;
using InjectTest.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

namespace InjectTest.Hooks
{
    public static class EvoloGameHooks
    {
        /// <summary>
        ///  Gets or sets a value indicating whether PlayZoneSlotMousedOver is enabled.
        /// </summary>
        public static bool EnablePlayZoneSlotMousedOver { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether EnableLockMousePosition is enabled.
        /// </summary>
        public static bool EnableLockMousePosition { get; set; }

        /// <summary>
        /// Gets or sets the value of PlayZoneSlotMousedOver.
        /// </summary>
        public static int PlayZoneSlotMousedOverValue { get; set; }

        /// <summary>
        /// The method to filter the return value of PlayZoneSlotMousedOver.
        /// </summary>
        /// <param name="position">The original position</param>
        /// <returns>The updated position</returns>
        public static int PlayZoneSlotMousedOver(int position)
        {
            if (EnablePlayZoneSlotMousedOver)
            {
                int count = GameState.Get().GetFriendlySidePlayer().GetBattlefieldZone().GetCardCount();
                int result = PlayZoneSlotMousedOverValue;

                if (result < 0 || result > count)
                {
                    // By default, the position will be the middle slot
                    result = GameState.Get().GetFriendlySidePlayer().GetBattlefieldZone().GetCardCount() / 2;
                }

                Console.WriteLine("SlotPosition " + position + " >>>>>>>>> " + result);
                return result;
            }
            else
            {
                return position;
            }
        }

        /// <summary>
        /// Return the center of the stage when EnableLockMousePosition is true.
        /// </summary>
        /// <param name="position">The original position</param>
        /// <returns>The center of the stage when EnableLockMousePosition is true</returns>
        public static Vector3 GetMousePosition(Vector3 position)
        {
            if (!EnableLockMousePosition)
            {
                return position;
            }

            // The center of the stage is the 8/15 of the screen. 
            return Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5334f, 0.0f));
        }

        public static string GetLaunchOption(string option)
        {
            if (option.Contains("ruRU"))
                return option;
            /*
            else if (option.Contains("EU") || option.Contains("US") || option.Contains("CN"))
            {
                return "EU";
                string[] commandLineArgs = Environment.GetCommandLineArgs();
                string server = "EU";
                for (var i = 0; i < commandLineArgs.Length; i++)
                {
                    var arg = commandLineArgs[i];
                    if (arg.Contains("-server") && i + 1 < commandLineArgs.Length)
                        server = commandLineArgs[i + 1];
                }
                return server;
            }
            */

            else
            {
                if (InjectMain.LastLaunchToken.Length > 0)
                    return InjectMain.LastLaunchToken;

                string token = "";
                string[] commandLineArgs = Environment.GetCommandLineArgs();
                for (var i = 0; i < commandLineArgs.Length; i++)
                {
                    var arg = commandLineArgs[i];
                    if (arg.Contains("-token") && i + 1 < commandLineArgs.Length)
                        token = commandLineArgs[i + 1];
                }
                if (token.Length > 0)
                {
                    InjectMain.LastLaunchToken = token;
                    return token;
                }

                token = GameTokenReceiver.GetGameToken();

                LogManager.WriteToErrorFile("[EvoloGameHooks.Token]: "+ token);
                if (token == "" || token == null)
                {
                    InjectMain.IsBotEnabled = true;
                    Application.Quit();
                }
                InjectMain.LastLaunchToken = token;
                return token;
            }
        }

        public static void OnFatalError()
        {
            InjectMain.IsBotEnabled = true;
            Application.Quit();
        }

        private static bool alreadyRelaunched = false;
        public static void RelaunchHS()
        {
            if (!InjectMain.IsBotEnabled || alreadyRelaunched) return;

            string[] commandLineArgs = Environment.GetCommandLineArgs();
            string email = "", pass = "", sandNumber = "", device = "-tablet";
            for (var i = 0; i < commandLineArgs.Length; i++)
            {
                var arg = commandLineArgs[i];
                if (arg.Contains("-email") && i + 1 < commandLineArgs.Length)
                    email = commandLineArgs[i + 1];
                if (arg.Contains("-pass") && i + 1 < commandLineArgs.Length)
                    pass = commandLineArgs[i + 1];
                if (arg.Contains("-sand") && i + 1 < commandLineArgs.Length)
                    sandNumber = commandLineArgs[i + 1];
                if (arg.Contains("-ipad"))
                    device = "-ipad";
                else if (arg.Contains("-android"))
                    device = "-android";
                else if (arg.Contains("-ios"))
                    device = "-ios";
                else if (arg.Contains("-tablet"))
                    device = "-tablet";
            }

            var start_args = "-launch " + device + " -sand " + sandNumber + " -email " + email + " -pass " + pass;
            if (InjectMain.LastLaunchToken.Length > 0)
                start_args += "-token " + InjectMain.LastLaunchToken;
            var dir = Directory.GetCurrentDirectory() + "\\Hearthstone.exe";
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = dir;
            p.StartInfo.Arguments = start_args;
            p.Start();

            alreadyRelaunched = true;
            Application.Quit();
        }


        public static string GetTargetServer(string server_off)
        {
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            for (var i = 0; i < commandLineArgs.Length; i++)
            {
                var arg = commandLineArgs[i];
                if (arg.Contains("-android"))
                {
                    PlatformSettings.s_os = OSCategory.Android;
                    PlatformSettings.s_input = InputCategory.Touch;
                    PlatformSettings.s_screen = ScreenCategory.Phone;
                    PlatformSettings.s_screenDensity = ScreenDensityCategory.High;
                }
                else if (arg.Contains("-ipad"))
                {
                    PlatformSettings.s_os = OSCategory.iOS;
                    PlatformSettings.s_input = InputCategory.Touch;
                    PlatformSettings.s_screen = ScreenCategory.MiniTablet;
                    PlatformSettings.s_screenDensity = ScreenDensityCategory.Normal;

                }
                else if (arg.Contains("-ios"))
                {
                    PlatformSettings.s_os = OSCategory.iOS;
                    PlatformSettings.s_input = InputCategory.Touch;
                    PlatformSettings.s_screen = ScreenCategory.Phone;
                    PlatformSettings.s_screenDensity = ScreenDensityCategory.High;
                }
                else if (arg.Contains("-tablet"))
                {
                    PlatformSettings.s_os = OSCategory.Android;
                    PlatformSettings.s_input = InputCategory.Touch;
                    PlatformSettings.s_screen = ScreenCategory.Tablet;
                    PlatformSettings.s_screenDensity = ScreenDensityCategory.High;
                }
            }


            string server = "eu.actual.battle.net";
            for (var i = 0; i < commandLineArgs.Length; i++)
            {
                var arg = commandLineArgs[i];
                if (arg.Contains("-server") && i + 1 < commandLineArgs.Length)
                    server = commandLineArgs[i + 1];
            }
            return server;
        }
    }
}
