﻿using Hearthstone;
using Hearthstone.Progression;
using Hearthstone.UI;
using InjectTest.Bot;
using InjectTest.Engine;
using InjectTest.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using static NetCache;

namespace InjectTest.Hooks
{
    public class EvoloGUI : MonoBehaviour
    {
        //Private
        private EvoloEngine rockEngine;
        private bool isDisenchanting, isSavingCardInfo, isHiding, IsUIClicking, deviceDataReaded;
        private Coroutine coroutine, uiclick_routine;
        private const int BOT_FPS = 8, NORMAL_FPS = 60;
        private const int WIDTH = 400, HEIGHT = 450;

        //Public Static
        public static bool IsSellReady, IsMedalReceived;

        void Start()
        {
            LogManager.WriteToFile("GUI started");
            StartCoroutine(FocusFix());
            if (Screen.currentResolution.width > WIDTH || Screen.currentResolution.height > HEIGHT)
                Screen.SetResolution(WIDTH, HEIGHT, false);
            try
            {
                DeviceDataSaver.LoadConfig();

                rockEngine = new EvoloEngine();
                InjectMain.IsBotEnabled = true;
                coroutine = this.StartCoroutine(this.EvoloRoutine());
                EvoloConfigFile.LoadConfig();
            }
            catch (Exception e)
            {
                LogManager.WriteToFile("Start Exception: " + e);
            }
        }

        IEnumerator FocusFix()
        {

            HearthstoneApplication hsApplication = null;
            while (hsApplication == null)
            {
                hsApplication = GameObject.FindObjectOfType<HearthstoneApplication>();
                LogManager.WriteToFile("[FocusFix]: Finding `HearthstoneApplication` ...");
                yield return new WaitForSeconds(1f);
            }
            LogManager.WriteToFile("[FocusFix]: Found: " + hsApplication);
            yield return new WaitForSeconds(60f);
            try
            {
                LogManager.WriteToFile("[FocusFix]: 1");
                var field = hsApplication.GetType().GetField("m_focusChangedListeners", BindingFlags.NonPublic | BindingFlags.Instance);
                LogManager.WriteToFile("[FocusFix]: 2. Field: " + field);
                var list = field.GetValue(hsApplication);
                LogManager.WriteToFile("[FocusFix]: 3");
                var temp_list = (IList)list;
                LogManager.WriteToFile("[FocusFix]: 4");
                temp_list.Clear();

                LogManager.WriteToFile("FocusFix SUCCESSFULLY");
            }
            catch (Exception e)
            {
                LogManager.WriteToFile("FocusFix Exception: " + e);
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
                isHiding = !isHiding;

            if (Input.GetKeyDown(KeyCode.F2))
                foreach (var go in GameObject.FindObjectsOfType<Component>())
                {
                    if (go is Transform
                        || go is VisualController
                        || go is WidgetInstance
                        || go is MeshFilter
                        || go is MeshRenderer
                        || go is UberText
                        || go is UIBHighlight
                        || go.GetType().ToString().Contains("UnityEngine")
                        || go.GetType().ToString().Contains("PlayMakerFSM")
                        || go.GetType().ToString().Contains("Camera")
                        || go is StorePackDef
                        || go is SoundDef
                        || go is WidgetTransform
                        || go is WidgetTemplate
                        || go is Spell
                        || go is BoxEventMgr
                        || go is HighlightState
                        || go is TransformOverride
                        || go is HSDontDestroyOnLoad
                        || go is PegUIElementProxy
                        )
                        continue;

                    LogManager.WriteToErrorFile("GameObject: " + go.name + "  " + go.GetType());
                }
            if (Input.GetKeyDown(KeyCode.F3))
            {
                this.rockEngine.ShowInfo("UIClicking: " + IsUIClicking);
                LogManager.WriteToErrorFile("F2 - Start: IsUIClicking - " + IsUIClicking);

                if (!IsUIClicking)
                    uiclick_routine = StartCoroutine(TestClickUI());
                else
                {
                    StopCoroutine(uiclick_routine);
                    uiclick_routine = null;
                    IsUIClicking = false;
                }
            }
            if (Input.GetKeyDown(KeyCode.F4))
            {
                DeviceDataSaver.Get().CompleteCurrentDevice();
            }
        }

        IEnumerator TestClickUI()
        {
            LogManager.WriteToErrorFile("F2 - TestClickUI ");
            var totalCount = 0;
            IsUIClicking = true;

            var peg = GameObject.FindObjectsOfType<PegUIElement>();
            totalCount += peg.Length;

            var c = 0;
            foreach (var b in peg)
            {
                c++;
                try
                {
                    b.TriggerRelease();
                }
                catch (Exception e)
                {
                    LogManager.WriteToErrorFile("[EvoloGUI.TestClickUI.peg]: C =  " + c + "    Error: " + e);
                }
                var text = c + " / " + totalCount + " " + b.name + " " + b.GetType();
                LogManager.WriteToErrorFile("[EvoloGUI.TestClickUI.peg]: " + text);
                this.rockEngine.ShowInfo(text);

                yield return new WaitForSeconds(0.5f);
            }
            IsUIClicking = false;
            LogManager.WriteToErrorFile("[EvoloGUI.TestClickUI]: Succesfully ended!");
            yield break;
        }

        /// <summary>
        /// Main loop of Hearthrock.
        /// </summary>
        /// <returns>IEnumerator of Unity async tasks.</returns>
        private IEnumerator EvoloRoutine()
        {
            var f = false;
            var preloaded = false;

            while (InjectMain.IsBotEnabled)
            {
                double delay = 3;
                try
                {
                    Application.targetFrameRate = BOT_FPS;
                    if (!preloaded)
                    {
                        this.rockEngine.Reload();
                        this.rockEngine.ShowInfo("Bot Started");
                        if (InjectMain.IsDevBot)
                            this.rockEngine.configuration.GameMode = Contracts.EvoloGameMode.NormalPractice;
                        else
                            this.rockEngine.configuration.GameMode = Contracts.EvoloGameMode.Ranked;
                        preloaded = true;
                        EvoloConfigFile.Instance.heroClass = (TAG_CLASS)2;
                    }
                    if (!f)
                    {
                        delay = 10;
                        f = true;
                        preloaded = false;
                    }

                    /*
                    //Class level checking
                    for (var i = 0; i < 10; i++)
                        if (IsTenLevelClass(EvoloConfigFile.Instance.heroClass))
                        {
                            if (IsTenLevelClass((TAG_CLASS)10))
                                EvoloConfigFile.Instance.heroClass = (TAG_CLASS)2;
                            else
                                EvoloConfigFile.Instance.heroClass = (TAG_CLASS)((int)EvoloConfigFile.Instance.heroClass + 1);
                        }
                    */
                    EvoloConfigFile.Instance.heroClass = TAG_CLASS.MAGE;
                    //Medal farm => Level farm check
                    {
                        var medal = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(PegasusShared.FormatType.FT_CLASSIC).GetMedalText();
                        if (!int.TryParse(medal, out _))
                            medal = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(PegasusShared.FormatType.FT_CLASSIC).GetRankName();
                        IsMedalReceived = medal.Contains("X") || medal.Contains("V") || medal.Contains("I") || medal.Contains("Бронза");
                        if (IsSellReady || IsMedalReceived)
                        {
                            if (DeviceDataSaver.Get().IsAllDevicesComplete())
                                this.rockEngine.configuration.GameMode = Contracts.EvoloGameMode.Casual;
                            else
                                this.rockEngine.configuration.GameMode = Contracts.EvoloGameMode.Ranked;
                            if (!IsSellReady)
                            {
                                int gold = 0, packs = 0;
                                var netCache = NetCache.Get();

                                netCache.RefreshNetObject<NetCacheGamesPlayed>();
                                netCache.RefreshNetObject<NetCacheHeroLevels>();
                                netCache.RefreshNetObject<NetCacheBoosters>();
                                //Gold
                                gold = (int)netCache.GetGoldBalance();
                                //Packs
                                var boostersData = netCache.GetNetObject<NetCacheBoosters>();
                                if (boostersData != null)
                                    packs = boostersData.GetTotalNumBoosters();
                                if (packs + gold / 100 >= 65)
                                    IsSellReady = true;
                            }
                        }
                    }

                    /*
                    if(IsMedalReceived)
                    {
                        Options.Get().SetBool(Option.HAS_CLICKED_TOURNAMENT, true);
                        Options.Get().SetBool(Option.HAS_SEEN_TOURNAMENT, true);
                        Options.SetFormatType(PegasusShared.FormatType.FT_STANDARD);
                        Options.SetInRankedPlayMode(true);
                    }*/
                    
                    this.rockEngine.Tick();

                    delay = this.rockEngine.Update();
                    if (!InjectMain.IsDevBot)
                        delay *= 3;

                    /*
                    if (this.isOpeningPacksEnabled)
                    {
                        // this.OpenPacks();
                    }
                    */
                }
                catch (Exception e)
                {
                    LogManager.WriteToFile("Exception: " + e);
                }
                yield return new WaitForSeconds((float)delay);
            }
        }

        private bool IsTenLevelClass(TAG_CLASS heroClass)
        {
            var net = NetCache.Get().GetNetObject<NetCacheHeroLevels>();
            if (net == null) return false;
            foreach (var c in net.Levels)
                if (c.Class == heroClass)
                    return c.CurrentLevel.Level >= 10;
            return false;
        }

        private IEnumerator Disenchant()
        {
            var page_manager = (CollectionPageManager)FindObjectOfType<BookPageManager>();
            var brokenList = new List<string>();
            var craftingManager = CraftingManager.Get();
            while (isDisenchanting)
            {
                var c = 0;
                Actor card_actor = null;

                try
                {
                    var actors = FindObjectsOfType<Actor>();
                    foreach (var actor in actors)
                    {
                        if (c >= 8)
                            break;
                        //if (actor.GetRarity() == TAG_RARITY.COMMON || actor.GetRarity() == TAG_RARITY.RARE)
                        {
                            if (brokenList.Contains(actor.GetNameText().Text))
                                continue;
                            card_actor = actor;
                            break;
                        }
                        c++;
                    }
                }
                catch (Exception e) { LogManager.WriteToFile("Disenchant: " + e); }
                if (card_actor == null)
                    page_manager.m_pageRightClickableRegion?.TriggerRelease();
                else
                {
                    try
                    {
                        craftingManager.EnterCraftMode(card_actor, null);
                    }
                    catch (Exception e) { LogManager.WriteToFile("Disenchant: " + e); }
                    yield return new WaitForSeconds(0.5f);
                    try
                    {
                        if (CraftingManager.GetIsInCraftingMode())
                        {
                            LogManager.WriteToFile("Disenchant: " + card_actor.GetNameText().Text + " Normal card: " + CraftingManager.Get()?.m_craftingUI?.m_buttonDisenchant.IsButtonEnabled());
                            var isNormal = CraftingManager.Get()?.m_craftingUI?.m_buttonDisenchant.IsButtonEnabled();
                            if (isNormal != null && isNormal == true)
                                craftingManager.DisenchantButtonPressed();
                            else
                                brokenList.Add(card_actor.GetNameText().Text);
                        }
                    }
                    catch (Exception e) { LogManager.WriteToFile("Disenchant: " + e); }
                    yield return new WaitForSeconds(0.1f);
                    try
                    {
                        craftingManager.CancelCraftMode();
                    }
                    catch (Exception e) { LogManager.WriteToFile("Disenchant: " + e); }
                    yield return new WaitForSeconds(1.5f);
                }

                yield return new WaitForSeconds(0.1f);
            }
        }

        private IEnumerator DisenchantByTransaction()
        {
            NetCache.Get().RefreshNetObject<NetCache.NetCacheCardValues>();
            yield return new WaitForSeconds(3f);
            NetCache.Get().RefreshNetObject<NetCache.NetCacheCardValues>();
            yield return new WaitForSeconds(3f);
            var cardValues = NetCache.Get().GetNetObject<NetCache.NetCacheCardValues>();
            LogManager.WriteToErrorFile("Values: " + cardValues?.Values?.Count);

            var c = 0;
            var count = cardValues.Values.Count;
            int numCopiesInCollection = 0, assetId = 0;
            foreach (var kv in cardValues.Values)
            {
                //LogManager.WriteToErrorFile($"Name: {kv.Key.Name} Premium: {kv.Key.Premium} Value: {kv.Value.GetSellValue()}");

                try
                {
                    string cardId = kv.Key.Name;
                    var cardType = kv.Key.Premium;
                    var value = kv.Value.GetSellValue();
                    //EntityDef entityDef = DefLoader.Get().GetEntityDef(cardId);
                    //var cardID = GameUtils.TranslateDbIdToCardId(entityDef.GetTag(GAME_TAG.DECK_RULE_COUNT_AS_COPY_OF_CARD_ID));
                    // LogManager.WriteToErrorFile("[EvoloGUI.DisenchantByTransaction]: " + assetId + " " + actor.GetNameText().Text);
                    numCopiesInCollection = CollectionManager.Get().GetNumCopiesInCollection(cardId, cardType);
                    if (numCopiesInCollection > 0)
                    {
                        assetId = GameUtils.TranslateCardIdToDbId(cardId);
                        Network.Get().SellCard(assetId, cardType, -1, value, numCopiesInCollection);
                    }
                }
                catch (Exception e)
                {
                    LogManager.WriteToErrorFile("[EvoloGUI.DisenchantByTransaction]: " + e);
                    // break;
                }
                LogManager.WriteToErrorFile("[EvoloGUI.DisenchantByTransaction]: " + c + " / " + count + " Copies: " + numCopiesInCollection + " assetID: " + assetId);
                c++;

                //if (c % 5 == 0)
                if (numCopiesInCollection > 0)
                    yield return new WaitForSeconds(5f);
            }

        }

        private IEnumerator SaveCardInfo()
        {
            var page_manager = (CollectionPageManager)FindObjectOfType<BookPageManager>();
            List<string> cardsListLegendary = new List<string>();
            List<string> cardsListEpic = new List<string>();
            var lastNum = page_manager.CurrentPageNum;
            var lastEq = -1;
            while (isSavingCardInfo)
            {
                var c = 0;
                Actor card_actor = null;
                try
                {
                    var actors = FindObjectsOfType<Actor>();

                    foreach (var actor in actors)
                    {
                        if (c >= 8)
                            break;
                        if (actor.GetRarity() == TAG_RARITY.LEGENDARY || actor.GetRarity() == TAG_RARITY.EPIC)
                        {
                            var golden_postfix = actor.GetPremium() == TAG_PREMIUM.GOLDEN ? "(ЗОЛОТАЯ)" : "";
                            var name = actor.GetNameText().Text + golden_postfix;
                            if (actor.GetRarity() == TAG_RARITY.LEGENDARY)
                            {
                                if (cardsListLegendary.Contains(name))
                                    continue;
                                cardsListLegendary.Add(name);
                            }
                            else
                            {
                                if (cardsListEpic.Contains(name))
                                    continue;
                                cardsListEpic.Add(name);
                            }
                            card_actor = actor;
                            break;
                        }
                        c++;
                    }
                    if (card_actor == null)
                        page_manager?.m_pageRightClickableRegion?.TriggerRelease();
                }
                catch (Exception e) { LogManager.WriteToFile("SaveCardInfo_1: " + e); }
                yield return new WaitForSeconds(0.8f);
                try
                {
                    LogManager.WriteToFile(lastNum + " " + page_manager.CurrentPageNum);
                    if (lastNum > 20 && lastEq == lastNum && lastNum == page_manager.CurrentPageNum)
                        isSavingCardInfo = false;
                    else if (lastNum == page_manager.CurrentPageNum)
                        lastEq = lastNum;
                    lastNum = page_manager.CurrentPageNum;
                }
                catch (Exception e) { LogManager.WriteToFile("SaveCardInfo_2: " + e); }
                yield return new WaitForSeconds(0.1f);
            }
            if (!isSavingCardInfo)
            {
                LogManager.WriteSaveInfo("");
                var legends = "";
                foreach (var k in cardsListLegendary)
                    legends += k + ", ";
                var epics = "";
                foreach (var k in cardsListEpic)
                    epics += k + ", ";
                LogManager.WriteSaveInfo(cardsListLegendary.Count + " Легендарок: " + legends);
                LogManager.WriteSaveInfo(cardsListEpic.Count + " Эпических: " + epics);
            }
        }


        IEnumerator OpenAllPacks()
        {
            while (true)
            {
                var m_director = FindObjectOfType<PackOpeningDirector>();
                if (m_director == null)
                    yield break;

                LogManager.WriteToFile("Director: " + m_director);
                for (var k = 0; k < 3; k++)
                {
                    try
                    {
                        m_director.FinishPackOpen();
                        PackOpening.Get().SendMessage("AutomaticallyOpenPack");
                        // PackOpening.Get().StartCoroutine("OpenNextPackWhenReady");

                        if (PackOpeningDirector.QuickPackOpeningAllowed)
                            for (var i = 0; i < 5; i++)
                                m_director.ForceRevealRandomCard();
                    }
                    catch (Exception e)
                    {
                        LogManager.WriteToErrorFile("PACKS: " + e);
                    }

                    //yield return new WaitForSeconds(Time.fixedDeltaTime);
                }
                yield return new WaitForSeconds(Time.fixedDeltaTime * 10f);
            }
        }
        /// <summary>
        /// For OnGUI Message of MonoBehaviour.
        /// </summary>
        public void OnGUI()
        {
            if (isHiding) return;
            int windowContentHeight = (EvoloUnityConstants.RockStatusHeight * 14)
                + EvoloUnityConstants.WindowTitleHeight
                + EvoloUnityConstants.RockButtonHeight
                + (EvoloUnityConstants.RockSpacing * 2);

            var windowWidth = EvoloUnityConstants.WindowContentWidth
                + ((EvoloUnityConstants.RockSpacing + EvoloUnityConstants.WindowBorderSize) * 2);
            var windowHeight = windowContentHeight
                + ((EvoloUnityConstants.RockSpacing + EvoloUnityConstants.WindowBorderSize) * 2);

            Rect rect = new Rect(Screen.width - windowWidth - EvoloUnityConstants.WindowPadding, EvoloUnityConstants.WindowPadding, windowWidth, windowHeight);

            GUI.ModalWindow(0, rect, this.OnWindow, EvoloUnityConstants.Title);
        }

        /// <summary>
        /// Main function of HearthrockUnity ModalWindow.
        /// </summary>
        /// <param name="windowID">The window id of an Unity Window.</param>
        private void OnWindow(int windowID)
        {
            int contentOffsetLeft = EvoloUnityConstants.RockSpacing + EvoloUnityConstants.WindowBorderSize;
            int buttonOffsetTop = EvoloUnityConstants.WindowBorderSize + EvoloUnityConstants.RockSpacing + EvoloUnityConstants.WindowTitleHeight;
            int statusOffsetTop = buttonOffsetTop + EvoloUnityConstants.RockButtonHeight + EvoloUnityConstants.RockSpacing;

            // The main rock button.
            if (GUI.Button(
                new Rect(contentOffsetLeft, buttonOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight),
                InjectMain.IsBotEnabled ? EvoloUnityConstants.EvoloButtonTitle_ON : EvoloUnityConstants.EvoloButtonTitle_OFF))
            {
                if (InjectMain.IsBotEnabled)
                {
                    LogManager.WriteToFile("RunButtonClick:  IsBotEnabled-> " + InjectMain.IsBotEnabled);
                    InjectMain.IsBotEnabled = false;
                    EvoloGameHooks.EnableLockMousePosition = false;

                    this.rockEngine.Reload();

                    this.rockEngine.ShowInfo("Bot Paused");
                    Application.targetFrameRate = NORMAL_FPS;
                    if (coroutine != null)
                    {
                        this.StopCoroutine(coroutine);
                        coroutine = null;
                    }
                }
                else
                {
                    LogManager.WriteToFile("RunButtonClick:  IsBotEnabled-> " + InjectMain.IsBotEnabled);
                    InjectMain.IsBotEnabled = true;

                    this.rockEngine.Reload();
                    this.rockEngine.ShowInfo("Bot Started");
                    if (coroutine != null)
                    {
                        this.StopCoroutine(coroutine);
                        coroutine = null;
                    }
                    coroutine = this.StartCoroutine(this.EvoloRoutine());
                }
            }

            int currentOffsetTop = statusOffsetTop;
            try
            {
                void AddLabel(string text)
                {
                    GUI.Label(new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockStatusHeight), text);
                    currentOffsetTop += EvoloUnityConstants.RockStatusHeight;
                }


                AddLabel($"Status: " + (InjectMain.IsBotEnabled ? "Running" : "Paused"));
                AddLabel("Class: " + EvoloConfigFile.Instance.heroClass.ToString());
                AddLabel("Search: " + EvoloConfigFile.Instance.heroClass.ToString());
                AddLabel("SellReady: " + IsSellReady);
                AddLabel("Medalled: " + IsMedalReceived);
                AddLabel("Device: " + (DeviceDataSaver.Get().GetCurrentDevice()));
            }
            catch
            {
            }

            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "AutoOpen Packs"))
            {
                try
                {

                    StartCoroutine(OpenAllPacks());
                }
                catch (Exception e) { LogManager.WriteToFile(e.ToString()); }
            }
            /*
            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "AutoDisanch cards"))
            {

                isDisenchanting = !isDisenchanting;

                this.rockEngine.ShowInfo("Disenchanting: " + isDisenchanting);

                if (isDisenchanting)
                    this.StartCoroutine(nameof(Disenchant));
                else
                    this.StopCoroutine(nameof(Disenchant));
            }
            */
            /*
            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "Save cards info"))
            {
                isSavingCardInfo = !isSavingCardInfo;

                this.rockEngine.ShowInfo("SavingCardInfo: " + isSavingCardInfo);

                if (isSavingCardInfo)
                    this.StartCoroutine(this.SaveCardInfo());
                else
                    this.StopCoroutine(this.SaveCardInfo());
            }*/

            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "SendAccountData"))
            {
                try
                {
                    EvoloEngine.Get().bot.OnAccountData();
                    this.rockEngine.ShowInfo("Account data send");
                }
                catch (Exception e)
                {
                    LogManager.WriteToErrorFile("ShowDataError: " + e);
                }
            }


            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "Claim rewards"))
            {
                for (var i = 0; i < 30; i++)
                    for (var j = 0; j < 30; j++)
                        RewardTrackManager.Get().ClaimRewardTrackReward(j, j, false);
            }

            /*
            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "Next device"))
            {
                DeviceDataSaver.Get().CompleteCurrentDevice();
            }
            
            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "Clear devices"))
            {
                try
                {
                    DeviceDataSaver.ClearConfig();
                    this.rockEngine.ShowInfo("Config cleared");
                }
                catch (Exception e)
                {

                }
            }
            */


            currentOffsetTop += EvoloUnityConstants.RockStatusHeight * 2;
            if (GUI.Button(
                new Rect(contentOffsetLeft, currentOffsetTop, EvoloUnityConstants.WindowContentWidth, EvoloUnityConstants.RockButtonHeight / 1.5f),
                "Start Rank"))
            {
                try
                {
                    var decks = GameObject.FindObjectsOfType<CollectionDeckBoxVisual>();
                    var exist = false;
                    long deckId = 0;
                    foreach (var d in decks)
                    {
                        if (d.GetClass() == TAG_CLASS.MAGE)
                        {
                            d.TriggerRelease();
                            exist = true;
                            deckId = d.GetDeckID();
                            break;
                        }
                    }
                    if (!exist)
                    {
                        this.rockEngine.ShowInfo("Deck not exist");
                        return;
                    }


                    GameMgr.Get().FindGame(PegasusShared.GameType.GT_RANKED, PegasusShared.FormatType.FT_STANDARD, 2, deckId: deckId);
                    this.rockEngine.ShowInfo("Started");
                }
                catch (Exception e)
                {
                    this.rockEngine.ShowInfo("Error");

                }
            }
        }

        T GetPrivateField<T>(object obj, string fieldName)
        {
            var field = obj.GetType().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
            return (T)field.GetValue(obj);
        }
    }
}
