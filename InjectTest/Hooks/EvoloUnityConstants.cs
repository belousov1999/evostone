﻿namespace InjectTest.Hooks
{
    public static class EvoloUnityConstants
    {
        
        public const string Title = "EvoStone";

        public const string EvoloButtonTitle_OFF = "Run";

        public const string EvoloButtonTitle_ON = "Pause";

        public const int WindowTitleHeight = 15;

        /// <summary>
        /// Size between the window and the game window edge.
        /// </summary>
        public const int WindowPadding = 15;

        /// <summary>
        /// Border size of the window.
        /// </summary>
        public const int WindowBorderSize = 2;

        /// <summary>
        /// Content width of the window.
        /// </summary>
        public const int WindowContentWidth = 150;

        /// <summary>
        /// Size between the elements inside the window.
        /// </summary>
        public const int RockSpacing = 8;

        /// <summary>
        /// Button height.
        /// </summary>
        public const int RockButtonHeight = 40;

        /// <summary>
        /// Status text height.
        /// </summary>
        public const int RockStatusHeight = 20;
    }
}
