﻿using InjectTest.Diagnostics;
using InjectTest.Pegasus.Internal;

namespace InjectTest.Pegasus
{
    public static class EvoloPegasusFactory
    {
        /// <summary>
        /// The factory method for IEvoloPegasus. 
        /// </summary>
        /// <param name="tracer">The RockTracer.</param>
        /// <returns>A IRockPegasus instance.</returns>
        public static IEvoloPegasus CreatePegasus(EvoloTracer tracer)
        {
            return new EvoloPegasus(tracer);
        }
    }
}
