﻿using InjectTest.Contracts;

namespace InjectTest.Pegasus
{
    public interface IEvoloPegasus
    {
        /// <summary>
        /// Trigger some activity to make user looks active.
        /// </summary>
        void TriggerUserActive();


        void DoCloseRandomPopup();

        /// <summary>
        /// Close a general dialog if there is one.
        /// </summary>
        /// <returns>return false if there is no general dialog.</returns>
        bool DoCloseGeneralDialog();

        /// <summary>
        /// Close a quests dialog if there is one.
        /// </summary>
        /// <returns>return false if there is no quests dialog.</returns>
        bool DoCloseQuestsDialog();

        /// <summary>
        /// End current turn.
        /// </summary>
        void DoEndTurn();

        /// <summary>
        /// End a finished game.
        /// </summary>
        void DoEndFinishedGame();

        /// <summary>
        /// Get current Pegasus Scene State.
        /// </summary>
        /// <returns>The RockPegasusSceneState.</returns>
        EvoloPegasusSceneState GetPegasusSceneState();

        /// <summary>
        /// Get current Pegasus Subscene State.
        /// </summary>
        /// <param name="sceneState">The current RockPegasusSceneState.</param>
        /// <returns>The RockPegasusSubsceneState.</returns>
        EvoloPegasusSubsceneState GetPegasusSubsceneState(EvoloPegasusSceneState sceneState);

        /// <summary>
        /// Get current Pegasus Game State.
        /// </summary>
        /// <returns>The RockPegasusGameState.</returns>
        EvoloPegasusGameState GetPegasusGameState();

        /// <summary>
        /// Navigate to Hub Scene.
        /// </summary>
        void NavigateToHubScene();

        /// <summary>
        /// Navigate to Tournament Scene.
        /// </summary>
        void NavigateToTournamentScene();

        /// <summary>
        /// Navigate to Adventure Scene.
        /// </summary>
        void NavigateToAdventureScene();

        /// <summary>
        /// Start a practice (PVE) game.
        /// </summary>
        void PlayPracticeGame();

        /// <summary>
        /// Start a tournament (PVP) game.
        /// </summary>
        void PlayTournamentGame();

        /// <summary>
        /// Config deck for a game.
        /// </summary>
        /// <param name="index">The index of deck.</param>
        bool ConfigDeck();

        /// <summary>
        /// Config opponent for practice game.
        /// </summary>
        /// <param name="index">The index of opponent.</param>
        void ConfigPracticeOpponent(int index);

        /// <summary>
        /// Config mode for practice game.
        /// </summary>
        /// <param name="expert">If play expert mode.</param>
        void ConfigPracticeMode(bool expert);

        /// <summary>
        /// Config mode for tournament game.
        /// </summary>
        /// <param name="ranked">If play ranked mode.</param>
        /// <param name="wild">If play wild format.</param>
        void ConfigTournamentMode(bool ranked, bool wild);

        /// <summary>
        /// Get a rock object with RockId.
        /// </summary>
        /// <param name="rockId">The RockId.</param>
        /// <returns>The pegasus object.</returns>
        IEvoloObject GetObject(int rockId);

        /// <summary>
        /// Click a rock object with RockId.
        /// </summary>
        /// <param name="rockId">The RockId.</param>
        void ClickObject(int rockId);

        /// <summary>
        /// Drop a holding pegasus object.
        /// </summary>
        void DropObject();

        /// <summary>
        /// Snapshot current scene.
        /// </summary>
        /// <param name="sessionId">The Session Id.</param>
        /// <param name="actionId">The Action Id.</param>
        /// <returns>The RockScene.</returns>
        EvoloScene SnapshotScene(string sessionId, int actionId);
    }
}
