﻿namespace InjectTest.Pegasus
{
    /// <summary>
    /// Subscene State for Pegasus.
    /// </summary>
    public enum EvoloPegasusSubsceneState
    {
        /// <summary>
        /// The None.
        /// </summary>
        None,

        /// <summary>
        /// Wait for choose a mode.
        /// </summary>
        WaitForChooseMode,

        /// <summary>
        /// Wait for choose a deck.
        /// </summary>
        WaitForChooseDeck,

        /// <summary>
        /// Wait for choose an opponent.
        /// </summary>
        WaitForChooseOpponent,

        /// <summary>
        /// All set.
        /// </summary>
        Ready
    }
}
