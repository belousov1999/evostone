﻿using System;

namespace InjectTest.Pegasus.Request
{
    [Serializable]
    public class ReadyPlay
    {
        public string nickname;
        public string medalLevel;
        public string email;

        public ReadyPlay(string nickname, string medalLevel, string email)
        {
            this.nickname = nickname;
            this.medalLevel = medalLevel;
            this.email = email;
        }
    }
}
