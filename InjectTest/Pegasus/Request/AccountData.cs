﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InjectTest.Pegasus.Request
{
    [Serializable]
    public class AccountData
    {
        public string medalLevel;
        public string email;
        public string pass;
        public string sandNumber;

        public int GoldCount;
        public int TotalGamesCount;
        public int TotalLevelsCount;
        public int BoostersCount;

        public AccountData(string medalLevel,string email, string pass, string sandNumber, int goldCount, int totalGamesCount, int totalLevelsCount, int boostersCount)
        {
            this.medalLevel = medalLevel;
            this.email = email;
            this.pass = pass;
            this.sandNumber = sandNumber;
            GoldCount = goldCount;
            TotalGamesCount = totalGamesCount;
            TotalLevelsCount = totalLevelsCount;
            BoostersCount = boostersCount;
        }
    }
}
