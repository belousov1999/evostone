﻿namespace InjectTest.Pegasus
{
    /// <summary>
    /// GameState of Pegasus
    /// </summary>
    public enum EvoloPegasusGameState
    {
        /// <summary>
        /// The None.
        /// </summary>
        None,

        /// <summary>
        /// Game is over.
        /// </summary>
        GameOver,

        /// <summary>
        /// Wait for mulligan action.
        /// </summary>
        WaitForMulligan,

        /// <summary>
        /// Wait for play action.
        /// </summary>
        WaitForPlay,

        /// <summary>
        /// Blocking by opponent, system, or some animation.
        /// </summary>
        Blocking
    }
}
