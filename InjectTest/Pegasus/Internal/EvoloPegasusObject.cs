﻿using InjectTest.Contracts;

namespace InjectTest.Pegasus.Internal
{
    /// <summary>
    /// Pegasus Object
    /// </summary>
    internal class EvoloPegasusObject : IEvoloObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RockPegasusObject" /> class.
        /// </summary>
        /// <param name="card">The Pegasus Card.</param>
        public EvoloPegasusObject(Card card)
        {
            this.PegasusCard = card;
        }

        /// <summary>
        /// Gets the Pegasus Card.
        /// </summary>
        public Card PegasusCard { get; }

        /// <summary>
        /// Gets the Pegasus Entity.
        /// </summary>
        public Entity PegasusEntity
        {
            get
            {
                return this.PegasusCard.GetEntity();
            }
        }

        /// <summary>
        /// Gets the CardId.
        /// </summary>
        public string CardId
        {
            get
            {
                return this.PegasusEntity.GetCardId();
            }
        }

        /// <summary>
        /// Gets the RockId.
        /// </summary>
        public int RockId
        {
            get
            {
                return this.PegasusEntity.GetEntityId();
            }
        }

        /// <summary>
        /// Gets the Name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.PegasusEntity.GetName();
            }
        }
    }
}
