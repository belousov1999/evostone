﻿using InjectTest.Contracts;
using InjectTest.Diagnostics;
using InjectTest.Engine;
using InjectTest.Hooks;
using InjectTest.Utils;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using static Assets.AdventureData;
using Hearthstone.Progression;
using InjectTest.Bot;

namespace InjectTest.Pegasus.Internal
{
    public class EvoloPegasus : IEvoloPegasus
    {
        /// <summary>
        /// The EvoloTracer.
        /// </summary>
        private EvoloTracer tracer;

        private float lastClosedTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="EvoloPegasus" /> class.
        /// </summary>
        /// <param name="tracer">The EvoloTracer.</param>
        public EvoloPegasus(EvoloTracer tracer)
        {
            this.tracer = tracer;
        }

        /// <summary>
        /// Trigger some activity to make user looks active.
        /// </summary>
        public void TriggerUserActive()
        {
            InactivePlayerKicker ipk = InactivePlayerKicker.Get();
            if (ipk == null)
            {
                // InactivePlayerKicker seesm to be used to make sure use is alive, but still need to confirm this.
                this.tracer.Verbose("InactivePlayerKicker is not available.");
                return;
            }
            //ipk.SetKickSec
            ipk.SetShouldCheckForInactivity(false);
        }

        public void DoCloseRandomPopup()
        {
            // var banners = GameObject.FindObjectsOfType<PegUIElement>();
            // foreach (var mono in banners)
            // {
            //    LogManager.WriteToErrorFile("PegUIElement: " + mono.name+" "+mono.GetType()+ " "+ mono.GetInteractionState());
            // }
            //QUEST POPUPS
            var questPopup = GameObject.FindObjectOfType<QuestNotificationPopup>();
            if (questPopup != null)
                questPopup.Hide();

            //RANDOM REWARDS (GOLD, PACKS)
            var rewardScroll = GameObject.FindObjectOfType<RewardScroll>();
            if (rewardScroll != null)
            {
                var dynMethod = typeof(RewardScroll).GetMethod("Hide", BindingFlags.NonPublic | BindingFlags.Instance);
                dynMethod.Invoke(rewardScroll, new object[] { });
            }

            //GameEnd reward skip
            //GameEnd level card skip
            //Reward Chest on Ranked Open
            {
                foreach (var go in GameObject.FindObjectsOfType<PegUIElement>())
                    if (go.name.Contains("Hitbox")
                        || go.name.Contains("ClickCatcher")
                        || go.name.Contains("RankReward_Chest20"))
                    {
                        go.TriggerRelease();
                    }

            }

            //Chest Inner Rewards
            {
                try
                {
                    foreach (var go in GameObject.FindObjectsOfType<RewardPackage>())
                        go.TriggerRelease();
                }
                catch { }
                try
                {
                    foreach (var go in GameObject.FindObjectsOfType<NormalButton>())
                        go.TriggerRelease();
                }
                catch { }

            }

            //Ranked Popup
            {
                try
                {
                    var rankedPopup = GameObject.FindObjectOfType<RankedIntroPopup>();
                    if (rankedPopup != null)
                        rankedPopup.Hide();
                }
                catch { }
            }

            //Choose Startup Deck 
            try
            {
                var classDeckButton = GameObject.FindObjectsOfType<Hearthstone.UI.Clickable>();
                if (classDeckButton.Length > 0)
                {
                    var r = UnityEngine.Random.Range(0, classDeckButton.Length);

                    FieldInfo field = typeof(Hearthstone.UI.Clickable).GetField("m_pegUiElement", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                    var ui = (PegUIElement)field.GetValue(classDeckButton[r]);
                    if (ui != null)
                    {
                        ui.TriggerPress();
                        ui.TriggerTap();
                    }
                }
                foreach (var t in GameObject.FindObjectsOfType<Hearthstone.UI.Clickable>())
                    if (t.name.Contains("ButtonFramed"))
                    {
                        FieldInfo field = typeof(Hearthstone.UI.Clickable).GetField("m_pegUiElement", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                        var ui = (PegUIElement)field.GetValue(t);
                        if (ui != null)
                        {
                            ui.TriggerPress();
                            ui.TriggerTap();
                            ui.TriggerRelease();
                        }
                        break;
                    }
                foreach (var t in GameObject.FindObjectsOfType<UIBButton>())
                    if (t.name.Contains("ConfirmButton"))
                        t.TriggerRelease();

            }
            catch (Exception e) { LogManager.WriteToErrorFile(e + ""); }

            try
            {
                var recs = GameObject.FindObjectsOfType<ReconnectHelperDialog>();
                if (recs != null)
                    foreach (var r in recs)
                        r.m_choiceOneButton.TriggerRelease();
            }
            catch (Exception e)
            {
            }


        }

        /// <summary>
        /// Close a general dialog if there is one.
        /// </summary>
        /// <returns>return false if there is no general dialog.</returns>
        public bool DoCloseGeneralDialog()
        {
            if (DialogManager.Get() == null)
            {
                return false;
            }

            if (DialogManager.Get().ShowingDialog())
            {
                DialogManager.Get().GoBack();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Close a quests dialog if there is one.
        /// </summary>
        /// <returns>return false if there is no quests dialog.</returns>
        public bool DoCloseQuestsDialog()
        {
            WelcomeQuests wq = WelcomeQuests.Get();
            if (wq != null)
            {
                wq.m_clickCatcher.TriggerRelease();
                return true;
            }

            return false;
        }

        /// <summary>
        /// End current turn.
        /// </summary>
        public void DoEndTurn()
        {
            InputManager.Get().DoEndTurnButton();
        }

        /// <summary>
        /// End a finished game.
        /// </summary>
        public void DoEndFinishedGame()
        {
            if (EndGameScreen.Get() != null)
            {
                try
                {
                    EndGameScreen.Get().m_hitbox.TriggerRelease();
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Get current Pegasus Scene State.
        /// </summary>
        /// <returns>The EvoloPegasusSceneState.</returns>
        public EvoloPegasusSceneState GetPegasusSceneState()
        {
            if (WelcomeQuests.Get() != null)
            {
                return EvoloPegasusSceneState.QuestsDialog;
            }

            if (DialogManager.Get() != null)
            {
                if (DialogManager.Get().ShowingDialog())
                {
                    return EvoloPegasusSceneState.GeneralDialog;
                }
            }

            if (Network.Get().IsFindingGame())
            {
                return EvoloPegasusSceneState.BlockingScene;
            }

            if (GameMgr.Get().IsTransitionPopupShown())
            {
                return EvoloPegasusSceneState.BlockingScene;
            }

            if (PopupDisplayManager.Get().IsShowing)
            {
                return EvoloPegasusSceneState.RandomPopup;
            }

            var sceneMode = SceneMgr.Get().GetMode();

            var pegasusState = EvoloPegasusHelper.GetPegasusSceneState(sceneMode);

            if (pegasusState == EvoloPegasusSceneState.GamePlay)
            {
                if (GameState.Get() == null)
                {
                    return EvoloPegasusSceneState.BlockingScene;
                }
            }

            if (NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>().CampaignProgress
                != TutorialProgress.ILLIDAN_COMPLETE)
            {
                return EvoloPegasusSceneState.TutorialScene;
            }

            return pegasusState;
        }

        /// <summary>
        /// Get current Pegasus Subscene State.
        /// </summary>
        /// <param name="sceneState">The current EvoloPegasusSceneState.</param>
        /// <returns>The EvoloPegasusSubsceneState.</returns>
        public EvoloPegasusSubsceneState GetPegasusSubsceneState(EvoloPegasusSceneState sceneState)
        {
            switch (sceneState)
            {
                case EvoloPegasusSceneState.AdventureScene:
                    return this.GetPegasusAdventureSubsceneState();
                case EvoloPegasusSceneState.TournamentScene:
                    return this.GetPegasusTournamentSubsceneState();
                default:
                    return EvoloPegasusSubsceneState.None;
            }
        }

        /// <summary>
        /// Get current Pegasus Game State.
        /// </summary>
        /// <returns>The EvoloPegasusGameState.</returns>
        public EvoloPegasusGameState GetPegasusGameState()
        {
            GameState state = GameState.Get();

            if (state.IsBusy())
            {
                return EvoloPegasusGameState.Blocking;
            }
            else if (state.IsGameOver())
            {
                return EvoloPegasusGameState.GameOver;
            }
            else if (state.IsResponsePacketBlocked())
            {
                return EvoloPegasusGameState.Blocking;
            }
            else if (state.IsTagBlockingInput())
            {
                return EvoloPegasusGameState.Blocking;
            }
            else if (state.IsMulliganPhase())
            {
                if (state.IsMulliganManagerActive() == false
                    || MulliganManager.Get() == null
                    || MulliganManager.Get().GetMulliganButton() == null
                    || MulliganManager.Get().GetMulliganButton().activeInHierarchy == false)
                {
                    return EvoloPegasusGameState.Blocking;
                }

                FieldInfo filedinfo = MulliganManager.Get().GetType().GetField("m_waitingForUserInput", BindingFlags.NonPublic | BindingFlags.Instance);
                bool iswaiting = (bool)filedinfo.GetValue(MulliganManager.Get());
                if (!iswaiting)
                {
                    return EvoloPegasusGameState.Blocking;
                }

                return EvoloPegasusGameState.WaitForMulligan;
            }
            else if (state.IsMulliganPhasePending())
            {
                return EvoloPegasusGameState.Blocking;
            }
            else if (state.IsFriendlySidePlayerTurn() == true)
            {
                if (EndTurnButton.Get().IsInWaitingState())
                {
                    return EvoloPegasusGameState.Blocking;
                }

                return EvoloPegasusGameState.WaitForPlay;
            }

            return EvoloPegasusGameState.None;
        }

        /// <summary>
        /// Navigate to Hub Scene.
        /// </summary>
        public void NavigateToHubScene()
        {
            SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
        }

        /// <summary>
        /// Navigate to Tournament Scene.
        /// </summary>
        public void NavigateToTournamentScene()
        {
            SceneMgr.Get().SetNextMode(SceneMgr.Mode.TOURNAMENT);
        }

        /// <summary>
        /// Navigate to Adventure Scene.
        /// </summary>
        public void NavigateToAdventureScene()
        {
            SceneMgr.Get().SetNextMode(SceneMgr.Mode.ADVENTURE);
        }

        /// <summary>
        /// Start a practice (PVE) game.
        /// </summary>
        public void PlayPracticeGame()
        {
            if (this.ConfigDeck())
                PracticePickerTrayDisplay.Get().m_playButton.TriggerRelease();
        }

        /// <summary>
        /// Start a tournament (PVP) game.
        /// </summary>
        public void PlayTournamentGame()
        {
            try
            {
                if(EvoloGUI.IsMedalReceived)
                {
                    Options.Get().SetBool(Option.HAS_CLICKED_TOURNAMENT, true);
                    Options.Get().SetBool(Option.HAS_SEEN_TOURNAMENT, true);
                    Options.SetFormatType(PegasusShared.FormatType.FT_STANDARD);
                    Options.SetInRankedPlayMode(true);

                    var decks = GameObject.FindObjectsOfType<CollectionDeckBoxVisual>();
                    var exist = false;
                    long deckId = 0;
                    foreach (var d in decks)
                    {
                        if (d.GetClass() == TAG_CLASS.MAGE)
                        {
                            d.TriggerRelease();
                            exist = true;
                            deckId = d.GetDeckID();
                            break;
                        }
                    }
                    if (exist)
                        GameMgr.Get().FindGame(PegasusShared.GameType.GT_RANKED, PegasusShared.FormatType.FT_STANDARD, 2, deckId: deckId);
                }
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile("[EvoloPegasus.PlayTournamentGame]: Ranked start: " + e);
            }
            /*
            if (this.ConfigDeck())
            {
                var button = Hearthstone.UI.Clickable.FindObjectOfType<PlayButton>();
                button.TriggerRelease();
            }
            */
        }

        /// <summary>
        /// Config deck for a game.
        /// </summary>
        /// <param name="index">The index of deck.</param>
        public bool ConfigDeck()
        {
            try
            {
                var decks = GameObject.FindObjectsOfType<CollectionDeckBoxVisual>();
                var exist = false;
                foreach (var d in decks)
                {
                    if (d.GetClass() == EvoloConfigFile.Instance.heroClass && !d.IsLocked())
                    {
                        d.TriggerRelease();
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    foreach (var d in decks)
                    {
                        if (d.GetClass() == TAG_CLASS.MAGE)
                        {
                            d.TriggerRelease();
                            exist = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile("ConfigDeck: " + e);
            }
            return true;
        }

        private bool IsDefaultClass(TAG_CLASS heroClass)
        {
            switch (heroClass)
            {

                case TAG_CLASS.DRUID:
                case TAG_CLASS.HUNTER:
                case TAG_CLASS.MAGE:
                case TAG_CLASS.PALADIN:
                case TAG_CLASS.PRIEST:
                case TAG_CLASS.ROGUE:
                case TAG_CLASS.SHAMAN:
                case TAG_CLASS.WARLOCK:
                case TAG_CLASS.WARRIOR:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Config opponent for practice game.
        /// </summary>
        /// <param name="index">The index of opponent.</param>
        public void ConfigPracticeOpponent(int index)
        {
            this.tracer.Verbose(GetPrivateField<PracticeAIButton>(PracticePickerTrayDisplay.Get(), "m_selectedPracticeAIButton")?.name);

            List<PracticeAIButton> m_practiceAIButtons = GetPrivateField<List<PracticeAIButton>>(PracticePickerTrayDisplay.Get(), "m_practiceAIButtons");


            if (!EvoloGUI.IsSellReady)
            {
                if (index <= 0 || index > m_practiceAIButtons.Count)
                {
                    var i = 0;
                    var f = true;

                    foreach (var b in m_practiceAIButtons)
                    {
                        if (b.GetClass() == TAG_CLASS.HUNTER)
                        {
                            i++;
                            continue;
                        }
                        if (b.m_questBang.active)
                        {
                            index = i;
                            f = false;
                            break;
                        }
                        i++;
                    }
                    if (f)
                    {
                        EvoloEngine.Get().configuration.GameMode = EvoloGameMode.Ranked;
                        return;
                    }
                    // Random r = new Random();
                    // index = r.Next(0, m_practiceAIButtons.Count);
                }
                else
                {
                    index -= 1;
                }
            }

            var c = 0;
            foreach (var b in m_practiceAIButtons)
            {
                if (b.m_questBang.active || !IsDefaultClass(b.GetClass()))
                    continue;
                c++;
            }
            if (c >= 4 && !EvoloGUI.IsSellReady)
            {
                EvoloEngine.Get().configuration.GameMode = EvoloGameMode.Ranked;
                return;
            }
            if (EvoloGUI.IsSellReady)
                index = 1;

            m_practiceAIButtons[index].TriggerRelease();

            // var peguis = GameObject.FindObjectsOfType<PegUIElement>();
            // foreach (var ui in peguis)
            //     LogManager.WriteToErrorFile("UI: " + ui.name + " " + ui.GetType());

            this.tracer.Verbose(GetPrivateField<PracticeAIButton>(PracticePickerTrayDisplay.Get(), "m_selectedPracticeAIButton")?.name);
        }

        /// <summary>
        /// Config mode for practice game.
        /// </summary>
        /// <param name="expert">If play expert mode.</param>
        public void ConfigPracticeMode(bool expert)
        {
            AdventureDbId adventureId = AdventureDbId.PRACTICE;// Options.Get().GetEnum<AdventureDbId>(Option.SELECTED_ADVENTURE, AdventureDbId.PRACTICE);
            AdventureModeDbId modeId = (AdventureModeDbId)1;//AdventureModeDbId.LINEAR;//Options.Get().GetEnum<AdventureModeDbId>(Option.SELECTED_ADVENTURE_MODE, AdventureModeDbId.LINEAR);
            if (expert)
            {
                modeId = AdventureModeDbId.EXPERT;//Options.Get().GetEnum<AdventureModeDbId>(Option.SELECTED_ADVENTURE_MODE, AdventureModeDbId.EXPERT);
            }
            // LogManager.WriteToErrorFile("AdventureID: " + adventureId + " ModeID: " + modeId);
            // LogManager.WriteToErrorFile("SELECTED AdventureID: " + Options.Get().GetEnum<AdventureDbId>(Option.SELECTED_ADVENTURE, AdventureDbId.PRACTICE) + " ModeID: " + Options.Get().GetEnum<AdventureModeDbId>(Option.SELECTED_ADVENTURE_MODE, AdventureModeDbId.LINEAR));
            if (AdventureConfig.CanPlayMode(adventureId, modeId))
            {
                AdventureConfig.Get().SetSelectedAdventureMode(adventureId, modeId);
                AdventureConfig.Get().ChangeSubSceneToSelectedAdventure();
            }
        }

        /// <summary>
        /// Config mode for tournament game.
        /// </summary>
        /// <param name="ranked">If play ranked mode.</param>
        /// <param name="wild">If play wild format.</param>
        public void ConfigTournamentMode(bool ranked, bool wild)
        {
            bool is_ranked = Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE);
            if (is_ranked != ranked)
            {
                Options.Get().SetBool(Option.IN_RANKED_PLAY_MODE, ranked);
            }

            bool is_wild = Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE);
            if (is_wild != wild)
            {
                Options.Get().SetBool(Option.IN_RANKED_PLAY_MODE, wild);
            }
        }

        /// <summary>
        /// Get a Evolo object with EvoloId.
        /// </summary>
        /// <param name="EvoloId">The EvoloId.</param>
        /// <returns>The pegasus object.</returns>
        public IEvoloObject GetObject(int EvoloId)
        {
            var card = GetCard(GameState.Get(), EvoloId);
            if (card == null)
            {
                return null;
            }
            return new EvoloPegasusObject(card);
        }

        /// <summary>
        /// Click a Evolo object with EvoloId.
        /// </summary>
        /// <param name="EvoloId">The EvoloId.</param>
        public void ClickObject(int EvoloId)
        {
            EvoloPegasusObject obj = (EvoloPegasusObject)this.GetObject(EvoloId);

            EvoloPegasusInputHelper.ClickCard(obj.PegasusCard);

            if (obj.PegasusCard.GetEntity().HasSubCards())
            {
                this.tracer.Verbose($"Click {obj.RockId} {obj.Name} HasSubCards");
                EvoloPegasusInputHelper.DropCard();
            }
            else
            {
                this.tracer.Verbose($"Click {obj.RockId} {obj.Name}");
            }
        }

        /// <summary>
        /// Drop a holding pegasus object.
        /// </summary>
        public void DropObject()
        {
            this.tracer.Verbose($"DropCard");
            EvoloPegasusInputHelper.DropCard();
        }

        /// <summary>
        /// Snapshot current scene.
        /// </summary>
        /// <param name="sessionId">The Session Id.</param>
        /// <param name="actionId">The Action Id.</param>
        /// <returns>The EvoloScene.</returns>
        public EvoloScene SnapshotScene(string sessionId, int actionId)
        {
            return EvoloPegasusSnapshotHelper.SnapshotScene(sessionId, actionId);
        }

        /// <summary>
        /// Get selected DeckID
        /// </summary>
        /// <returns>The DeckId.</returns>
        public long GetSelectedDeckID()
        {
            // DeckPickerTrayDisplay is used on both Tournament and Practice
            return DeckPickerTrayDisplay.Get().GetSelectedDeckID();
        }

        /// <summary>
        /// Get a private field of an object
        /// </summary>
        /// <typeparam name="T">The field type.</typeparam>
        /// <param name="obj">The object.</param>
        /// <param name="field">The field name.</param>
        /// <returns>The field value.</returns>
        public static T GetPrivateField<T>(object obj, string field)
        {
            FieldInfo fieldinfo = obj.GetType().GetField(field, BindingFlags.NonPublic | BindingFlags.Instance);
            T m_practiceAIButtons = (T)fieldinfo.GetValue(obj);

            return m_practiceAIButtons;
        }

        public static void SetPrivateField(object obj, string field, object value)
        {
            FieldInfo fieldinfo = obj.GetType().GetField(field, BindingFlags.NonPublic | BindingFlags.Instance);
            fieldinfo.SetValue(obj, value);
        }

        /// <summary>
        /// Get a Card with EvoloId.
        /// </summary>
        /// <param name="gameState">The GameState.</param>
        /// <param name="EvoloId">The EvoloId.</param>
        /// <returns>The card.</returns>
        private static Card GetCard(GameState gameState, int EvoloId)
        {
            return GameState.Get().GetEntity(EvoloId)?.GetCard();
        }

        /// <summary>
        /// Get a Entity with EvoloId.
        /// </summary>
        /// <param name="gameState">The GameState.</param>
        /// <param name="EvoloId">The EvoloId.</param>
        /// <returns>The entity.</returns>
        private static Entity GetEntity(GameState gameState, int EvoloId)
        {
            return GameState.Get().GetEntity(EvoloId);
        }

        /// <summary>
        /// Get the SubsceneState in AdventureScene
        /// </summary>
        /// <returns>The EvoloPegasusSubsceneState.</returns>
        private EvoloPegasusSubsceneState GetPegasusAdventureSubsceneState()
        {
            if (AdventureConfig.Get() == null)
            {
                return EvoloPegasusSubsceneState.None;
            }

            Adventuresubscene currentSubScene = AdventureConfig.Get().CurrentSubScene;

            if (currentSubScene == Adventuresubscene.CHOOSER)
            {
                return EvoloPegasusSubsceneState.WaitForChooseMode;
            }

            if (currentSubScene == Adventuresubscene.PRACTICE)
            {
                if (PracticePickerTrayDisplay.Get().IsShown() == false)
                {
                    return EvoloPegasusSubsceneState.WaitForChooseDeck;
                }

                if (GetPrivateField<PracticeAIButton>(PracticePickerTrayDisplay.Get(), "m_selectedPracticeAIButton") == null)
                {
                    return EvoloPegasusSubsceneState.WaitForChooseOpponent;
                }
                else
                {
                    return EvoloPegasusSubsceneState.Ready;
                }
            }

            return EvoloPegasusSubsceneState.None;
        }

        /// <summary>
        /// Get the SubsceneState in TournamentScene
        /// </summary>
        /// <returns>The EvoloPegasusSubsceneState.</returns>
        private EvoloPegasusSubsceneState GetPegasusTournamentSubsceneState()
        {
            if (DeckPickerTrayDisplay.Get() == null)
            {
                return EvoloPegasusSubsceneState.None;
            }
            else
            {
                return EvoloPegasusSubsceneState.Ready;
            }
        }

        //// private static void FindGame(EvoloGameMode gameMode, long deckId, int missionId)
        //// {
        ////     Options.Get().SetBool(Option.HAS_PLAYED_EXPERT_AI, true);
        ////     GameType gameType = EvoloPegasusHelper.GetGameType(gameMode);
        ////     FormatType formatType = EvoloPegasusHelper.GetFormatType(gameMode);
        //// 
        ////     GameMgr.Get().FindGame(gameType, formatType, missionId, deckId, 0L);
        //// }
    }
}
