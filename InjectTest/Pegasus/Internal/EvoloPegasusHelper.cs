﻿using InjectTest.Contracts;
using PegasusShared;

namespace InjectTest.Pegasus.Internal
{
    internal static class EvoloPegasusHelper
    {
        /// <summary>
        /// Get Pegasus Game Type from EvoloGameMode
        /// </summary>
        /// <param name="gameMode">The EvoloGameMode</param>
        /// <returns>The GameType</returns>
        public static GameType GetGameType(EvoloGameMode gameMode)
        {
            switch (gameMode)
            {
                case EvoloGameMode.NormalPractice:
                case EvoloGameMode.ExpertPractice:
                    return GameType.GT_VS_AI;
                case EvoloGameMode.Casual:
                case EvoloGameMode.WildCasual:
                    return GameType.GT_CASUAL;
                case EvoloGameMode.Ranked:
                case EvoloGameMode.WildRanked:
                    return GameType.GT_RANKED;
                default:
                    return GameType.GT_UNKNOWN;
            }
        }

        /// <summary>
        /// Get Pegasus Format Type from EvoloGameMode
        /// </summary>
        /// <param name="gameMode">The EvoloGameMode</param>
        /// <returns>The FormatType</returns>
        public static FormatType GetFormatType(EvoloGameMode gameMode)
        {
            switch (gameMode)
            {
                case EvoloGameMode.Casual:
                case EvoloGameMode.Ranked:
                    return FormatType.FT_STANDARD;
                case EvoloGameMode.NormalPractice:
                case EvoloGameMode.ExpertPractice:
                case EvoloGameMode.WildCasual:
                case EvoloGameMode.WildRanked:
                    return FormatType.FT_WILD;
                default:
                    return FormatType.FT_UNKNOWN;
            }
        }

        /// <summary>
        /// Get EvoloPegasusSceneState from scene mode.
        /// </summary>
        /// <param name="sceneMode">The Pegasus Scene Mode.</param>
        /// <returns>The EvoloPegasusSceneState.</returns>
        public static EvoloPegasusSceneState GetPegasusSceneState(SceneMgr.Mode sceneMode)
        {
            switch (sceneMode)
            {
                case SceneMgr.Mode.STARTUP:
                case SceneMgr.Mode.LOGIN:
                case SceneMgr.Mode.RESET:
                    return EvoloPegasusSceneState.BlockingScene;
                case SceneMgr.Mode.COLLECTIONMANAGER:
                case SceneMgr.Mode.PACKOPENING:
                case SceneMgr.Mode.FRIENDLY:
                case SceneMgr.Mode.CREDITS:
                case SceneMgr.Mode.DRAFT:
                case SceneMgr.Mode.TAVERN_BRAWL:
                    return EvoloPegasusSceneState.CancelableScene;
                case SceneMgr.Mode.TOURNAMENT:
                    return EvoloPegasusSceneState.TournamentScene;
                case SceneMgr.Mode.HUB:
                    return EvoloPegasusSceneState.HubScene;
                case SceneMgr.Mode.GAMEPLAY:
                    return EvoloPegasusSceneState.GamePlay;
                case SceneMgr.Mode.ADVENTURE:
                    return EvoloPegasusSceneState.AdventureScene;
                case SceneMgr.Mode.INVALID:
                case SceneMgr.Mode.FATAL_ERROR:
                default:
                    return EvoloPegasusSceneState.InvalidScene;
            }
        }
    }
}
