﻿using InjectTest.Contracts;
using InjectTest.Engine;
using System;
using System.Collections.Generic;

namespace InjectTest.Pegasus.Internal
{
    internal class EvoloPegasusSnapshotHelper
    {
        /// <summary>
        /// Snapshot Scene
        /// </summary>
        /// <param name="sessionId">The Session Id.</param>
        /// <param name="actionId">The Action Id.</param>
        /// <returns>The EvoloScene.</returns>
        public static EvoloScene SnapshotScene(string sessionId, int actionId)
        {
            var EvoloScene = new EvoloScene();

            Player self = GameState.Get().GetFriendlySidePlayer();
            Player opponent = GameState.Get().GetFirstOpponentPlayer(GameState.Get().GetFriendlySidePlayer());

            EvoloScene.Self = SnapshotPlayer(self);
            EvoloScene.Opponent = SnapshotPlayer(opponent);
            EvoloScene.PlayOptions = SnapshotOptions();
            EvoloScene.Turn = GameState.Get().GetTurn();
            EvoloScene.SessionId = sessionId;
            EvoloScene.ActionId = actionId;

            if (EvoloEngineBot.BotNickname == "")
                EvoloEngineBot.BotNickname = GameState.Get().GetFriendlySidePlayer().GetName();
            EvoloScene.Self.NickName = EvoloEngineBot.BotNickname;
            EvoloScene.Self.GameMode = EvoloEngine.GetGameMode();
            EvoloScene.Self.MedalLevel = RankMgr.Get().GetLocalPlayerMedalInfo().GetCurrentMedal(PegasusShared.FormatType.FT_CLASSIC).GetMaxStarLevel();
            try
            {
                EvoloScene.Opponent.NickName = GameState.Get()?.GetOpposingSidePlayer()?.GetName();
                EvoloScene.Opponent.MedalLevel = GameState.Get().GetOpposingSidePlayer().GetRank().GetCurrentMedal(PegasusShared.FormatType.FT_CLASSIC).GetMaxStarLevel();
            }
            catch (Exception e) { }

            return EvoloScene;
        }

        /// <summary>
        /// Snapshot a Player.
        /// </summary>
        /// <param name="player">The Player.</param>
        /// <returns>The EvoloPlayer.</returns>
        private static EvoloPlayer SnapshotPlayer(Player player)
        {

            var EvoloPlayer = new EvoloPlayer();
            EvoloPlayer.Resources = player.GetNumAvailableResources();

            // EvoloPlayer.PermanentResources = player.Mana();
            EvoloPlayer.TemporaryResources = player.GetRealTimeTempMana();
            EvoloPlayer.Hero = SnapshotHero(player);
            EvoloPlayer.Power = SnapshotPower(player);
            EvoloPlayer.Minions = SnapshotMinions(player);
            EvoloPlayer.Cards = SnapshotCards(player);
            EvoloPlayer.Choices = SnapshotChoices(player);
            EvoloPlayer.PowerAvailable = !player.GetHeroPower().IsExhausted();

            EvoloPlayer.HasWeapon = player.HasWeapon();
            if (EvoloPlayer.HasWeapon)
            {
                EvoloPlayer.Weapon = SnapshotCard(player.GetHero().GetWeaponCard().GetEntity());
            }
            else
            {
                EvoloPlayer.Weapon = new EvoloCard();
            }

            return EvoloPlayer;
        }

        /// <summary>
        /// Snapshot a Hero.
        /// </summary>
        /// <param name="player">The Player.</param>
        /// <returns>The EvoloHero.</returns>
        private static EvoloHero SnapshotHero(Player player)
        {
            var EvoloHero = new EvoloHero();

            var heroEntity = player.GetHero();
            switch (player.GetHeroCard().GetEntity().GetClass())
            {
                case TAG_CLASS.WARLOCK:
                    EvoloHero.Class = EvoloHeroClass.Warlock;
                    break;
                case TAG_CLASS.HUNTER:
                    EvoloHero.Class = EvoloHeroClass.Hunter;
                    break;
                case TAG_CLASS.DRUID:
                    EvoloHero.Class = EvoloHeroClass.Druid;
                    break;
                case TAG_CLASS.PALADIN:
                    EvoloHero.Class = EvoloHeroClass.Paladin;
                    break;
                case TAG_CLASS.ROGUE:
                    EvoloHero.Class = EvoloHeroClass.Rogue;
                    break;
                case TAG_CLASS.SHAMAN:
                    EvoloHero.Class = EvoloHeroClass.Shaman;
                    break;
                case TAG_CLASS.WARRIOR:
                    EvoloHero.Class = EvoloHeroClass.Warrior;
                    break;
                case TAG_CLASS.PRIEST:
                    EvoloHero.Class = EvoloHeroClass.Priest;
                    break;
                case TAG_CLASS.MAGE:
                    EvoloHero.Class = EvoloHeroClass.Mage;
                    break;
                default:
                    EvoloHero.Class = EvoloHeroClass.None;
                    break;
            }

            EvoloHero.RockId = heroEntity.GetEntityId();
            EvoloHero.Name = heroEntity.GetName();
            EvoloHero.CardId = heroEntity.GetCardId();
            EvoloHero.Damage = heroEntity.GetATK();
            EvoloHero.CanAttack = heroEntity.CanAttack();
            EvoloHero.IsExhausted = heroEntity.IsExhausted();
            EvoloHero.Health = heroEntity.GetRealTimeRemainingHP();
            EvoloHero.IsQuest = heroEntity.IsQuest();
            EvoloHero.IsSecret = heroEntity.IsSecret();

            return EvoloHero;
        }

        /// <summary>
        /// Snapshot a Hero Power.
        /// </summary>
        /// <param name="player">The Player.</param>
        /// <returns>The EvoloCard.</returns>
        private static EvoloCard SnapshotPower(Player player)
        {
            return SnapshotCard(player.GetHeroPower());
        }

        /// <summary>
        /// Snapshot minions.
        /// </summary>
        /// <param name="player">The Player.</param>
        /// <returns>The list of EvoloMinion.</returns>
        private static List<EvoloMinion> SnapshotMinions(Player player)
        {
            var EvoloMinions = new List<EvoloMinion>();

            List<Card> minions = player.GetBattlefieldZone().GetCards();
            foreach (var minion in minions)
            {
                EvoloMinions.Add(SnapshotMinion(minion.GetEntity()));
            }

            return EvoloMinions;
        }

        /// <summary>
        /// Snapshot a minion.
        /// </summary>
        /// <param name="minion">The Entity.</param>
        /// <returns>The EvoloMinion.</returns>
        private static EvoloMinion SnapshotMinion(Entity minion)
        {
            var EvoloMinion = new EvoloMinion();

            EvoloMinion.RockId = minion.GetEntityId();
            EvoloMinion.Name = minion.GetName();
            EvoloMinion.CardId = minion.GetCardId();
            EvoloMinion.Health = minion.GetRealTimeRemainingHP();
            EvoloMinion.BaseHealth = minion.GetHealth();
            EvoloMinion.CanAttack = minion.CanAttack();
            EvoloMinion.CanBeAttacked = minion.CanBeAttacked();
            EvoloMinion.Damage = minion.GetATK();
            EvoloMinion.HasTaunt = minion.HasTaunt();
            EvoloMinion.HasWindfury = minion.HasWindfury();
            EvoloMinion.HasDivineShield = minion.HasDivineShield();
            EvoloMinion.HasAura = minion.HasAura();
            EvoloMinion.IsStealthed = minion.IsStealthed();
            EvoloMinion.IsExhausted = minion.IsExhausted();
            EvoloMinion.IsFrozen = minion.IsFrozen();
            EvoloMinion.IsAsleep = minion.IsAsleep();
            EvoloMinion.HasDeathrattle = minion.HasDeathrattle();
            EvoloMinion.HasInspire = minion.HasInspire();
            EvoloMinion.HasTriggerVisual = minion.HasTriggerVisual();
            EvoloMinion.HasLifesteal = minion.HasLifesteal();
            EvoloMinion.IsPoisonous = minion.IsPoisonous();
            EvoloMinion.IsEnraged = minion.IsEnraged();
            EvoloMinion.HasBattlecry = minion.HasBattlecry();
            EvoloMinion.Race = (EvoloRace)(int)minion.GetEntityDef().GetRace();

            return EvoloMinion;
        }

        /// <summary>
        /// Snapshot cards.
        /// </summary>
        /// <param name="player">The Player.</param>
        /// <returns>The list of EvoloCard.</returns>
        private static List<EvoloCard> SnapshotCards(Player player)
        {
            var EvoloCards = new List<EvoloCard>();

            List<Card> cards = player.GetHandZone().GetCards();
            foreach (var card in cards)
            {
                EvoloCards.Add(SnapshotCard(card.GetEntity()));
            }

            return EvoloCards;
        }

        /// <summary>
        /// Snapshot card choices.
        /// </summary>
        /// <param name="player">The Player.</param>
        /// <returns>The list of EvoloCard.</returns>
        private static List<EvoloCard> SnapshotChoices(Player player)
        {
            var EvoloCards = new List<EvoloCard>();

            var choices = GameState.Get()?.GetEntityChoices(player.GetPlayerId());

            if (choices != null)
            {
                foreach (var entityId in choices.Entities)
                {
                    EvoloCards.Add(SnapshotCard(GameState.Get()?.GetEntity(entityId)));
                }
            }

            return EvoloCards;
        }

        /// <summary>
        /// Snapshot a card.
        /// </summary>
        /// <param name="card">The Entity.</param>
        /// <returns>The EvoloCard.</returns>
        private static EvoloCard SnapshotCard(Entity card)
        {
            var EvoloCard = new EvoloCard();

            EvoloCard.RockId = card.GetEntityId();
            EvoloCard.Name = card.GetName();
            EvoloCard.CardId = card.GetCardId();
            EvoloCard.Cost = card.GetCost();
            if (card.IsMinion())
            {
                EvoloCard.CardType = EvoloCardType.Minion;
            }
            else if (card.IsSpell())
            {
                EvoloCard.CardType = EvoloCardType.Spell;
            }
            else if (card.IsEnchantment())
            {
                EvoloCard.CardType = EvoloCardType.Enchantment;
            }
            else if (card.IsWeapon())
            {
                EvoloCard.CardType = EvoloCardType.Weapon;
            }
            else
            {
                EvoloCard.CardType = EvoloCardType.None;
            }

            EvoloCard.Damage = card.GetATK();
            EvoloCard.Health = card.IsWeapon() ? card.GetCurrentDurability() : card.GetHealth();
            EvoloCard.HasTaunt = card.HasTaunt();
            EvoloCard.HasCharge = card.HasCharge();
            EvoloCard.Options = new List<EvoloCard>();

            if (card.HasSubCards())
            {
                foreach (var subCardID in card.GetSubCardIDs())
                {
                    EvoloCard.Options.Add(SnapshotCard(GameState.Get().GetEntity(subCardID)));
                }
            }

            return EvoloCard;
        }

        /// <summary>
        /// Snapshot play options.
        /// </summary>
        /// <returns>The play options.</returns>
        private static List<List<int>> SnapshotOptions()
        {
            var ret = new List<List<int>>();

            var options = GameState.Get()?.GetOptionsPacket();
            if (options == null || options.List == null)
            {
                var choices = GameState.Get()?.GetFriendlyEntityChoices();
                if (choices != null)
                {
                    foreach (var entityId in choices.Entities)
                    {
                        ret.Add(new List<int> { entityId });
                    }
                }

                return ret;
            }

            foreach (var option in options.List)
            {
                foreach (var subOption in SnapshotOptions(option))
                {
                    ret.Add(subOption);
                }
            }

            return ret;
        }

        /// <summary>
        /// Snapshot play options.
        /// </summary>
        /// <param name="option">The Network Option.</param>
        /// <returns>The play options.</returns>
        private static IEnumerable<List<int>> SnapshotOptions(Network.Options.Option option)
        {
            if (option.Subs != null && option.Subs.Count > 0)
            {
                foreach (var sub in option.Subs)
                {
                    foreach (var subOption in SnapshotSubOptions(sub))
                    {
                        var ret = new List<int> { option.Main.ID };
                        ret.AddRange(subOption);
                        yield return ret;
                    }
                }
            }
            else
            {
                foreach (var mainOption in SnapshotSubOptions(option.Main))
                {
                    yield return mainOption;
                }
            }
        }

        /// <summary>
        /// Snapshot play sub options.
        /// </summary>
        /// <param name="subOption">The Network SubOption.</param>
        /// <returns>The play sub options.</returns>
        private static IEnumerable<List<int>> SnapshotSubOptions(Network.Options.Option.SubOption subOption)
        {
            if (!subOption.HasValidTarget())
            {
                if (subOption.PlayErrorInfo.IsValid())
                {
                    if (subOption.ID == 0)
                    {
                        yield break;
                    }
                    else
                    {
                        yield return new List<int> { subOption.ID };
                    }
                }
            }
            else
            {
                foreach (var target in subOption.Targets)
                {
                    if (target.PlayErrorInfo.IsValid())
                    {
                        yield return new List<int> { subOption.ID, target.ID };
                    }
                }
            }
        }
    }
}
