﻿using InjectTest.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace InjectTest.Bot
{
    public class DeviceDataSaver
    {
        public enum DEVICE_TYPE
        {
            NONE = 0,
            ANDROID = 1,
            IPAD = 2,
            IOS = 3,
            TABLET = 4
        }

        [SerializeField]
        public struct DeviceData
        {
            public DEVICE_TYPE Device_Type;
            public bool Completed;

            public DeviceData(DEVICE_TYPE device_Type, bool completed)
            {
                Device_Type = device_Type;
                Completed = completed;
            }
        }

        //Public
        public List<DeviceData> Devices_array = new List<DeviceData>();

        //Private
        private DEVICE_TYPE CurrentDevice = DEVICE_TYPE.NONE;

        //Private Static
        private static DeviceDataSaver _instance;
        private static string CONFIG_PATH = "devicesSaver.json";

        public DeviceDataSaver()
        {
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            var text = "";
            for (var i = 0; i < commandLineArgs.Length; i++)
            {
                var arg = commandLineArgs[i];
                text += arg + " ";
            }

            if (text.Contains("android"))
                CurrentDevice = DEVICE_TYPE.ANDROID;
            else if (text.Contains("ipad"))
                CurrentDevice = DEVICE_TYPE.IPAD;
            else if (text.Contains("ios"))
                CurrentDevice = DEVICE_TYPE.IOS;
            else if (text.Contains("tablet"))
                CurrentDevice = DEVICE_TYPE.TABLET;
            LogManager.WriteToErrorFile("[DeviceDataSaver.Initial Construct]: Arguments: " + text + "\nDevice: " + CurrentDevice);
        }

        public static DeviceDataSaver Get()
        {
            if (_instance == null)
                LoadConfig();
            return _instance;
        }

        public DEVICE_TYPE GetCurrentDevice()
            => CurrentDevice;

        public bool IsAllDevicesComplete()
        {
            var c = 0;
            foreach (var deviceData in Devices_array)
                if (deviceData.Completed)
                    c++;
            return c >= 4;
        }

        bool IsDeviceCompleted(DEVICE_TYPE device)
        {
            foreach (var data in Devices_array)
                if (data.Device_Type == device)
                    return data.Completed;
            return false;
        }

        public string GetNextDeviceKeyword()
        {
            if (!IsDeviceCompleted(DEVICE_TYPE.ANDROID))
                return "-android";
            if (!IsDeviceCompleted(DEVICE_TYPE.IPAD))
                return "-ipad";
            if (!IsDeviceCompleted(DEVICE_TYPE.IOS))
                return "-ios";
            if (!IsDeviceCompleted(DEVICE_TYPE.TABLET))
                return "-tablet";
            return "";
        }

        public void CompleteCurrentDevice()
        {
            try
            {
                var contains = false;
                for (var i = 0; i < Devices_array.Count; i++)
                {
                    var data = Devices_array[i];
                    if (data.Device_Type != CurrentDevice) continue;
                    data.Completed = true;
                    Devices_array[i] = data;
                    contains = true;
                }
                if (!contains)
                    Devices_array.Add(new DeviceData(CurrentDevice, true));
                SaveConfig();
                if (!IsAllDevicesComplete())
                    RelaunchForNextDevice();
            }
            catch (Exception e)
            {
                LogManager.WriteToErrorFile("[DeviceDataSaver.CompleteCurrentDevice]: " + e);
            }
        }

        public static void ClearConfig()
        {
            try
            {
                var path = Directory.GetCurrentDirectory() + @"\" + CONFIG_PATH;
                File.WriteAllText(path, "{}");
            }
            catch { }
        }

        public static void LoadConfig()
        {
            try
            {
                if (_instance != null) return;

                var path = Directory.GetCurrentDirectory() + @"\" + CONFIG_PATH;
                if (File.Exists(path))
                {
                    var text = File.ReadAllText(path);
                    _instance = JsonConvert.DeserializeObject<DeviceDataSaver>(text);
                    if (_instance == null)
                    {
                        ClearConfig();
                        _instance = new DeviceDataSaver();
                    }
                }
                else
                    _instance = new DeviceDataSaver();
            }
            catch
            {
                ClearConfig();
                _instance = new DeviceDataSaver();
            }
        }

        private void SaveConfig()
        {
            var path = Directory.GetCurrentDirectory() + @"\" + CONFIG_PATH;
            File.WriteAllText(path, JsonConvert.SerializeObject(this));
        }

        private void RelaunchForNextDevice()
        {
            InjectMain.IsBotEnabled = false;

            string[] commandLineArgs = Environment.GetCommandLineArgs();
            string email = "", pass = "", sandNumber = "";
            for (var i = 0; i < commandLineArgs.Length; i++)
            {
                var arg = commandLineArgs[i];
                if (arg.Contains("-email") && i + 1 < commandLineArgs.Length)
                    email = commandLineArgs[i + 1];
                if (arg.Contains("-pass") && i + 1 < commandLineArgs.Length)
                    pass = commandLineArgs[i + 1];
                if (arg.Contains("-sand") && i + 1 < commandLineArgs.Length)
                    sandNumber = commandLineArgs[i + 1];
            }

            var start_args = "-launch " + GetNextDeviceKeyword() + " -sand " + sandNumber + " -email " + email + " -pass " + pass;
            if (InjectMain.LastLaunchToken.Length > 0)
                start_args += "-token " + InjectMain.LastLaunchToken;
            var dir = Directory.GetCurrentDirectory() + "\\Hearthstone.exe";
            LogManager.WriteToErrorFile("[DeviceDataSaver.Relaunch]: dir: " + dir + " Args: " + start_args);

            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = dir;
            p.StartInfo.Arguments = start_args;
            p.Start();

            Application.Quit();

            /*
             * 
            System.Diagnostics.Process.Start(dir , start_args);
            Application.Quit();
            */
        }
    }
}
